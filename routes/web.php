<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', function(){
        return view('welcome');
    });
    Route::get('profile', 'UserController@profile');

    Route::get('acceptance-protocol-list/{id}', 'CustomerController@acceptanceProtocolList');
    Route::get('acceptance-protocol-pdf/{id}', 'PDFController@acceptanceProtocol');

    Route::get('get-session', 'SessionController@getSession');
    Route::post('set-session', 'SessionController@setSession');

    // Customer
    Route::middleware(['permission:4'])->group(function (){ // customer permission
        Route::get('my-contracts', 'CustomerController@myContracts');
        Route::get('contract-preview/{id}', 'CustomerController@contractPreview');
        Route::get('my-collecting-forms/{id}', 'CustomerController@myCollectingForm');

        Route::get('my-companies', 'CustomerController@myCompanies');
        Route::get('subsidiary/{id}', 'CustomerController@subsidiary');

        Route::post('my-collecting-form-pdf', 'PDFController@myCollectingFormDownload');
    });

    // User
    Route::middleware(['permission:1|2'])->group(function (){ // admin, user permission
        Route::get('customer-list', 'UserController@getCustomerList');
        Route::get('driver-list', 'UserController@getDriverList');
        Route::get('user-list', 'UserController@getUserList');
        Route::get('new-customer/{role}', 'UserController@newCustomer');
        Route::post('save-customer', 'UserController@insertUpdate');
        Route::get('edit-customer/{id}', 'UserController@edit');
        Route::post('delete-customer', 'UserController@deleteCustomer');

        Route::get('customer/{id}/contract-list', 'ContractController@userContractList');
        Route::get('new-contract/{id}', 'ContractController@newContract');
        Route::get('edit-contract/{id}', 'ContractController@edit');
        Route::post('save-contract', 'ContractController@insertUpdate');
        Route::post('close-contract', 'ContractController@close');
        Route::post('delete-contract', 'ContractController@deleteContract');

        Route::get('new-custom-waste/{id}', 'ContractController@newCustomWaste');
        Route::get('edit-custom-waste/{id}', 'ContractController@editCustomWaste');
        Route::post('save-custom-waste', 'ContractController@saveCustomWaste');
        Route::get('delete-custom-waste/{id}', 'ContractController@deleteCustomWaste');

        Route::get('price-list', 'PriceController@getList');
        Route::post('edit-price', 'PriceController@editPrice');

        Route::any('annual-report', 'AnnualReportController@annualReport');

        Route::any('stock-records', 'ExcelController@stockRecordList');
        Route::any('export-stock-records', 'ExcelController@exportStockRecord');

        Route::any('get-annual-report', 'AnnualReportController@getAnnualReport');
        Route::any('export-records-sheets', 'ExcelController@recordSheets');

        Route::get('edit-collecting-form/{id}', 'CollectingFormController@editCollectingForm');
        Route::post('save-collecting-form', 'CollectingFormController@saveCollectingForm');
        Route::post('delete-collecting-form', 'CollectingFormController@deleteCollectingForm');

        Route::get('warehouse', 'WarehouseController@index');
        Route::get('add-stock-movement', 'WarehouseController@addStockMovement');
        Route::get('edit-stock-movement/{id}', 'WarehouseController@editStockMovement');
        Route::post('save-stock-movement', 'WarehouseController@saveStockMovement');
        Route::get('delete-stock-movement/{id}', 'WarehouseController@deleteStockMovement');

        Route::get('add-warehouse-month-record', 'WarehouseController@addMonthRecord');
        Route::get('stock-movement-list', 'WarehouseController@stockMovementList');

        Route::any('reports/collecting-form-list', 'ReportsController@collectingFormList');
        Route::any('export-collecting-form-list', 'ExcelController@exportCollectingFormList');
    });
        //Driver
    Route::middleware(['permission:1|2|3'])->group(function (){ // admin, user, driver permission
        Route::any('road-list', 'DriverController@roadList');
        Route::get('removal-confirmation/{id}', 'DriverController@removalConfirmation');
        Route::post('save-removal-confirmation', 'DriverController@save');
        Route::post('save-removal-confirmation-signature', 'DriverController@saveSignature');
        Route::get('signature-pad/{id}', 'DriverController@signaturePad');

        Route::get('storage-barel-list', 'DriverController@storageBarelList');

        Route::get('acceptance-protocol/{contract_id}', 'DriverController@acceptanceProtocol');
        Route::post('save-acceptance-protocol', 'DriverController@saveAcceptanceProtocol');

        Route::post('driver-update', 'UserController@driverUpdate');

        Route::get('company-collecting-form-list', 'DriverController@companyCollectingFormList');
        Route::get('collecting-form-list/{id}', 'CustomerController@myCollectingForm');

        Route::post('collecting-form-pdf', 'PDFController@collectingFormDownload');
    });

});



//Test
Route::any('/test', 'DriverController@test');
//Route::any('/test', 'PDFController@zbernyList');
//Route::any('/testd', 'PDFController@zbernyListDownload');
