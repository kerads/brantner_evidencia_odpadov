<?php

namespace App;

use App\Contract;
use Illuminate\Database\Eloquent\Model;

class Harmonogram extends Model
{
    public function todayList($date){
        $today = date('w', strtotime($date));
        $oddEvenWeek = (date('W', strtotime($date))%2==1);

        $contract = new Contract();
        $contractList = $contract->select(
            'contracts.*'
        )
            ->join('repeating_cycles', 'repeating_cycles.id', '=', 'contracts.repeating_cycle_id')
            ->where('contracts.deleted',0)
            ->where(function ($query) use ($date) {
                $query->where('contracts.closed_at', '<', $date)
                    ->orWhereNull('contracts.closed_at');
            })
            ->where('contracts.recurrency_week_days', 'LIKE', "%{$today}%")
            ->where(function ($query) use ($oddEvenWeek) {
                if($oddEvenWeek == true){
                    $query->where('repeating_cycles.odd_week', 1);
                } else {
                    $query->where('repeating_cycles.even_week', 1);
                }
            })
            ->get();

        $contractList = $this->isTodayCollected($contractList, $date);

        return $contractList;
    }

    public function isTodayCollected($contractList, $date){
        foreach($contractList as $contract){
            $contract->extracted = false;
            foreach($contract->collectingForm as $collectingForm){
                if(date('Y-m-d', strtotime($collectingForm->extraction_date)) == $date ){
                    $contract->extracted = true;
                }
            }
        }

        return $contractList;
    }

}
