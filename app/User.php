<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id', 'name', 'email', 'password', 'external_id', 'ico', 'dic', 'icdph', 'adresa', 'psc', 'mesto', 'telefon', 'zodpovedny_veduci', 'z_telefon', 'role_id', 'signature', 'vrn'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role(){
        return $this->hasOne('App\Role', 'id', 'role_id');
    }

    public function parent(){
        return $this->hasOne('App\User', 'id', 'parent_id');
    }

    public function child(){
        return $this->hasMany('App\User', 'parent_id', 'id');
    }

    public function contract(){
        return $this->hasMany('App\Contract', 'user_id', 'id')->orderBy('id', 'DESC');
    }

    public function getContract($id){
        return $this->hasOne('App\Contract', 'user_id', 'id')->where('id', $id)->first();
    }

    public function firstContract(){
        return $this->hasOne('App\Contract', 'user_id', 'id')->first();
    }

    public function lastContract(){
        return $this->hasOne('App\Contract', 'user_id', 'id')->where('deleted', 0)->latest();
    }

    public function waste(){
        return $this->hasMany('App\Waste', 'user_id', 'id');
    }

    public function storageBarelMovement(){
        return $this->hasMany('App\StorageBarelMovement', 'user_id', 'id');
    }

    public function hasPermission($permission_id){
        $userPermission = Auth::user()->role_id;
        if($userPermission == $permission_id){
            return true;
        }

        return false;
    }

    public function validate($request){
        if($request->role_id == 3){
            return $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|max:255|email',
                'telefon' => 'numeric|nullable',
            ]);
        }

        if($request->role_id == 4){
            return $request->validate([
                'name' => 'required|max:255',
                'email' => 'required|max:255',
                'external_id' => 'numeric|nullable',
                'ico' => 'numeric|nullable',
                'dic' => 'numeric|nullable',
                'telefon' => 'numeric|nullable',
                'z_telefon' => 'numeric|nullable',
            ]);
        }
    }

    public function insertUpdate($request){
        if(isset($request->id)){
            $user = $this->where('id', $request->id)->first();
        } else{
            $user = new User();
        }

        $user->name = isset($request->name) ? $request->name : $user->name;
        $user->email = isset($request->email) ? $request->email : $user->email;
        $user->role_id = isset($request->role_id) ? $request->role_id : 4 ;
        $user->telefon = isset($request->telefon) ? $request->telefon : $user->telefon ;

        if(!isset($request->role_id ) || $request->role_id == 4){
            $user->parent_id = $request->parent_id;
            $user->external_id = (isset($request->external_id) || $request->external_id == '') ? $request->external_id : '';
            $user->ico = isset($request->ico) ? $request->ico : '';
            $user->dic = isset($request->dic) ? $request->dic : '';
            $user->icdph = isset($request->icdph) ? $request->icdph : '';
            $user->adresa = isset($request->adresa) ? $request->adresa : '';
            $user->psc = isset($request->psc) ? $request->psc : '';
            $user->mesto = isset($request->mesto) ? $request->mesto : '';
            $user->zodpovedny_veduci = isset($request->zodpovedny_veduci) ? $request->zodpovedny_veduci : '' ;
            $user->z_telefon = isset($request->z_telefon) ? $request->z_telefon : $user->z_telefon ;
        }

        if(isset($request->role_id ) && $request->role_id == 3){
            $user->vrn = isset($request->vrn) ? $request->vrn : $user->vrn ;
            $user->signature = isset($request->signature) ? $request->signature : $user->signature ;
        }

        $user->save();

        return $user;
    }

    public function deleteCustomer($id){
        $user = $this->where('id',$id)->first();
        $user->password = null;
        $user->deleted = 1;
        $user->save();

        return $id;
    }

}
