<?php

namespace App;

use App\User;
use App\Contract;
use App\PriceList;
use App\CustomWasteType;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class CollectingForm extends Model
{
    protected $fillable=[
        'contract_id', 'customer_id', 'driver_id', 'waste_type', 'waste_code', 'waste_name', 'num_barels', 'barel_size',
        'waste_weight', 'extra_num_barels','extra_barel_size', 'extra_waste_weight',
        'oil_capacity', 'write_off', 'toilet_paper', 'kitchen_towels', 'hygienic_handkerchiefs', 'oil_for_oil',
        'fat_weight', 'fat_write_off', 'fat_toilet_paper', 'fat_kitchen_towels', 'fat_hygienic_handkerchiefs', 'fat_for_oil',
        'lat', 'lng', 'vrn', 'extraction_date',
        'caught', 'signature', 'driver_signature'
    ];

    protected $user;

    protected $priceList;

    protected $customWasteType;

    public function __construct(){
        $this->user = new User();
        $this->priceList = new PriceList();
        $this->customWasteType = new CustomWasteType();
    }

    public function contract(){
        return $this->belongsTo('App\Contract', 'contract_id', 'id');
    }

    public function customer(){
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }

    public function driver(){
        return $this->belongsTo('App\User', 'driver_id', 'id');
    }

    public function validate($request){
        if(!isset($request->id)){
            $request->validate([
                'contract_id' => 'required',
                'driver_id' => 'required',
                'extraction_date' => 'required|date',
            ]);

            if($request->waste_switcher == 1){
                $request->validate([
                    'num_barels' => 'required|numeric|min:0',
                    'barel_size' => 'required|numeric',
                    'waste_weight' => 'required|numeric',
                ]);
            }

            if($request->extra_waste_switcher == 1){
                $request->validate([
                    'extra_num_barels' => 'required|numeric|min:0',
                    'extra_barel_size' => 'required|numeric',
                    'extra_waste_weight' => 'required|numeric',
                ]);
            }

            if($request->oil_switcher == 1){
                $request->validate([
                    'oil_capacity' => 'numeric|required',
                ]);

                if($request->write_off != 1 && empty($request->toilet_paper) && empty($request->kitchen_towels) && empty($request->hygienic_handkerchiefs) ){
                    $request->validate([
                        'toilet_paper' => 'numeric|required|min:0',
                        'kitchen_towels' => 'numeric|required|min:0',
                        'hygienic_handkerchiefs' => 'numeric|required|min:0',
                        'oil_for_oil' => 'numeric|required|min:0',
                    ]);
                }
            }

            if($request->fat_switcher == 1){
                $request->validate([
                    'fat_weight' => 'nullable|numeric|required',
                ]);

                if($request->fat_write_off != 1 && empty($request->fat_toilet_paper) && empty($request->fat_kitchen_towels) && empty($request->fat_hygienic_handkerchiefs) ){
                    $request->validate([
                        'fat_toilet_paper' => 'numeric|required|min:0',
                        'fat_kitchen_towels' => 'numeric|required|min:0',
                        'fat_hygienic_handkerchiefs' => 'numeric|required|min:0',
                        'fat_for_oil' => 'numeric|required|min:0',
                    ]);
                }
            }
        } elseif($request->caught == 1){
            $request->validate([
                'caught' => 'required',
                'signature' => 'required',
            ]);
        }
    }

    public function insertUpdate($request){
        $driver = $this->user->where('id', $request->driver_id)->first();

        if($request->id){
            $collectingForm = $this->where('id', $request->id)->first();
        } else {
            $collectingForm = new CollectingForm();
        }

        if(isset($request->waste_type) && $request->waste_type == '-1') {
            $collectingForm->waste_type = $request->waste_type;
            $collectingForm->waste_code = null;
            $collectingForm->waste_name = null;
        } elseif(isset($request->waste_type) && $collectingForm->waste_type !== $request->waste_type) {
            $wasteType = $this->customWasteType->where('id', $request->waste_type)->first();

            $collectingForm->waste_type = $wasteType->id;
            $collectingForm->waste_code = $wasteType->code;
            $collectingForm->waste_name = $wasteType->name;
        }

        $collectingForm->contract_id = isset($request->contract_id) ? $request->contract_id : $collectingForm->contract_id;
        $collectingForm->customer_id = isset($request->customer_id) ? $request->customer_id : $collectingForm->customer_id;
        $collectingForm->driver_id = isset($request->driver_id) ? $request->driver_id : $collectingForm->driver_id;

        if (!isset($request->id)){
            $collectingForm->num_barels = ($request->waste_switcher == 1) && ($request->num_barels > 0 ) ? $request->num_barels : 0;
            $collectingForm->extra_num_barels = ($request->extra_waste_switcher == 1) && ($request->extra_num_barels > 0 ) ? $request->extra_num_barels : $collectingForm->extra_num_barels;
        } else {
            $collectingForm->num_barels = isset($request->num_barels) ? $request->num_barels : $collectingForm->num_barels;
            $collectingForm->extra_num_barels = isset($request->extra_num_barels) ? $request->extra_num_barels : $collectingForm->extra_num_barels;
        }

        $collectingForm->barel_size = isset($request->barel_size) ? $request->barel_size : $collectingForm->barel_size;
        $collectingForm->waste_weight = isset($request->waste_weight) ? $request->waste_weight : $collectingForm->waste_weight;
        $collectingForm->extra_barel_size = isset($request->extra_barel_size) ? $request->extra_barel_size : $collectingForm->extra_barel_size;
        $collectingForm->extra_waste_weight = isset($request->extra_waste_weight) ? $request->extra_waste_weight : $collectingForm->extra_waste_weight;
        $collectingForm->oil_capacity = isset($request->oil_capacity) ? $request->oil_capacity : $collectingForm->oil_capacity;
        $collectingForm->write_off = isset($request->write_off) ? $request->write_off : (isset($collectingForm->write_off) ? $collectingForm->write_off : 0);
        $collectingForm->toilet_paper = isset($request->toilet_paper) ? $request->toilet_paper : $collectingForm->toilet_paper;
        $collectingForm->kitchen_towels = isset($request->kitchen_towels) ? $request->kitchen_towels : $collectingForm->kitchen_towels;
        $collectingForm->hygienic_handkerchiefs = isset($request->hygienic_handkerchiefs) ? $request->hygienic_handkerchiefs : $collectingForm->hygienic_handkerchiefs;
        $collectingForm->oil_for_oil = isset($request->oil_for_oil) ? $request->oil_for_oil : $collectingForm->oil_for_oil;
        $collectingForm->fat_weight = isset($request->fat_weight) ? $request->fat_weight : $collectingForm->fat_weight;
        $collectingForm->fat_write_off = isset($request->fat_write_off) ? $request->fat_write_off : (isset($collectingForm->fat_write_off) ? $collectingForm->fat_write_off : 0);
        $collectingForm->fat_toilet_paper = isset($request->fat_toilet_paper) ? $request->fat_toilet_paper : $collectingForm->fat_toilet_paper;
        $collectingForm->fat_kitchen_towels = isset($request->fat_kitchen_towels) ? $request->fat_kitchen_towels : $collectingForm->fat_kitchen_towels;
        $collectingForm->fat_hygienic_handkerchiefs = isset($request->fat_hygienic_handkerchiefs) ? $request->fat_hygienic_handkerchiefs : $collectingForm->fat_hygienic_handkerchiefs;
        $collectingForm->fat_for_oil = isset($request->fat_for_oil) ? $request->fat_for_oil : $collectingForm->fat_for_oil;
        $collectingForm->extraction_date = isset($request->extraction_date) ? $request->extraction_date : $collectingForm->extraction_date;
        $collectingForm->lat = isset($request->lat) ? $request->lat : $collectingForm->lat;
        $collectingForm->lng = isset($request->lng) ? $request->lng : $collectingForm->lng;
        $collectingForm->vrn = isset($driver->vrn) ? $driver->vrn : $collectingForm->vrn;
        $collectingForm->driver_signature = isset($driver->signature) ? $driver->signature : $collectingForm->driver_signature;
        $collectingForm->caught = isset($request->caught) ? $request->caught : $collectingForm->caught;
        $collectingForm->signature = isset($request->signature) ? $request->signature : $collectingForm->signature;

        $collectingForm->save();

        return $collectingForm;
    }

    public function getContractMonthDataRemovalConfirmation($contractId){
        $from = date('Y-m-1 00:00:00');
        $to = date('Y-m-t 23:59:59');
        $date = date('Y-m');

        $collectingFormList = $this->select(
            'customer_id',
            'contract_id',
            DB::raw('count(id) as count_total'),
            DB::raw('sum(waste_weight) / 1000 as waste_weight'),
            DB::raw('sum(num_barels) as num_barels'),
            DB::raw('sum(fat_weight) / 1000 as fat_weight'),
            DB::raw('sum(extra_waste_weight) / 1000 as extra_waste_weight'),
            DB::raw('sum(extra_num_barels) as extra_num_barels')
        )
            ->where('contract_id', $contractId)
            ->whereBetween('extraction_date', [$from, $to])
            ->groupBy('customer_id', 'contract_id')
            ->first('customer_id', 'contract_id', 'id', 'waste_weight', 'num_barels', 'extra_waste_weight', 'extra_num_barels', 'fat_weight');

        $list = $this->getContractBarelsPerMonth($collectingFormList, $date, $contractId);
        $list->num_barels = $collectingFormList == null ? 0 : $collectingFormList->num_barels;

        return $list;
    }

    public function getExcelData($date){
        $from = date('Y-m-1 00:00:00', strtotime($date));
        $to = date('Y-m-t 23:59:59', strtotime($date));

        $list = $this->getExcelWasteData($from, $to);
        $list = $this->monthPayment($list, $date);
        $list = $this->getExcelOilWriteData($from, $to, $list);
        $list = $this->getExcelOilWriteOffData($from, $to, $list);
        $list = $this->getExcelFatWriteData($from, $to, $list);
        $list = $this->getExcelFatWriteOffData($from, $to, $list);


        return $list;
    }

    public function getExcelWasteData($from, $to){
        $list = $this->select(
            'customer_id',
            'contract_id',
            DB::raw('count(id) as count_total'),
            DB::raw('sum(waste_weight) / 1000 as waste_weight'),
            DB::raw('sum(num_barels) as num_barels'),
//            DB::raw('sum(fat_weight) / 1000 as fat_weight'),
            DB::raw('sum(extra_waste_weight) / 1000 as extra_waste_weight'),
            DB::raw('sum(extra_num_barels) as extra_num_barels'),
            DB::raw('sum(toilet_paper + fat_toilet_paper) as toilet_paper'),
            DB::raw('sum(hygienic_handkerchiefs + fat_hygienic_handkerchiefs) as hygienic_handkerchiefs'),
            DB::raw('sum(kitchen_towels + fat_kitchen_towels) as kitchen_towels'),
            DB::raw('sum(oil_for_oil + fat_for_oil) as oil')
        )
            ->whereBetween('extraction_date', [$from, $to])
            ->groupBy('customer_id', 'contract_id')
            ->get('customer_id', 'contract_id', 'id', 'waste_weight', 'num_barels', 'extra_waste_weight', 'extra_num_barels', 'fat_weight', 'toilet_paper', 'hygienic_handkerchiefs', 'kitchen_towels','oil_for_oil', 'fat_toilet_paper', 'fat_hygienic_handkerchiefs', 'fat_kitchen_towels', 'fat_for_oil');

        return $list;
    }

    public function getExcelOilWriteData($from, $to, $list){
        $oilList = $this->select(
            'customer_id',
            'contract_id',
            DB::raw('sum(toilet_paper) as toilet_paper'),
            DB::raw('sum(hygienic_handkerchiefs) as hygienic_handkerchiefs'),
            DB::raw('sum(kitchen_towels) as kitchen_towels'),
            DB::raw('sum(oil_for_oil) as oil_for_oil'),
            DB::raw('sum(oil_capacity) * 0.9 /1000 as oil_capacity'),
            DB::raw('sum(oil_capacity) * 0.9 /1000 as write_oil_weight')
        )
            ->where('write_off', 0)
            ->whereBetween('extraction_date', [$from, $to])
            ->groupBy('customer_id', 'contract_id')
            ->get('customer_id', 'contract_id', 'oil_capacity', 'write_oil_weight', 'toilet_paper', 'hygienic_handkerchiefs', 'kitchen_towels', 'oil_for_oil');

        foreach($list as &$item){
            foreach($oilList as $oilItem){
                $oilItem->oil_total_price = 0;

                if($item->customer_id == $oilItem->customer_id && $oilItem->write_oil_weight > 0){
                    $item->write_on_oil = $oilItem;
                }
            }
        }

        return $list;
    }

    public function getExcelOilWriteOffData($from, $to, $list){
        $oilList = $this->select(
            'customer_id',
            'contract_id',
            DB::raw('sum(oil_capacity) as oil_capacity'),
            DB::raw('sum(oil_capacity) * 0.9 /1000 as write_off_oil_weight')
        )
            ->whereBetween('extraction_date', [$from, $to])
            ->where('write_off', 1)
            ->groupBy('customer_id', 'contract_id')
            ->get('customer_id', 'contract_id', 'oil_capacity', 'write_off_oil_weight');

        $oilPrice = $this->priceList->select('price')->where('id', 6)->first();

        foreach($list as &$item){
            foreach($oilList as $oilItem){
                if($oilItem->contract->custom_oil_price == null){
                    $oilItem->write_off_oil_total_price = ($oilPrice->price * $oilItem->oil_capacity * -1);
                } else {
                    $oilItem->write_off_oil_total_price = ($oilItem->contract->custom_oil_price * $oilItem->oil_capacity * -1);
                }

                if($item->customer_id == $oilItem->customer_id && $oilItem->write_off_oil_weight > 0){
                    $item->write_off_oil = $oilItem;
                }
            }
        }

        return $list;
    }

    public function getExcelFatWriteData($from, $to, $list){
        $fatList = $this->select(
            'customer_id',
            'contract_id',
            DB::raw('sum(fat_toilet_paper) as fat_toilet_paper'),
            DB::raw('sum(fat_hygienic_handkerchiefs) as fat_hygienic_handkerchiefs'),
            DB::raw('sum(fat_kitchen_towels) as fat_kitchen_towels'),
            DB::raw('sum(fat_for_oil) as fat_for_oil'),
            DB::raw('sum(fat_weight) / 1000 as fat_capacity'),
            DB::raw('sum(fat_weight) / 1000 as write_fat_weight')
        )
            ->whereNotNull('fat_weight')
            ->where('fat_write_off', 0)
            ->whereBetween('extraction_date', [$from, $to])
            ->groupBy('customer_id', 'contract_id')
            ->get('customer_id', 'contract_id', 'fat_weight', 'write_fat_weight', 'fat_toilet_paper', 'fat_hygienic_handkerchiefs', 'fat_kitchen_towels', 'fat_for_oil');

        foreach($list as &$item){
            foreach($fatList as $fatItem){
                $fatItem->fat_total_price = 0;

                if($item->customer_id == $fatItem->customer_id && $fatItem->write_fat_weight > 0){
                    $item->write_on_fat = $fatItem;
                }
            }
        }

        return $list;
    }

    public function getExcelFatWriteOffData($from, $to, $list){
        $fatList = $this->select(
            'customer_id',
            'contract_id',
            DB::raw('sum(fat_weight) / 1000 as fat_weight'),
            DB::raw('sum(fat_weight) / 1000 as write_off_fat_weight')
        )
            ->whereNotNull('fat_weight')
            ->whereBetween('extraction_date', [$from, $to])
            ->where('fat_write_off', 1)
            ->groupBy('customer_id', 'contract_id')
            ->get('customer_id', 'contract_id', 'fat_weight', 'write_off_fat_weight');

        $fatPrice = $this->priceList->select('price')->where('id', 7)->first();

        foreach($list as &$item){
            foreach($fatList as $fatItem){
                if($fatItem->contract->custom_fat_price == null){
                    $fatItem->write_off_oil_total_price = ($fatPrice->price * $fatItem->fat_weight * -1);
                } else {
                    $fatItem->write_off_fat_total_price = ($fatItem->contract->custom_fat_price * $fatItem->fat_capacity * -1);
                }

                if($item->customer_id == $fatItem->customer_id && $fatItem->write_off_fat_weight > 0){
                    $item->write_off_fat = $fatItem;
                }
            }
        }

        return $list;
    }

    public function yearWasteAnnualReport($year, $customer_id){
        $from = date($year . '-1-1 00:00:00');
        $to = date($year . '-12-31 23:59:59');

        $list = $this->select(
            'customer_id',
            DB::raw('sum(waste_weight) / 1000 as waste_weight'),
            DB::raw('sum(oil_capacity) * 0.9 /1000 as oil_weight'),
            DB::raw('sum(fat_weight) / 1000 as fat_weight'),
            DB::raw('"O" as waste_catalog'),
            DB::raw('"1" as row_count'),
            DB::raw('"20 01 08" as waste_catalog_num'),
            DB::raw('"20 01 25" as fat_catalog_num'),
            DB::raw('"biologicky rozložiteľný kuchynský a <br> reštauračný odpad" as waste_note'),
            DB::raw('"použité jedlé oleje a tuky" as fat_note'),
            DB::raw('"0" as fat'),
            DB::raw('"1" as data')
        )
            ->where('customer_id', $customer_id)
            ->whereBetween('extraction_date', [$from, $to])
            ->groupBy('customer_id')
            ->first('customer_id', 'waste_weight', 'oil_capacity', 'fat_weight');


        if(empty($list)) {
            $message = ('Za rok ' . $year . ' neexistuje zberný list.');
            return (['warning' => $message]);
        }

        $list->arrYear = str_split($year);
        $list->arrIco = $this->stringToArray( $list->customer->ico, 8);

        if(empty($list->customer->parent)){
            $list->customer->parent = $list->customer;
        }
        return $list;
    }

    public function monthPayment($list, $date){
        foreach($list as &$item){
            $item =  $this->getContractMonthData($item, $date);
        }
        return $list;
    }

    public function getContractMonthData($item, $date){
        $item = $this->getContractBarelsPerMonth($item, $date);

        //                Ziskat cenu z cennika alebo zo zmluvy
        if($item->contract->price_type != 1){
            $item->contract_price = $item->contract->custom_price;
        } else {
            if(!isset($item->contract->price->price)){
                $url = url('edit-contract/' . $item->contract->id);
                $link = ('<a href="' . $url . '">' . $item->contract->id . '</a>');
                $message = ('Zmluva s id: "' . $link . '" nie je správne zadaná do systému. Prosím skontrolujte ju a skúste to znovu.');
                return redirect()->back()->with(['error' => $message]);
            }
            $item->contract_price = $item->contract->price->price * (100 - $item->contract->discount) / 100;
        }

        //                Vypocitat cenu za mesiac podla zmluvy
        $desinfectionPrice = $this->priceList->select('price')->where('id', 5)->first();
        $oilPrice = $this->priceList->select('price')->where('id', 6)->first();
        $fatPrice = $this->priceList->select('price')->where('id', 7)->first();

        switch($item->contract->price_type) {
            case 1:
            case 2:
                $item = $this->countMonthPrice($item);
                $item->desinfection_price = ($item->num_barels + $item->extra_num_barels) * $desinfectionPrice->price;
                break;
            case 3:
                $item->price_total = $item->contract_price;
                $item->desinfection_price = $item->contract->num_barels * $desinfectionPrice->price;
                break;
            case 4:
                if($item->contract->min_num_barels <= $item->num_barels && $item->num_barels <= $item->contract->max_num_barels){
                    $item = $this->countMonthPrice($item);
                    $item->price_total = $item->contract_price;
                    $item->desinfection_price = ($item->num_barels + $item->extra_num_barels) * $desinfectionPrice->price;
                } else {
                    $item->contract->barelsPerMonth = $item->contract->min_num_barels;
                    $item->contract->maxBarelsPerMonth = $item->contract->max_num_barels;
                    $item = $this->countMonthPrice($item);
                    $item->desinfection_price = ($item->num_barels + $item->extra_num_barels) * $desinfectionPrice->price;
                    break;
                }

        }

        // ak je pausalna cena za dezinfekciu, nastavit pausalnu cenu
        if($item->contract->custom_desinfection){
            $item->desinfection_price = $item->contract->custom_desinfection_price;
        }

        if($item->contract->custom_oil_price == null){
            $item->contract->custom_oil_price = $oilPrice->price;
        }

        if($item->contract->custom_oil_price == null){
            $item->contract->custom_fat_price = $fatPrice->price;
        }

        return $item;
    }

    public function countMonthPrice($item){
        if($item->contract->barelsPerMonth <= $item->num_barels && $item->num_barels <= $item->contract->maxBarelsPerMonth){
            $item->price_total = $item->contract_price;
        }elseif($item->num_barels < $item->contract->barelsPerMonth) {
            $count_barels = $item->contract->barelsPerMonth;
            if($item->contract->price_type == 4){
                $count_barels = $item->contract->num_barels;
            }
            $item->price_total = ($item->contract_price / $count_barels * $item->num_barels);
        }else{
            $item->price_total = $item->contract_price;
        }

        //        Cena za nadoby nad zmluvne mnozstvo
        $aboveContractQuantitiPrice = $item->contract->custom_above_quantity_price == 1 ? $item->contract->above_quantity_price : $this->priceList->select('price')->where('id', 3)->first()->price;
        $item->extra_price_total = $item->extra_num_barels * $aboveContractQuantitiPrice;

        return $item;
    }

    public function getContractBarelsPerMonth($item, $date, $contractId=null){
        $firstDayOfMonth = date('w', strtotime(date('Y-m-1', strtotime($date)))); // prvy den v mesiaci (pondelok az nedela)
        $lastDayOfMonth = (int) date('t', strtotime($date)); // pocet dni v mesiaci (vrati 28,29,30 alebo 31)
        $oddEvenWeek = (date('W', strtotime(date('Y-m-1', strtotime($date))))%2==1); // prvý týždeň v mesiaci (neparny / parny == false / true)

        $contract = new Contract();
        $userContract = isset($item->contract) ? $item->contract : $contract->where('id', $contractId)->first();


        $week_days = $userContract->recurrency_week_days == null ? null : $contract->stringToArray($userContract->recurrency_week_days);

        $userContract->repetitionsPerMonth = 0;
        $userContract->maxRepetitionsPerMonth = 0;

        if(!empty($week_days != null)){

            foreach($week_days as $key => $day){
                if($day < $firstDayOfMonth){
                    $firstSelectedDay = ($day - $firstDayOfMonth + 7);
                }else{
                    $firstSelectedDay = ($day - $firstDayOfMonth);
                }

                //                zisti pocet opakovani v mesiaci podla zmluvy
                switch($userContract->repeating_cycle_id){
                    case 1:
                        $userContract->repetitionsPerMonth += 1;
                        $userContract->maxRepetitionsPerMonth += 1;
                        break;
                    case 2:
                        $userContract->repetitionsPerMonth += 2;
                        if($oddEvenWeek){
                            $userContract->maxRepetitionsPerMonth += ceil(($lastDayOfMonth - $firstSelectedDay) / 14);
                        }else{
                            $userContract->maxRepetitionsPerMonth += ceil(($lastDayOfMonth - $firstSelectedDay - 7) / 14);
                        }
                        break;
                    case 3:
                        $userContract->repetitionsPerMonth += 2;
                        if(!$oddEvenWeek){
                            $userContract->maxRepetitionsPerMonth += ceil(($lastDayOfMonth - $firstSelectedDay) / 14);
                        }else{
                            $userContract->maxRepetitionsPerMonth += ceil(($lastDayOfMonth - $firstSelectedDay - 7) / 14);
                        }
                        break;
                    case 4:
                        $userContract->repetitionsPerMonth += 4;
                        $userContract->maxRepetitionsPerMonth += ceil(($lastDayOfMonth - $firstSelectedDay) / 7);
                        break;
                }
            }

            //                Pocet sudov za mesiac a maximalny pocet za mesiac
            $userContract->maxBarelsPerMonth = $userContract->maxRepetitionsPerMonth * $userContract->num_barels;
            $userContract->barelsPerMonth = $userContract->repetitionsPerMonth * $userContract->num_barels;
        }
        if(isset($item->contract)){
            $item->contract = $userContract;
            return $item;
        } else {
            $userContract->contract = $userContract;
            return $userContract;
        }

    }

    public function getRecordsUserExcelData($year, $customer_id){
        $from = date($year . '-1-1 00:00:00');
        $to = date($year . '-12-31 23:59:59');


        $list = $this->select()
            ->where('customer_id', $customer_id)
            ->whereBetween('extraction_date', [$from, $to])
            ->get();

        $customer = $this->user->where('id', $customer_id)->first();
        $customer->arrIco = $this->stringToArray( $customer->ico, 8);
        $customer->list = $list;

        return $customer;
    }

    public function stringToArray($string, $minLength=null){
        if($minLength != null){
            if(strlen($string) < $minLength){
                $string = str_pad($string, $minLength, "0", STR_PAD_LEFT);
            }
        }

        return str_split($string);
    }
}
