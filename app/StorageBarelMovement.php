<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StorageBarelMovement extends Model
{
    protected $fillable = [
        'user_id', 'contract_id', 'driver_id', 'barel_30', 'barel_60', 'lat', 'lng', 'signature', 'driver_signature', 'extraction_date'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function contract(){
        return $this->belongsTo('App\Contract', 'contract_id', 'id');
    }

    public function driver(){
        return $this->belongsTo('App\User', 'driver_id', 'id');
    }

    public function validate($request){
        $request->validate([
            'user_id' => 'required',
            'contract_id' => 'required',
            'driver_id' => 'required',
            'barel_30' => 'required|integer',
            'barel_60' => 'required|integer',
        ]);

        if($request->barel_30 == 0 && $request->barel_60 == 0){
            $request->validate([
                'barel_30' => 'required|integer|min:1',
                'barel_60' => 'required|integer|min:1',
            ]);
        }
    }

    public function insertUpdate($request){
        if(isset($reuqest->id)){
            $newStorageMovement = $this->where('id', $request->id)->first();
        } else {
            $newStorageMovement = new StorageBarelMovement();
        }

        $user = new User();
        $driver = $user->where('id', $request->driver_id)->first();

        $newStorageMovement->user_id = $request->user_id;
        $newStorageMovement->contract_id = $request->contract_id;
        $newStorageMovement->driver_id = $request->driver_id;
        $newStorageMovement->barel_30 = $request->barel_30;
        $newStorageMovement->barel_60 = $request->barel_60;
        $newStorageMovement->lat = $request->lat ?? null;
        $newStorageMovement->lng = $request->lng ?? null;
        $newStorageMovement->signature = $request->signature ?? null;
        $newStorageMovement->driver_signature = $driver->signature;
        $newStorageMovement->extraction_date = $request->extraction_date;


        $newStorageMovement->save();

        return $newStorageMovement;
    }
}
