<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomWasteType extends Model
{
    protected $fillable = ['user_id', 'contact_id', 'code', 'name'];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function contract(){
        return $this->belongsTo('App\Contract', 'contract_id', 'id');
    }

    public function validate($request){
        $request->validate([
            'user_id' => 'required',
            'contract_id' => 'required',
            'code' => 'required|max:255',
            'name' => 'required|max:255',
        ]);
    }

    public function isUnique($request){
        if(isset($request->id)){
            return true;
        }
        return (!$this->where('code', $request->code)->where('name', $request->name)->where('contract_id', $request->contract_id)->exists());
    }

    public function insertUpdate($request){
        if(isset($request->id)){
            $record = $this->where('id', $request->id)->first();
        } else {
            $record = new CustomWasteType();
            $record->user_id = $request->user_id;
            $record->contract_id = $request->contract_id;
        }

        $record->code = $request->code;
        $record->name = $request->name;

        return $record->save();
    }
}
