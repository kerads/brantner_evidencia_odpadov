<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepeatingCycle extends Model
{
    protected $fillable = [
        'id', 'name', 'odd_week', 'even_week'
    ];
}
