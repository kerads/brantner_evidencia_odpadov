<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Waste extends Model
{
    protected $fillable = [
        'id', 'user_id', 'typ', 'mnozstvo', 'vaha', 'datum', 'poznamka', 'umiestnenie', 'vyber', 'odpis', 'hygiena', 'zatvorene', 'typolejov', 'auto', 'sofer'
    ];

//    protected $month;
//
//    public function __construct(){
//        $this->month = date('M');
//    }

    public function user(){
        return $this->belongsTo('App/User', 'id', 'user_id');
    }

    public function monthDetail($user_id, $date = null ){
        if($date = null){
            $date = date();
        }
        $year = date('YYYY', strtotime($date));
        $month = date('MM', strtotime($date));

        $monthDetail = $this->where('user_id', $user_id)
            ->whereBetween('datum',)->get();
    }
}
