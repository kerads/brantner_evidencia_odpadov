<?php

namespace App\Http\Controllers;

use App\CollectingForm;
use Maatwebsite\Excel\Excel;
use App\Exports\AnnualReportExport;
use Illuminate\Http\Request;

class AnnualReportController extends Controller
{
    private $excel;

    public function __construct(Excel $excel){
        $this->excel = $excel;
    }

    public function annualReport(Request $request){
        return $this->excel->download(new AnnualReportExport, 'annualExport.xlsx', 'Xlsx');
    }

    public function getAnnualReport(Request $request){
        $collectingForm = new CollectingForm();
        $data = $collectingForm->yearWasteAnnualReport($_GET['year'], $_GET['customer_id']);

        if(isset($data['warning'])){
            return back()->with($data);
        }

        $response =  $this->excel->download(new AnnualReportExport($request), 'annualExport.xlsx', 'Xlsx');
        ob_end_clean();

        return $response;
    }
}
