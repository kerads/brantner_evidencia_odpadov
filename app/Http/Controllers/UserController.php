<?php

namespace App\Http\Controllers;

use App\User;
use App\Contract;
use App\SendMail;
use App\CValidityRecurrencyUntil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $user;

    public function __construct(){
        $this->user = new User();
    }

    public function profile(){
        return view('profile');
    }

    public function getCustomerList(){
        $user = new User();
        $userList = $user->where('role_id', 4)->where('deleted', 0)->get();

        return view('user.customer-list')->with(['userList' => $userList]);
    }

    public function getDriverList(){
        $user = new User();
        $userList = $user->where('role_id', 3)->where('deleted', 0)->get();

        return view('user.driver-list')->with(['userList' => $userList]);
    }

    public function getUserList(){
        $user = new User();
        $userList = $user->where('role_id', 2)->where('deleted', 0)->get();

        return view('user.user-list')->with(['userList' => $userList]);
    }

    public function newCustomer($role){
        $required = 'required';
        $disabled = '';
        $ValidityRecurrencyUntil = new CValidityRecurrencyUntil();
        $recurrencyList = $ValidityRecurrencyUntil->get();

        $userList = $this->user->where('role_id', 4)->get();

        return view('user.add-edit-customer')
            ->with(['role_id' => $role])
            ->with(['required' => $required])
            ->with(['disabled' => $disabled])
            ->with(['userList' => $userList])
            ->with(['recurrencyList' => $recurrencyList]);
    }

    public function insertUpdate(Request $request){
        $user = new User();
        $user->validate($request);
        $addedUser = $user->insertUpdate($request);
        if(!isset($request->id)){
            $sendMail = new SendMail();
            $sendMail->mailWelcome($addedUser->email);
        }
        return redirect( url('edit-customer/' . $addedUser->id) )->with('success', 'Uloženie prebehlo úspešne.');
    }

    public function driverUpdate(Request $request){
        $user = new User();
        $updatedUser = $user->insertUpdate($request);

        return redirect(url('profile/'))->with('success', 'Uloženie prebehlo úspešne.');
    }

    public function edit($id){
        $required = 'required';
        $disabled = '';
        $user = $this->user->where('id', $id)->first();
        $userList = $this->user->where('role_id', 4)->get();

        return view('user.add-edit-customer')
            ->with(['user' => $user])
            ->with(['required' => $required])
            ->with(['userList' => $userList])
            ->with(['disabled' => $disabled]);
    }

    public function deleteCustomer(Request $request){
        return $this->user->deleteCustomer($request->id);
    }
}
