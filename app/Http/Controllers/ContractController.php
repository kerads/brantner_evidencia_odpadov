<?php

namespace App\Http\Controllers;

use App\User;
use App\Contract;
use App\PriceList;
use App\RepeatingCycle;
use App\CustomWasteType;
use App\CValidityRecurrencyUntil;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    protected $user;

    protected $contract;

    protected $priceList;

    protected $repeatingCycle;

    protected $customWasteType;

    protected $ValidityRecurrencyUntil;

    public function __construct(){
        $this->user = new User();
        $this->contract = new Contract();
        $this->priceList = new PriceList();
        $this->repeatingCycle = new RepeatingCycle();
        $this->customWasteType = new CustomWasteType();
        $this->ValidityRecurrencyUntil = new CValidityRecurrencyUntil();
    }

    public function userContractList($id){
        $currentUser = $this->user->where('id', $id)->first();

        return view('user.contract-list')->with(['user' => $currentUser]);
    }

    public function insertUpdate(Request $request){
        $this->contract->validate($request);
        $contract = $this->contract->insertUpdate($request);

        return redirect(url('edit-contract/' . $contract->id))->with('success', 'Uloženie prebehlo úspešne.');
    }

    public function newContract($user_id){
        $priceList = $this->priceList->where('contract', 1)->get();
        $repeatingCycleList = $this->repeatingCycle->get();
        $recurrencyList = $this->ValidityRecurrencyUntil->get();
        $user =  $this->user->where('id', $user_id)->first();

        return view('user.add-edit-contract')
            ->with(['repeatingCycleList' => $repeatingCycleList])
            ->with(['recurrencyList' => $recurrencyList])
            ->with(['priceList' => $priceList])
            ->with(['user' => $user]);
    }

    public function edit($id){
        $priceList = $this->priceList->where('contract', 1)->get();
        $repeatingCycleList = $this->repeatingCycle->get();
        $recurrencyList = $this->ValidityRecurrencyUntil->get();
        $contract = $this->contract->where('id', $id)->first();
        $user = $this->user->where('id', $contract->user_id)->first();
        return view('user.add-edit-contract')
            ->with(['repeatingCycleList' => $repeatingCycleList])
            ->with(['recurrencyList' => $recurrencyList])
            ->with(['priceList' => $priceList])
            ->with(['contract' => $contract])
            ->with(['user' => $user]);
    }

    public function deleteContract(Request $request){
        return $this->contract->delContract($request->id);
    }

    public function close(Request $request){
        return $this->contract->close($request);
    }

    public function newCustomWaste($id){
        $contract = $this->contract->where('id', $id)->first();

        return view('user.add-edit-custom-waste')->with(['contract' => $contract]);
    }

    public function editCustomWaste($id){
        $customWaste = $this->customWasteType->where('id', $id)->first();

        return view('user.add-edit-custom-waste')
            ->with(['customWaste' => $customWaste])
            ->with(['contract' => $customWaste->contract]);
    }

    public function saveCustomWaste(Request $request){
        $this->customWasteType->validate($request);
        $isUnique = $this->customWasteType->isUnique($request);

        if($isUnique){
            $this->customWasteType->insertUpdate($request);
        } else {
            return back()->with('danger', 'Takýto záznam už existuje.');
        }

        return redirect(url('new-custom-waste/' . $request->contract_id))->with('success', 'Uloženie prebehlo úspešne.');
    }

    public function deleteCustomWaste($id){
        $customWaste = $this->customWasteType->where('id', $id)->first();
        $contract = $customWaste->contract;

        $customWaste->delete();

        return redirect(url('new-custom-waste/' . $contract->id))->with('warning', 'Záznam už existuje.');
    }
}
