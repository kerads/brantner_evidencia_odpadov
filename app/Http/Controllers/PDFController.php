<?php

namespace App\Http\Controllers;

use PDF;
use App\CollectingForm;
use App\StorageBarelMovement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PDFController extends Controller
{
    public function zbernyList(Request $request){
        $collectingForm = new CollectingForm();

        $form = $collectingForm->inRandomOrder()->first();
        $form->arrDate = $collectingForm->stringToArray($form->extraction_date);
//        PDF::loadView('pdf.zberny-list', $data)->setPaper('a4', 'portaint')->save( storage_path('app/public/pdf/invoices/' . $invoice->id . '.pdf') );
        return view('pdf.zberny-list')->with('form', $form);
    }

    public function collectingFormDownload(Request $request, CollectingForm $collectingForm){
        $form['form'] = $collectingForm->where('id', $request->id )->first();
        $form['form']->arrDate = $collectingForm->stringToArray($form['form']->extraction_date);

        $response = PDF::loadView('pdf.zberny-list', $form)->download('zberny-list-' . $form['form']->id . '.pdf');
        ob_end_clean();

        return $response;
//        return PDF::loadView('pdf.zberny-list', $form)->download('zberny-list-' . $form['form']->id . '.pdf');
    }

    public function myCollectingFormDownload(Request $request){

        $collectingForm = new CollectingForm();

        $form['form'] = $collectingForm->where('id', $request->id )->first();
        $form['form']->arrDate = $collectingForm->stringToArray($form['form']->extraction_date);

        if(Auth::user()->id == $form['form']->customer_id){
            $response = PDF::loadView('pdf.zberny-list', $form)->download('zberny-list-' . $form['form']->id . '.pdf');

            ob_end_clean();

            return $response;
//            return PDF::loadView('pdf.zberny-list', $form)->download('zberny-list-' . $form['form']->id . '.pdf');
        }
    }

    public function acceptanceProtocol($id){
        $storageBarelMovement = new StorageBarelMovement();

        $data = [];

        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2){
            $data['acceptanceProtocol'] = $storageBarelMovement->where('id', $id)->first();
        } else {
            $data['acceptanceProtocol'] = $storageBarelMovement->where('id', $id)->where('user_id', Auth::id())->first();
        }

        if(empty($data['acceptanceProtocol'])){
            return back()->with('error', 'Akceptačný protokol sa nenašiel, alebo nemáte právo na jeho zobrazenie');
        }

        $response = PDF::loadView('pdf.acceptance-protocol', $data)->download('akceptacny_protokol.pdf');
        ob_end_clean();

        return $response;
//        return PDF::loadView('pdf.acceptance-protocol', $data)->download('akceptacny_protokol.pdf');
    }
}
