<?php

namespace App\Http\Controllers;

use App\PriceList;
use Illuminate\Http\Request;

class PriceController extends Controller
{
    protected $priceList;

    public function __construct(){
        $this->priceList = new PriceList();
    }

    public function getList(){
        $priceList = $this->priceList->get();

        return view('user.price-list')->with(['priceList' => $priceList]);
    }

    public function editPrice(Request $request){
        return $this->priceList->inserUpdate($request);
    }
}
