<?php

namespace App\Http\Controllers;

use App\CollectingForm;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Exports\StockRecordsExport;
use App\Exports\RecordSheetsExport;
use App\Exports\CollectingFormListExport;

class ExcelController extends Controller
{
    private $excel;

    protected $collectingForm;

    public function __construct(Excel $excel){
        $this->excel = $excel;
        $this->collectingForm = new CollectingForm();
    }

    public function stockRecordList(Request $request){
        $year_month = isset($request->year_month) ? $request->year_month : date("Y-m");
        $collectingFormList = $this->collectingForm->getExcelData($year_month);

        return view('user.stock-record-list')
            ->with(['collectingFormList' => $collectingFormList])
            ->with(['year_month' => $year_month]);
    }

    public function exportStockRecord(Request $request){
        $response = $this->excel->download(new StockRecordsExport($request), 'Evidencia_' . $request->year_month . '.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        ob_end_clean();

        return $response;
//        return $this->excel->download(new StockRecordsExport($request), 'Evidencia_' . $request->year_month . '.xlsx', 'Xlsx');
    }

    public function stockRecords(Request $request){
        $date = isset($request->date) ? $request->date : date("Y-m-d");
        $collectingFormList = $this->collectingForm->getExcelData($date);

        return $collectingFormList;
    }

    public function recordSheets(Request $request){

        $response = $this->excel->download(new RecordSheetsExport($request), 'evidencni_list_' . $request->user_id . '_' . $request->year . '.xlsx', 'Xlsx');
        ob_end_clean();

        return $response;
//        return $this->excel->download(new RecordSheetsExport($request), 'evidencni_list_' . $request->user_id . '_' . $request->year . '.xlsx', 'Xlsx');
    }

    public function exportCollectingFormList(Request $request){
        $response = $this->excel->download(new CollectingFormListExport($request), 'zberne_listy_' . $request->date_from . '_' . $request->date_to . '.xlsx', 'Xlsx');
        ob_end_clean();

        return $response;
//        return $this->excel->download(new CollectingFormListExport($request), 'zberne_listy_' . $request->date_from . '_' . $request->date_to . '.xlsx', 'Xlsx');
    }
}
