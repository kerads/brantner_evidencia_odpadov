<?php

namespace App\Http\Controllers;

use App\CollectingForm;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    protected $collectingForm;

    public function __construct(){
        $this->collectingForm = new CollectingForm();
    }

    public function collectingFormList(Request $request){
        $from = isset($request->date_from) ? $request->date_from : date("Y-m-d 00:00:00");
        $to = isset($request->date_to) ? $request->date_to : date("Y-m-d 23:59:59");

        if($from > $to){
            return back()->with(['warning' => 'Dátum od musí byť skôr ako dátum do!']);
        }

        $collectingForms = $this->collectingForm->whereBetween('extraction_date', [$from, $to])->get();

        return view('reports.collecting-form-list')
            ->with('collectingForms', $collectingForms)
            ->with('date_from', $from)
            ->with('date_to', $to);
    }
}
