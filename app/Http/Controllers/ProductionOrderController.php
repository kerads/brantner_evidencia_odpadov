<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ProductionOrderController extends Controller
{
    public function getCompanyList(){
        $user = new User();
        $customerList = $user->where('role_id', 4)->get();

        return view('user.production-order-customer-list')->with(['customerList' => $customerList]);
    }
}
