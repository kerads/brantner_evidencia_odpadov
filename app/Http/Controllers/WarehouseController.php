<?php

namespace App\Http\Controllers;

use App\Storage;
use App\CollectingForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WarehouseController extends Controller
{
    protected $storage;

    protected $collectingForm;

    public function __construct(){
        $this->storage = new Storage();
        $this->collectingForm = new CollectingForm();
    }


    public function index(Request $request){
        session(['backUrl' => $request->fullUrl()]);
        $lastStorageRecord = $this->storage->where('is_summary', 1)->orderBy('change_date', 'DESC')->first();
        $lastFiveStorageRecord = $this->storage->where('is_summary', 0)->orderBy('change_date', 'DESC')->take(5)->get();
        $lastFiveStorageMonths = $this->storage->where('is_summary', 1)->orderBy('summary_date', 'DESC')->take(5)->get();

        $sumStorageRecord = $this->storage
            ->select(DB::raw("SUM(toilet_paper) as toilet_paper")
                , DB::raw("SUM(hygienic_handkerchiefs) as hygienic_handkerchiefs")
                , DB::raw("SUM(kitchen_towels) as kitchen_towels")
                , DB::raw("SUM(oil) as oil"))
            ->where('is_summary', 0)
            ->where('change_date', '>=', $lastStorageRecord->summary_date)
            ->first();

        $sumCollectingFormRecords = $this->collectingForm
            ->select(DB::raw("SUM(toilet_paper + fat_toilet_paper) as toilet_paper")
                , DB::raw("SUM(hygienic_handkerchiefs + fat_hygienic_handkerchiefs) as hygienic_handkerchiefs")
                , DB::raw("SUM(kitchen_towels + fat_kitchen_towels) as kitchen_towels")
                , DB::raw("SUM(oil_for_oil + fat_for_oil) as oil"))
            ->where('extraction_date', '>=', $lastStorageRecord->summary_date)
            ->first();

        $lastStorageRecord->toilet_paper = $lastStorageRecord->toilet_paper + $sumStorageRecord->toilet_paper - $sumCollectingFormRecords->toilet_paper;
        $lastStorageRecord->hygienic_handkerchiefs = $lastStorageRecord->hygienic_handkerchiefs + $sumStorageRecord->hygienic_handkerchiefs - $sumCollectingFormRecords->hygienic_handkerchiefs;
        $lastStorageRecord->kitchen_towels = $lastStorageRecord->kitchen_towels + $sumStorageRecord->kitchen_towels - $sumCollectingFormRecords->kitchen_towels;
        $lastStorageRecord->oil = $lastStorageRecord->oil + $sumStorageRecord->oil - $sumCollectingFormRecords->oil;

        return view('user.warehouse')
            ->with(['lastStorageRecord' => $lastStorageRecord])
            ->with(['lastFiveStorageRecord' => $lastFiveStorageRecord])
            ->with(['lastFiveStorageMonths' => $lastFiveStorageMonths]);
    }

    public function stockMovementList(Request $request){
        session(['backUrl' => $request->fullUrl()]);
        $storageRecords = $this->storage->where('is_summary', 0)->orderBy('summary_date', 'desc')->take(5)->get();
        return view('user.warehouse-record-list')
            ->with(['storageRecords' => $storageRecords]);
    }

    public function addStockMovement(){
        return view('user.add-edit-stock-movement');
    }

    public function editStockMovement($id){
        $lastStorageRecord = $this->storage->where('is_summary', 1)->orderBy('summary_date', 'desc')->first();
        $record = $this->storage->where('id', $id)->first();

        if($lastStorageRecord->summary_date > $record->change_date){
            return back()->with('warning', 'Tento záznam už nie je možné upraviť. Už bol uzatvorený mesiac.');
        }

        return view('user.add-edit-stock-movement')->with(['record' => $record]);
    }

    public function saveStockMovement(Request $request){
        $backUrl = $request->session()->get('backUrl', 'company-collecting-form-list');
        $record = $this->storage->insertUpdate($request);

        if($record == 'error'){
            return back()->with('error', 'Bohužiaľ niečo zlihalo :( Prosím prejdi si .');
        }

        return redirect($backUrl)->with('success', 'Uloženie prebehlo úspešne.');
    }

    public function deleteStockMovement($id){
        $record = $this->storage->where('id', $id)->first();
        $record->delete();

        return back()->with('success', 'Záznam bol vymazaný');
    }

    public function addMonthRecord(){
        $lastStorageRecord = $this->storage->where('is_summary', 1)->orderBy('summary_date', 'desc')->first();
        $nextMonth = date('Y-m-d', strtotime($lastStorageRecord->summary_date));
        $dateFrom = date('Y-m-01', strtotime($nextMonth));
        $dateTo = date('Y-m-t', strtotime($nextMonth));

        $sumStorageRecord = $this->storage
            ->select(DB::raw("SUM(toilet_paper) as toilet_paper")
                , DB::raw("SUM(hygienic_handkerchiefs) as hygienic_handkerchiefs")
                , DB::raw("SUM(kitchen_towels) as kitchen_towels")
                , DB::raw("SUM(oil) as oil"))
            ->where('is_summary', 0)
            ->whereBetween('change_date', [$dateFrom, $dateTo])
            ->first();

        $sumCollectingFormRecords = $this->collectingForm
            ->select(DB::raw("SUM(toilet_paper + fat_toilet_paper) as toilet_paper")
                , DB::raw("SUM(hygienic_handkerchiefs + fat_hygienic_handkerchiefs) as hygienic_handkerchiefs")
                , DB::raw("SUM(kitchen_towels + fat_kitchen_towels) as kitchen_towels")
                , DB::raw("SUM(oil_for_oil + fat_for_oil) as oil"))
            ->whereBetween('extraction_date', [$dateFrom, $dateTo])
            ->first();

        $lastStorageRecord->toilet_paper = $lastStorageRecord->toilet_paper + $sumStorageRecord->toilet_paper - $sumCollectingFormRecords->toilet_paper;
        $lastStorageRecord->hygienic_handkerchiefs = $lastStorageRecord->hygienic_handkerchiefs + $sumStorageRecord->hygienic_handkerchiefs - $sumCollectingFormRecords->hygienic_handkerchiefs;
        $lastStorageRecord->kitchen_towels = $lastStorageRecord->kitchen_towels + $sumStorageRecord->kitchen_towels - $sumCollectingFormRecords->kitchen_towels;
        $lastStorageRecord->oil = $lastStorageRecord->oil + $sumStorageRecord->oil - $sumCollectingFormRecords->oil;

        $newRecord = $this->storage->createMonthRecord($lastStorageRecord);

        return back()->with('success', 'Uloženie prebehlo úspešne.');
    }
}
