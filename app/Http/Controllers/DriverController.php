<?php

namespace App\Http\Controllers;

use App\User;
use App\Contract;
use App\Harmonogram;
use App\CollectingForm;
use App\StorageBarelMovement;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    protected $user;

    protected $contract;

    protected $collectingForm;

    protected $storageBarelMovement;

    public function __construct(){
        $this->user = new User();
        $this->contract = new Contract();
        $this->collectingForm = new CollectingForm();
        $this->storageBarelMovement = new StorageBarelMovement();
    }

    public function roadList(Request $request){
        session(['signaturePadBackUrl' => $request->fullUrl()]);
        $date = isset($request->date) ? $request->date : date('Y-m-d');
        $harmonogram = new Harmonogram();
        $contractList = $harmonogram->todayList($date);

        return view('driver.harmonogram')
            ->with(['date' => $date])
            ->with(['contractList' => $contractList]);
    }

    public function collectingFormList($id){
        $user = $this->user->where('id', $id)->first();

        return view('driver.collection-form-list')->with(['user' => $user]);
    }

    public function companyCollectingFormList(Request $request){
        session(['signaturePadBackUrl' => $request->fullUrl()]);
        $companyList = $this->user->where('role_id', 4)->get();

        return view('driver.customer-list')->with(['userList' => $companyList]);
    }

    public function removalConfirmation($id){
        $contract = new Contract();
        $contract = $contract->where('id', $id)->first();
        $driverList = $this->user->where('role_id', '3')->where('deleted', 0)->get();

        $contractCollectingForms = $this->collectingForm->getContractMonthDataRemovalConfirmation($id);

        return view('driver.removal-confirmation')
            ->with(['contract' => $contract])
            ->with(['contractCollectingForms' => $contractCollectingForms])
            ->with(['driverList' => $driverList]);
    }

    public function save(Request $request){
        $this->collectingForm->validate($request);
        $collectingForm = $this->collectingForm->insertUpdate($request);

        return redirect( url('signature-pad/' . $collectingForm->id));
    }

    public function saveSignature(Request $request){
        $this->collectingForm->validate($request);
        $collectingForm = $this->collectingForm->insertUpdate($request);

        return redirect( url($request->session()->get('signaturePadBackUrl', 'company-collecting-form-list')))->with('success', 'Uloženie prebehlo úspešne.');
    }

    public function signaturePad($id){
        $collectionForm = $this->collectingForm->where('id', $id)->first();

        return view('driver.signature-pad')->with(['collectionForm' => $collectionForm]);
    }

    public function storageBarelList(Request $request){
        session(['signaturePadBackUrl' => $request->fullUrl()]);
        $contractList = $this->contract->where('deleted', '0')->get();

        return view('driver.storage-barel-list')->with(['contractList' => $contractList]);
    }

    public function acceptanceProtocol($contract_id){
        $driverList = $this->user->where('role_id', '3')->get();
        $contract = $this->contract->where('id', $contract_id)->first();

        return view('driver.acceptance-protocol')
            ->with(['contract' => $contract])
            ->with(['driverList' => $driverList]);
    }

    public function saveAcceptanceProtocol(Request $request){
        $this->storageBarelMovement->validate($request);
        $this->storageBarelMovement->insertUpdate($request);

        return redirect( url($request->session()->get('signaturePadBackUrl', 'company-collecting-form-list')))->with('success', 'Uloženie prebehlo úspešne. ');
    }

    public function test(){
        return view('test');
    }
}
