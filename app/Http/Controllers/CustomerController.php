<?php

namespace App\Http\Controllers;

use App\User;
use App\Contract;
use App\PriceList;
use App\CollectingForm;
use App\RepeatingCycle;
use App\StorageBarelMovement;
use App\CValidityRecurrencyUntil;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomerController extends Controller
{
    protected $user;
    protected $contract;
    protected $priceList;
    protected $collectingForm;
    protected $storageBarelMovement;
    protected $ValidityRecurrencyUntil;

    public function __construct(){
        $this->user = new User();
        $this->contract = new Contract();
        $this->priceList = new PriceList();
        $this->collectingForm = new CollectingForm();
        $this->repeatingCycle = new RepeatingCycle();
        $this->storageBarelMovement = new StorageBarelMovement();
        $this->ValidityRecurrencyUntil = new CValidityRecurrencyUntil();
    }

    public function myContracts(){
        return view('customer.contract-list');
    }

    public function contractPreview($id){
        $priceList = $this->priceList->where('contract', 1)->get();
        $recurrencyList = $this->ValidityRecurrencyUntil->get();
        $repeatingCycleList = $this->repeatingCycle->get();

        $contract = $this->contract->where('id', $id)->where('user_id', Auth::id())->first();
        return view('customer.contract-preview')
            ->with(['repeatingCycleList' => $repeatingCycleList])
            ->with(['recurrencyList' => $recurrencyList])
            ->with(['priceList' => $priceList])
            ->with(['contract' => $contract]);
    }

    public function myCollectingForm($id, Request $request){
        session(['signaturePadBackUrl' => $request->fullUrl()]);

        if(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3 ){
            $collectingFormList = $this->collectingForm->where('customer_id', $id)->orderBy('extraction_date', 'DESC')->get();
        } elseif(Auth::user()->role_id == 4){
            $collectingFormList = $this->collectingForm->where('contract_id', $id)->where('customer_id', Auth::id())->orderBy('extraction_date', 'DESC')->get();
        }

        foreach($collectingFormList as $collectingForm){
            $customer = $collectingForm->customer;
        }

        if (empty($collectingForm->customer)){
            return back()->with('warning', 'Nenašli sa žiadne zberné listy.');
        }

        return view('customer.collecting-form-list')
            ->with(['customer' => $customer])
            ->with(['collectingFormList' => $collectingFormList]);
    }

    public function myCompanies(){
        return view('customer.company-list');
    }

    public function subsidiary($id){
        $user = $this->user->where('id', $id)->where('parent_id', Auth::id())->first();
        if(null == $user){
            return redirect('/');
        }

        return view('customer.subsidiary')->with(['user' => $user]);
    }

    public function acceptanceProtocolList($id){
        if(Auth::user()->role_id == 4){
            $contract = $this->contract->where('id', $id)->where('user_id', Auth::id())->first();
        } elseif(Auth::user()->role_id == 1 || Auth::user()->role_id == 2 || Auth::user()->role_id == 3){
            $contract = $this->contract->where('id', $id)->first();
        }

        return view('customer.acceptance-protocol-list')->with(['contract' => $contract]);
    }
}
