<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function getSession(Request $request) {
        $currentUrl = $request->currentUrl;

        if(Session::has('search')){
            $searchValue = Session::get('search');

            foreach($searchValue as $key=> $value){
                if($key === $currentUrl){
                    return $value ;
                }
            }
        }

        return '';
    }

    public function setSession(Request $request) {
        $currentUrl = $request->currentUrl;
        $currentPage = $request->currentPage;
        $searchValue = $request->searchValue;

        if(Session::has('search')){
            $search = Session::get('search');
            $search[$currentUrl] = ['currentPage' => $currentPage, 'searchValue' => $searchValue];
            return Session::put('search', $search);
        }

        return Session::put('search', [$currentUrl => ['currentPage' => $currentPage, 'searchValue' => $searchValue]]);
    }
}
