<?php

namespace App\Http\Controllers;

use App\User;
use App\Contract;
use App\CollectingForm;
use Illuminate\Http\Request;

class CollectingFormController extends Controller
{
    protected $user;

    protected $contract;

    protected $collectingForm;

    public function __construct() {
        $this->user = new User();
        $this->contract = new Contract();
        $this->collectingForm = new CollectingForm();
    }

    public function editCollectingForm ($id) {
        $collectingForm = $this->collectingForm->where('id', $id)->first();
        $driverList = $this->user->where('role_id', '3')->get();
        $contract = $collectingForm->contract;

        $contractCollectingForms = $this->collectingForm->getContractMonthDataRemovalConfirmation($collectingForm->contract_id);

        return view('driver.removal-confirmation')
            ->with(['editing' => true])
            ->with(['contract' => $contract])
            ->with(['collectingForm' => $collectingForm])
            ->with(['contractCollectingForms' => $contractCollectingForms])
            ->with(['driverList' => $driverList]);
    }

    public function saveCollectingForm(Request $request) {
        $this->collectingForm->validate($request);
        $this->collectingForm->insertUpdate($request);

        return back()->with('success', 'Uloženie prebehlo úspešne.');
    }

    public function deleteCollectingForm(Request $request) {
        $collectingForm = $this->collectingForm->where('id', $request->id)->first();
        $collectingForm->delete();

        return (['success' => 'Zberný list bol vymazaný']);
    }
}
