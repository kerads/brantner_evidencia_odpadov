<?php

namespace App;

use Mail;
use App\Mail\welcome;
use App\Mail\collecting_form;
use Illuminate\Database\Eloquent\Model;

class SendMail extends Model
{
    public function mailWelcome($email){
        $message = (new welcome())
        ->subject('Vytvorenie reistrácie - evidenica odpadov Brantner Poprad');

        Mail::to($email)->queue($message);
    }

    public function mailCollectingForm($email){
        $message = (new collecting_form())
            ->subject('Zberný list  - evidenica odpadov Brantner Poprad');

        Mail::to($email)->queue($message);
    }
}
