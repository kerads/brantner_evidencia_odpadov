<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;

class Storage extends Model
{
    protected $table = 'storage';

    protected $fillable = ['toilet_paper','hygienic_handkerchiefs', 'kitchen_towels', 'oil', 'is_summary', 'summary_date', 'change_date'];

    public function insertUpdate($request){
        if(isset($record->id)){
            $record = $this->where('id', $request->id)->first();
        } else{
            $record = new Storage();
        }

        try{
            $record->toilet_paper = isset($request->toilet_paper) ? $request->toilet_paper : 0;
            $record->hygienic_handkerchiefs = isset($request->hygienic_handkerchiefs) ? $request->hygienic_handkerchiefs : 0;
            $record->kitchen_towels = isset($request->kitchen_towels) ? $request->kitchen_towels : 0;
            $record->oil = isset($request->oil) ? $request->oil : 0;
            $record->is_summary = isset($request->is_summary) ? $request->is_summary : 0;
            $record->summary_date = isset($request->summary_date) ? $request->summary_date : null;
            $record->change_date = isset($request->change_date) ? $request->change_date : null;

            $record->save();
        } catch(Exception $e) {
            if($e){
                return 'error';
            }
        }

        return $record;
    }

    public function createMonthRecord($data){
        $date = date('Y-m-01', strtotime('+1 month', strtotime($data->summary_date)));

        $monthRecord = new Storage();

        $monthRecord->toilet_paper = $data->toilet_paper;
        $monthRecord->hygienic_handkerchiefs = $data->hygienic_handkerchiefs;
        $monthRecord->kitchen_towels = $data->kitchen_towels;
        $monthRecord->oil = $data->oil;
        $monthRecord->is_summary = 1;
        $monthRecord->summary_date = $date;
        $monthRecord->change_date = $date;

        $monthRecord->save();

        return $monthRecord;
    }
}
