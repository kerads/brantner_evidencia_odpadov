<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceList extends Model
{
    protected $table = 'price_list';

    protected $fillable = [
        'name', 'price', 'ks', 'contract', 'billing'
    ];

    public function inserUpdate($request){
        $priceList = new PriceList();
        $price = $priceList->where('id', $request->id)->first();

        switch ($request->name){
            case 'price':
                $price->price = $request->value;
            case 'ks':
                $price->ks = $request->value;
            case 'contract':
                $price->contract = $request->value;
        }
        return $price->save();
    }
}
