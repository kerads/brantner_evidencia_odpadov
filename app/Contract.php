<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $fillable = [
        'id', 'user_id', 'branch_name', 'adresa', 'psc', 'mesto', 'validity_recurrency_until_id', 'start_date',
        'recurrency_until_date', 'recurrency_num_of_repetitions', 'repeating_cycle_id', 'recurrency_week_days',
        'min_num_barels', 'num_barels', 'max_num_barels', 'stored_barels_30', 'stored_barels_60', 'barel_size', 'price_type', 'price_id', 'discount', 'custom_price', 'custom_above_quantity_price',
        'custom_desinfection', 'custom_desinfection_price', 'above_quantity_price', 'custom_oil_price', 'custom_fat_price', 'contract', 'company_stamp', 'external_id', 'poznamka', 'closed', 'closed_at', 'deleted'
    ];

    public $repetitionsPerMonth;

    public $billingPerMonth;

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function price(){
        return $this->hasOne('App\PriceList', 'id', 'price_id');
    }

    public function repeatingCycle(){
        return $this->hasOne('App\RepeatingCycle', 'id', 'repeating_cycle_id');
    }

    public function collectingForm(){
        return $this->hasMany('App\CollectingForm', 'contract_id', 'id')->orderBy('id', 'DESC');
    }

    public function lastCollectingForm(){
        return $this->hasOne('App\CollectingForm', 'contract_id', 'id')->latest();
    }

    public function customWasteType(){
        return $this->hasMany('App\CustomWasteType', 'contract_id', 'id');
    }

    public function storageBarelMovement(){
        return $this->hasMany('App\StorageBarelMovement', 'contract_id', 'id');
    }

    public function validate($request){
        $request->validate([
            'user_id' => 'required',
            'adresa' => 'required|max:255',
            'psc' => 'required|max:10',
            'mesto' => 'required|max:45',
            'validity_recurrency_until_id' => 'required|not_in:-1',
            'start_date' => 'required|date',
            'recurrency_until_date' => 'required_if:validity_recurrency_until_id,2',
            'recurrency_num_of_repetitions' => 'required_if:validity_recurrency_until_id,2',
            'repeating_cycle_id' => 'required|not_in:-1',
            'price_type' => 'required',
            'num_barels' => 'required|numeric',
            'external_id' => 'nullable|numeric',
            'barel_size' => 'required|not_in:-1',
        ]);

        if(!($request->stored_barels_30 > 0) && !($request->stored_barels_60 > 0)){
            $request->validate([
                'stored_barels_30' => 'required|numeric',
                'stored_barels_60' => 'required|numeric',
            ]);
        }

        if($request->price_type == 1){
            $request->validate([
                'price_id' => 'required|numeric',
                'discount' => 'required|numeric|min:0|max:100',
            ]);
        } elseif($request->price_type == 2 || $request->price_type == 3) {
            $request->validate([
                'custom_price' => 'required|numeric',
            ]);
        } elseif($request->price_type == 4) {
            $request->validate([
                'custom_price' => 'required|numeric',
                'min_num_barels' => 'required|numeric|min:1|lte:num_barels',
                'num_barels' => 'required|numeric|min:1|gte:min_num_barels|lte:max_num_barels',
                'max_num_barels' => 'required|numeric|min:1|gte:num_barels',
            ]);
        }

        if($request->custom_above_quantity_price == 1){
            $request->validate([
                'above_quantity_price' => 'required|numeric',
            ]);
        }

        if($request->custom_desinfection == 1){
            $request->validate([
                'custom_desinfection_price' => 'required|numeric',
            ]);
        }

        if($request->oil_switcher == 1){
            $request->validate([
                'custom_oil_price' => 'required|numeric',
            ]);
        }

        if($request->fat_switcher == 1){
            $request->validate([
                'custom_fat_price' => 'required|numeric',
            ]);
        }

        if($request->contract){
            $request->validate([
                'contract' => 'mimes:pdf',
            ]);
        }

        if($request->company_stamp){
            $request->validate([
                'company_stamp' => 'mimes:jpeg,bmp,png,gif,svg',
            ]);
        }
    }

    public function insertUpdate($request){
        if(isset($request->id)){
            $contract = $this->where('id', $request->id)->first();
        } else {
            $contract = new Contract();
        }

        $contract->user_id = $request->user_id;
        $contract->branch_name = $request->branch_name;
        $contract->adresa = $request->adresa;
        $contract->psc = $request->psc;
        $contract->mesto = $request->mesto;
        $contract->validity_recurrency_until_id = $request->validity_recurrency_until_id;
        $contract->start_date = $request->start_date;
        $contract->recurrency_until_date = $request->recurrency_until_date;
        $contract->recurrency_num_of_repetitions = $request->recurrency_num_of_repetitions;
        $contract->repeating_cycle_id = $request->repeating_cycle_id;
        $contract->recurrency_week_days = isset($request->recurrency_week_days) && !empty($request->recurrency_week_days) ? $this->arrayToString($request->recurrency_week_days) : '';
        $contract->min_num_barels = isset($request->min_num_barels) ? $request->min_num_barels : null;
        $contract->num_barels = $request->num_barels;
        $contract->max_num_barels = isset($request->max_num_barels) ? $request->max_num_barels : null;
        $contract->stored_barels_30 = $request->stored_barels_30;
        $contract->stored_barels_60 = $request->stored_barels_60;
        $contract->barel_size = $request->barel_size;
        $contract->price_type = $request->price_type;
        $contract->price_id = $request->price_type == 1 ? $request->price_id : '-1';
        $contract->discount = $request->price_type == 1 ? $request->discount : 0 ;
        $contract->custom_price =  $request->price_type != 1 ? $request->custom_price : null;
        $contract->custom_above_quantity_price = $request->custom_above_quantity_price;
        $contract->custom_desinfection = $request->custom_desinfection == 1 ? $request->custom_desinfection : 0 ;
        $contract->custom_desinfection_price = $request->custom_desinfection == 1 ? $request->custom_desinfection_price : null ;
        $contract->above_quantity_price = $request->custom_above_quantity_price == 1 ? $request->above_quantity_price : 0;
        $contract->custom_oil_price = $request->oil_switcher == 1 ? $request->custom_oil_price : null;
        $contract->custom_fat_price = $request->fat_switcher == 1 ? $request->custom_fat_price : null;
        $contract->external_id = $request->external_id;
        $contract->poznamka = $request->poznamka;

        // Save the PDF contract
        if($request->hasFile('contract')){
            $filenameWithExt = $request->file('contract')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('contract')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $request->file('contract')->storeAs('public/zmluvy/' . $request->user_id,$fileNameToStore);

            $contract->contract = $fileNameToStore ;
        }

        // Save the
        if($request->hasFile('company_stamp')){
            $filenameWithExt = $request->file('company_stamp')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('company_stamp')->getClientOriginalExtension();
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            $request->file('company_stamp')->storeAs('public/firemne-peciatky/' . $request->user_id,$fileNameToStore);

            $contract->company_stamp = $fileNameToStore ;
        }

        $contract->save();

        return $contract;
    }

    // get array return string
    public function arrayToString($array){
        return implode(",", $array);
    }

    // get string return array
    public function stringToArray($string){
        return explode(",", $string);
    }

    public function delContract($id){
        $contract = $this->where('id', $id)->first();
        $contract->deleted = 1;
        $contract->save();

        return $id;
    }

    public function close($request){
        $contract = $this->where('id', $request->id)->first();
        $contract->closed = '1';
        $contract->closed_at = $request->closed_at;
        $contract->save();

        return ($contract);
    }
}
