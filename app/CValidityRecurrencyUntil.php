<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CValidityRecurrencyUntil extends Model
{
    protected $fillable=['name'];
}
