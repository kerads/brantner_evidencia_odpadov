<?php

namespace App\Exports;

use Excel;
use App\CollectingForm;
use App\Exports\Sheets\StockRecordsFirstSheet;
use App\Exports\Sheets\StockRecordsSecondSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class StockRecordsExport implements WithMultipleSheets
{
    public function sheets(): array
    {
        $collectingForm = new CollectingForm();
        $collectingFormList = $collectingForm->getExcelData($_GET['year_month']);


        $sheets = [];

        $sheets[1] = new StockRecordsFirstSheet($collectingFormList);
        $sheets[2] = new StockRecordsSecondSheet($collectingFormList);

        return $sheets;
    }
}
