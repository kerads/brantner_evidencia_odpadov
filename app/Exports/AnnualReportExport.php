<?php

namespace App\Exports;

use Excel;
use App\CollectingForm;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use App\Exports\Sheets\AnnualReportFirstSheet;
use App\Exports\Sheets\AnnualReportSecondSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class AnnualReportExport implements WithMultipleSheets, WithEvents
{
    public function registerEvents() : array{
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $protection = $event->getSheet()->getDelegate()->getProtection();

                $protection->setSheet(true);
                $protection->setSort(true);
                $protection->setInsertRows(true);
                $protection->setFormatCells(true);
            },

        ];
    }

    public function sheets(): array
    {
        $collectingForm = new CollectingForm();
        $data = $collectingForm->yearWasteAnnualReport($_GET['year'], $_GET['customer_id']);

        $sheets = [];

        $sheets[1] = new AnnualReportFirstSheet($data);
        $sheets[2] = new AnnualReportSecondSheet($data);

        return $sheets;
    }
}
