<?php

namespace App\Exports;

use App\CollectingForm;
use App\Exports\Sheets\RecordsSheetFirstSheet;
use App\Exports\Sheets\RecordsSheetSecondSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class RecordSheetsExport implements WithMultipleSheets
{
    public function sheets(): array
    {
        $collectingForm = new CollectingForm();
        $collectingFormList = $collectingForm->getRecordsUserExcelData($_GET['year'], $_GET['user_id']);

        $sheets = [];

        $sheets[1] = new RecordsSheetFirstSheet($collectingFormList);
        $sheets[2] = new RecordsSheetSecondSheet($collectingFormList);

        return $sheets;
    }
}
