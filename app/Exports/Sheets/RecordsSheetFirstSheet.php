<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class RecordsSheetFirstSheet implements FromView, WithColumnWidths, WithEvents
{
    protected $data;

    protected $percent = 0.1;

    protected function getPercent($width){
        return $width * $this->percent;
    }

    protected $w1 = 34.5;
    protected $w2 = 57.525;
    protected $w3 = 16.5;
    protected $w4 = 6.9;
    protected $w5 = 2.175;
    protected $w6 = 10.5;
    protected $w7 = 3.45;
    protected $w8 = 54;
    protected $w9 = 44.475;
    protected $w10 = 8.475;
    protected $w11 = 5.925;
    protected $w12 = 9;
    protected $w13 = 15.975;
    protected $w14 = 20.025;
    protected $w15 = 18;

    public function __construct($data){
        $this->data = $data;
    }

    public function registerEvents() : array{
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->styleCells(
                    'A1:BZ70',
                    [
                        'font' => [
                            'name' => 'Arial',
                            'size' => 6,
                            'bold'      =>  false,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                );
            },
        ];
    }

    public function view(): View
    {

        return view('exports.report-sheets-1',
                [
                    'data' => $this->data
                ]
        );
    }

    public function columnWidths(): array
    {
        return [
            'A' => $this->getPercent($this->w1),
            'B' => $this->getPercent($this->w2),
            'C' => $this->getPercent($this->w3),
            'D' => $this->getPercent($this->w4),
            'E' => $this->getPercent($this->w5),
            'F' => $this->getPercent($this->w6),
            'G' => $this->getPercent($this->w7),
            'H' => $this->getPercent($this->w5),
            'I' => $this->getPercent($this->w8),
            'J' => $this->getPercent($this->w9),
            'K' => $this->getPercent($this->w10),
            'L' => $this->getPercent($this->w11),
            'M' => $this->getPercent($this->w12),
            'N' => $this->getPercent($this->w13),
            'O' => $this->getPercent($this->w13),
            'P' => $this->getPercent($this->w6),
            'Q' => $this->getPercent($this->w14),
            'R' => $this->getPercent($this->w14),
            'S' => $this->getPercent($this->w14),
            'T' => $this->getPercent($this->w14),
            'U' => $this->getPercent($this->w14),
            'V' => $this->getPercent($this->w14),
            'W' => $this->getPercent($this->w14),
            'X' => $this->getPercent($this->w14),
            'Y' => $this->getPercent($this->w14),
            'Z' => $this->getPercent($this->w14),
            'AA' => $this->getPercent($this->w14),
            'AB' => $this->getPercent($this->w14),
            'AC' => $this->getPercent($this->w14),
            'AD' => $this->getPercent($this->w14),
            'AE' => $this->getPercent($this->w14),
            'AF' => $this->getPercent($this->w14),
            'AG' => $this->getPercent($this->w14),
            'AH' => $this->getPercent($this->w14),
            'AI' => $this->getPercent($this->w14),
            'AJ' => $this->getPercent($this->w14),
            'AK' => $this->getPercent($this->w14),
            'AL' => $this->getPercent($this->w14),
            'AM' => $this->getPercent($this->w14),
            'AN' => $this->getPercent($this->w14),
            'AO' => $this->getPercent($this->w14),
            'AP' => $this->getPercent($this->w14),
            'AQ' => $this->getPercent($this->w15),
            'AR' => $this->getPercent($this->w15),
            'AS' => $this->getPercent($this->w15),
        ];
    }
}
