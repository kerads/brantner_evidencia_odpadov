<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\BeforeSheet;

class CollectingFormFirstSheet implements FromView
{
    protected $data;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function registerEvents() : array{
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->styleCells(
                    'A1:BZ70',
                    [
                        'font' => [
                            'name' => 'Arial',
                            'size' => 8,
                            'bold'      =>  false,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                );
            },
        ];
    }

    public function view(): View
    {
        return view('exports.collecting-form-sheet-1',
            [
                'data' => $this->data
            ]
        );
    }
}
