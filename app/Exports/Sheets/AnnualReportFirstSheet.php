<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class AnnualReportFirstSheet implements FromView, WithColumnWidths, WithEvents
{
    protected $data;

    protected $percent = 0.9;

    protected function getPercent($width){
        return $width * $this->percent;
    }

    protected $w1 = 0;
    protected $w2 = 1;
    protected $w3 = 1.2;
    protected $w4 = 1.47;
    protected $w5 = 1.01;
    protected $w6 = 1.26;
    protected $w7 = 1.54;
    protected $w8 = 2.13;

    public function __construct($data){
        $this->data = $data;
    }

    public function registerEvents() : array{
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->styleCells(
                    'A1:BZ70',
                    [
                        'font' => [
                            'name' => 'Arial',
                            'size' => 8,
                            'bold'      =>  false,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                );
            },
        ];
    }

    public function view(): View
    {

        return view('exports.annual-report-sheet-1',
                [
                    'data' => $this->data
                ]
        );
    }

    public function columnWidths(): array
    {
        return [
            'A' => $this->getPercent($this->w1),
            'B' => $this->getPercent($this->w2),
            'C' => $this->getPercent($this->w3),
            'D' => $this->getPercent($this->w3),
            'E' => $this->getPercent($this->w3),
            'F' => $this->getPercent($this->w3),
            'G' => $this->getPercent($this->w3),
            'H' => $this->getPercent($this->w3),
            'I' => $this->getPercent($this->w3),
            'J' => $this->getPercent($this->w3),
            'K' => $this->getPercent($this->w3),
            'L' => $this->getPercent($this->w3),
            'M' => $this->getPercent($this->w3),
            'N' => $this->getPercent($this->w3),
            'O' => $this->getPercent($this->w3),
            'P' => $this->getPercent($this->w3),
            'Q' => $this->getPercent($this->w3),
            'R' => $this->getPercent($this->w3),
            'S' => $this->getPercent($this->w3),
            'T' => $this->getPercent($this->w3),
            'U' => $this->getPercent($this->w4),
            'V' => $this->getPercent($this->w4),
            'W' => $this->getPercent($this->w6),
            'X' => $this->getPercent($this->w7),
            'Y' => $this->getPercent($this->w6),
            'Z' => $this->getPercent($this->w8),
            'AA' => $this->getPercent($this->w5),
            'AB' => $this->getPercent($this->w5),
            'AC' => $this->getPercent($this->w5),
            'AD' => $this->getPercent($this->w5),
            'AE' => $this->getPercent($this->w5),
            'AF' => $this->getPercent($this->w5),
            'AG' => $this->getPercent($this->w5),
            'AH' => $this->getPercent($this->w5),
            'AI' => $this->getPercent($this->w5),
            'AJ' => $this->getPercent($this->w3),
            'AK' => $this->getPercent($this->w3),
            'AL' => $this->getPercent($this->w3),
            'AM' => $this->getPercent($this->w3),
            'AN' => $this->getPercent($this->w3),
            'AO' => $this->getPercent($this->w3),
            'AP' => $this->getPercent($this->w3),
            'AQ' => $this->getPercent($this->w3),
            'AR' => $this->getPercent($this->w3),
            'AS' => $this->getPercent($this->w3),
            'AT' => $this->getPercent($this->w3),
            'AU' => $this->getPercent($this->w3),
            'AV' => $this->getPercent($this->w3),
            'AW' => $this->getPercent($this->w3),
            'AX' => $this->getPercent($this->w3),
            'AY' => $this->getPercent($this->w3),
            'AZ' => $this->getPercent($this->w3),
            'BA' => $this->getPercent($this->w3),
            'BB' => $this->getPercent($this->w3),
            'BC' => $this->getPercent($this->w3),
            'BD' => $this->getPercent($this->w3),
            'BE' => $this->getPercent($this->w3),
            'BF' => $this->getPercent($this->w3),
            'BG' => $this->getPercent($this->w3),
            'BH' => $this->getPercent($this->w4),
            'BI' => $this->getPercent($this->w4),
            'BJ' => $this->getPercent($this->w4),
            'BK' => $this->getPercent($this->w4),
            'BL' => $this->getPercent($this->w4),
            'BM' => $this->getPercent($this->w4),
            'BN' => $this->getPercent($this->w4),
            'BO' => $this->getPercent($this->w4),
            'BP' => $this->getPercent($this->w4),
            'BQ' => $this->getPercent($this->w4),
            'BR' => $this->getPercent($this->w4),
            'BS' => $this->getPercent($this->w4),
            'BT' => $this->getPercent($this->w4),
            'BU' => $this->getPercent($this->w4),
            'BV' => $this->getPercent($this->w4),
            'BW' => $this->getPercent($this->w4),
            'BX' => $this->getPercent($this->w3),
            'BY' => $this->getPercent($this->w3),
            'BZ' => $this->getPercent($this->w3),
            'CA' => $this->getPercent($this->w3),
            'CB' => $this->getPercent($this->w3),
            'CC' => $this->getPercent($this->w3),
            'CD' => $this->getPercent($this->w3),
            'CE' => $this->getPercent($this->w3),
            'CF' => $this->getPercent($this->w3),
        ];
    }
}
