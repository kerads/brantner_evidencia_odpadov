<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\BeforeSheet;

class StockRecordsSecondSheet implements FromView
{
    protected $data;

    public function __construct($data){
        $this->data = $data;
    }

    public function registerEvents() : array{
        return [
            BeforeSheet::class => function (BeforeSheet $event) {
                $event->sheet->styleCells(
                    'A1:BZ70',
                    [
                        'font' => [
                            'name' => 'Arial',
                            'size' => 8,
                            'bold'      =>  false,
                            'color' => ['argb' => '000000'],
                        ],
                    ]
                );
            },
        ];
    }

    public function view(): View
    {

        return view('exports.stock-records-sheet-2',
            [
                'data' => $this->data
            ]
        );
    }
}
