<?php

namespace App\Exports;

use Excel;
use App\CollectingForm;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Exports\Sheets\CollectingFormFirstSheet;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CollectingFormListExport implements WithMultipleSheets
{
    public function sheets(): array
    {
        $collectingForm = new CollectingForm();

        $from = isset($_GET['date_from']) ? $_GET['date_from'] : date("Y-m-d 00:00:00");
        $to = isset($_GET['date_to']) ? $_GET['date_to'] : date("Y-m-d 23:59:59");

        $collectingForms = $collectingForm->whereBetween('extraction_date', [$from, $to])->get();


        $sheets = [];

        $sheets[1] = new CollectingFormFirstSheet($collectingForms);

        return $sheets;
    }
}
