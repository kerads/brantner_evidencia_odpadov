@extends('layouts.master')

@section('title')
    @if(isset($collectingForm))
        Úprava zberného listu / {{ $collectingForm->id }}
    @else
        Potvrdenie o prevzatí
    @endif
@endsection

@section('content')
    <div class="row col-12">
        <form action="{{ url(isset($collectingForm) ? 'save-collecting-form' : 'save-removal-confirmation') }}" class="col-12" method="POST" id="collecting-form">
            @csrf
            @if(isset($collectingForm))
                <input type="hidden" name="id" value="{{ $collectingForm->id }}">
            @endif
            <input type="hidden" name="contract_id" value="{{ $contract->id }}">
            <input type="hidden" name="customer_id" value="{{ $contract->user_id }}">
            <input type="hidden" name="lat" value="{{ isset($collectingForm) ? $collectingForm->lat : '' }}" id="lat">
            <input type="hidden" name="lng" value="{{ isset($collectingForm) ? $collectingForm->lng : '' }}" id="lng">
            <div class="col=12 col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        @if(isset($collectingForm))
                            Úprava zberného listu / {{ $collectingForm->id }}
                        @else
                            Potvrdenie o prevzatí
                        @endif
                    </div>

                    <div class="card-body">

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Názov prevádzky: </div>

                            <div class="col-md-8">
                                {{ null != $contract->name ? $contract->name : $contract->user->name }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Ulica: </div>

                            <div class="col-md-8">
                                {{ $contract->adresa }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">PSČ: </div>

                            <div class="col-md-8">
                                {{ $contract->psc }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Mesto: </div>

                            <div class="col-md-8">
                                {{ $contract->mesto }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">IČO: </div>

                            <div class="col-md-8">
                                {{ $contract->user->ico }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">DIČ: </div>

                            <div class="col-md-8">
                                {{ $contract->user->dic }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Odporúčané zn. na sklade(30L/60L): </div>

                            <div class="col-md-8">
                                {{ $contract->stored_barels_30 }} / {{ $contract->stored_barels_60 }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Uskladnené zn.(30L/60L): </div>

                            <div class="col-md-8">
                                {{ $contract->storageBarelMovement->sum('barel_30') }} / {{ $contract->storageBarelMovement->sum('barel_60') }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Počet vyzbieraných sudov za mesiac: </div>

                            <div class="col-md-8">
                                {{ $contractCollectingForms->num_barels }} / {{ $contractCollectingForms->contract->maxBarelsPerMonth }}
                            </div>
                        </div>

                        <hr>

                        @if(Auth::user()->role_id != 3)
                            <div class="form-group row">
                                <label for="driver_id" class="col-md-4 col-form-label text-md-right">
                                    Vodič
                                </label>

                                <div class="col-md-8">
                                    <select name="driver_id"
                                            class="form-control @error('driver_id') alert alert-danger @enderror">
                                        @error('driver_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <option value=""></option>
                                        @foreach($driverList as $driver)
                                            <option value="{{ $driver->id }}"
                                                {{ old('driver_id', isset($collectingForm) ? $collectingForm->driver_id : '')  == $driver->id  ? 'selected' : '' }}>
                                                {{ $driver->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="driver_id" value="{{ Auth::user()->id }}">
                        @endif

                        <div class="form-group row">
                            <label for="extraction_date" class="col-md-4 col-form-label text-md-right">
                                Dátum odovzdania
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="datetime-local"
                                    class="form-control @error('extraction_date') alert alert-danger @enderror "
                                    name="extraction_date"
                                    value="{{ old('extraction_date', isset($collectingForm) ? date('Y-m-d\TH:i', strtotime($collectingForm->extraction_date)) : date('Y-m-d\TH:i'))}}"
                                >
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="waste_switcher" class="col-md-4 col-form-label text-md-right">
                                Odpad
                            </label>
                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="hidden" name="waste_switcher" value="0">
                                    <input type="checkbox" name="waste_switcher" value="1" id="waste_switcher" {{ old('waste_switcher', (isset($collectingForm) && $collectingForm->waste_weight)) == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row waste-group">
                            <label for="driver_id" class="col-md-4 col-form-label text-md-right">
                                druh odpadu
                            </label>

                            <div class="col-md-8">
                                <select name="waste_type"
                                        class="form-control @error('waste_type') alert alert-danger @enderror">
                                    @error('waste_type')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <option value="-1" {{!isset($collectingForm) || $collectingForm->waste_type !== '-1'}}>20 01 08 - biologicky rozložiteľný kuchynský a reštauračný odpad</option>
                                    @foreach($contract->customWasteType as $wasteType)
                                        <option value="{{ $wasteType->id }}"
                                            {{ old('waste_type', isset($collectingForm) ? $collectingForm->waste_type : '')  == $wasteType->id  ? 'selected' : '' }}
                                        >{{ $wasteType->code }} - {{ $wasteType->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row waste-group">
                            <label for="num_barels" class="col-md-4 col-form-label text-md-right">
                                Počet sudov KS
                            </label>
                            <div class="col-md-8">
                                <input
                                    id="num_barels"
                                    type="number"
                                    step="1"
                                    min="0"
                                    max="{{$contractCollectingForms->contract->maxBarelsPerMonth - $contractCollectingForms->num_barels }}"
                                    class="form-control recurrency @error('num_barels') alert alert-danger @enderror"
                                    name="num_barels"
                                    value="{{ old('num_barels', isset($collectingForm) ? $collectingForm->num_barels : (!isset($contract) ? '' : $contract->num_barels))}}"
                                    data-editing="{{ isset($editing) && true === $editing ? 1 : 0 }}"
                                >
                            </div>
                        </div>

                        <div class="form-group row waste-group">
                            <label for="barel_size" class="col-md-4 col-form-label text-md-right">
                                Veľkosť sudov
                            </label>

                            <div class="col-md-8">
                                <select name="barel_size"
                                        class="form-control @error('barel_size') alert alert-danger @enderror">
                                    @error('barel_size')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <option value="-1"></option>
                                    <option value="30"
                                        {{ old('barel_size', isset($collectingForm) ? $collectingForm->barel_size : (isset($contract) ? $contract->barel_size : ''))  == 30  ? 'selected' : '' }}>
                                        30 L
                                    </option>
                                    <option value="60"
                                        {{ old('barel_size', isset($collectingForm) ? $collectingForm->barel_size : (isset($contract) ? $contract->barel_size : ''))  == 60  ? 'selected' : '' }}>
                                        60 L
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row waste-group">
                            <label for="waste_weight" class="col-md-4 col-form-label text-md-right">
                                Váha KG
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="number"
                                    step="0.01"
                                    min="0"
                                    class="form-control recurrency @error('waste_weight') alert alert-danger @enderror"
                                    name="waste_weight"
                                    value="{{ old('waste_weight', isset($collectingForm) ? $collectingForm->waste_weight : '')}}"
                                >
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="extra_waste_switcher" class="col-md-4 col-form-label text-md-right">
                                Nad zmluvné množstvo
                            </label>
                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="hidden" name="extra_waste_switcher" value="0">
                                    <input type="checkbox" name="extra_waste_switcher" value="1" id="extra_waste_switcher" {{ old('extra_waste_switcher',  (isset($collectingForm) && $collectingForm->extra_waste_weight)) == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row extra-waste-group">
                            <label for="extra_num_barels" class="col-md-4 col-form-label text-md-right">
                                Počet sudov KS
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('extra_num_barels') alert alert-danger @enderror"
                                    name="extra_num_barels"
                                    value="{{ old('extra_num_barels', isset($collectingForm) ? $collectingForm->extra_num_barels : (!isset($contract) ? '' : $contract->extra_num_barels)) }}"
                                >
                            </div>
                        </div>

                        <div class="form-group row extra-waste-group">
                            <label for="extra_barel_size" class="col-md-4 col-form-label text-md-right">
                                Veľkosť sudov
                            </label>

                            <div class="col-md-8">
                                <select name="extra_barel_size"
                                        class="form-control @error('extra_barel_size') alert alert-danger @enderror">
                                    @error('extra_barel_size')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <option value="-1"></option>
                                    <option value="30"
                                        {{ old('extra_barel_size', isset($collectingForm) ? $collectingForm->barel_size : (isset($contract) ? $contract->barel_size : ''))  == 30  ? 'selected' : '' }}>
                                        30 L
                                    </option>
                                    <option value="60"
                                        {{ old('extra_barel_size', isset($collectingForm) ? $collectingForm->barel_size : (isset($contract) ? $contract->barel_size : ''))  == 60  ? 'selected' : '' }}>
                                        60 L
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row extra-waste-group">
                            <label for="extra_waste_weight" class="col-md-4 col-form-label text-md-right">
                                Váha KG
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="number"
                                    step="0.01"
                                    min="0"
                                    class="form-control recurrency @error('extra_waste_weight') alert alert-danger @enderror"
                                    name="extra_waste_weight"
                                    value="{{ old('extra_waste_weight', isset($collectingForm) ? $collectingForm->extra_waste_weight : '') }}"
                                >
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="oil_switcher" class="col-md-4 col-form-label text-md-right">
                                Olej
                            </label>
                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="checkbox" name="oil_switcher" value="1" id="oil_switcher" {{ old('oil_switcher', (isset($collectingForm) && $collectingForm->oil_capacity)) == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row oil-group">
                            <label for="oil_capacity" class="col-md-4 col-form-label text-md-right">
                                Objem L
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="number"
                                    step="0.01"
                                    min="0.01"
                                    class="form-control recurrency @error('oil_capacity') alert alert-danger @enderror"
                                    name="oil_capacity"
                                    value="{{ old('oil_capacity', isset($collectingForm) ? $collectingForm->oil_capacity : '')}}"
                                >
                            </div>
                        </div>

                        <div class="form-group row oil-group">
                            <label for="oil_write_off" class="col-md-4 col-form-label text-md-right">
                                Odpis za olej
                            </label>
                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="checkbox" name="write_off" id="write_off" value="1" {{ old('write_off', (isset($collectingForm) ? $collectingForm->write_off : 0) ) == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row oil-write-off">
                            <label for="toilet_paper" class="col-md-4 col-form-label text-md-right">
                                Toaletný papier KS <span class="small text-success">(1L)</span>
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('toilet_paper') alert alert-danger @enderror"
                                    name="toilet_paper"
                                    value="{{ old('toilet_paper', isset($collectingForm) ? $collectingForm->toilet_paper : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <div class="form-group row oil-write-off">
                            <label for="kitchen_towels" class="col-md-4 col-form-label text-md-right">
                                Kuchynské utierky KS <span class="small text-success">(4L)</span>
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('kitchen_towels') alert alert-danger @enderror"
                                    name="kitchen_towels"
                                    value="{{ old('kitchen_towels', isset($collectingForm) ? $collectingForm->kitchen_towels : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <div class="form-group row oil-write-off">
                            <label for="hygienic_handkerchiefs" class="col-md-4 col-form-label text-md-right">
                                Hygienické vreckovky KS <span class="small text-success">(0.25L)</span>
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('hygienic_handkerchiefs') alert alert-danger @enderror"
                                    name="hygienic_handkerchiefs"
                                    value="{{ old('hygienic_handkerchiefs', isset($collectingForm) ? $collectingForm->hygienic_handkerchiefs : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <div class="form-group row oil-write-off">
                            <label for="oil_for_oil" class="col-md-4 col-form-label text-md-right">
                                Olej L
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('oil_for_oil') alert alert-danger @enderror"
                                    name="oil_for_oil"
                                    value="{{ old('oil_for_oil', isset($collectingForm) ? $collectingForm->oil_for_oil : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="fat_switcher" class="col-md-4 col-form-label text-md-right">
                                Tuk
                            </label>
                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="checkbox" name="fat_switcher" value="1" id="fat_switcher" {{ old('fat_switcher', (isset($collectingForm) && $collectingForm->fat_weight)) == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row fat-group">
                            <label for="fat_weight" class="col-md-4 col-form-label text-md-right">
                                Váha KG
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="number"
                                    step="0.01"
                                    min="0.01"
                                    class="form-control recurrency @error('fat_weight') alert alert-danger @enderror"
                                    name="fat_weight"
                                    value="{{ old('fat_weight', isset($collectingForm) ? $collectingForm->fat_weight : '' )}}"
                                >
                            </div>
                        </div>

                        <div class="form-group row fat-group">
                            <label for="fat_write_off" class="col-md-4 col-form-label text-md-right">
                                Odpis za tuk
                            </label>
                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="checkbox" name="fat_write_off" id="fat_write_off" value="1" {{ old('fat_write_off', (isset($collectingForm) ? $collectingForm->fat_write_off : 0) ) == 1 ? 'checked' : '' }}>
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row fat-write-off">
                            <label for="fat_toilet_paper" class="col-md-4 col-form-label text-md-right">
                                Toaletný papier KS <span class="small text-success">(1KG)</span>
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('fat_toilet_paper') alert alert-danger @enderror"
                                    name="fat_toilet_paper"
                                    value="{{ old('fat_toilet_paper', isset($collectingForm) ? $collectingForm->fat_toilet_paper : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <div class="form-group row fat-write-off">
                            <label for="fat_kitchen_towels" class="col-md-4 col-form-label text-md-right">
                                Kuchynské utierky KS <span class="small text-success">(4KG)</span>
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('fat_kitchen_towels') alert alert-danger @enderror"
                                    name="fat_kitchen_towels"
                                    value="{{ old('fat_kitchen_towels', isset($collectingForm) ? $collectingForm->fat_kitchen_towels : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <div class="form-group row fat-write-off">
                            <label for="fat_hygienic_handkerchiefs" class="col-md-4 col-form-label text-md-right">
                                Hygienické vreckovky KS <span class="small text-success">(0.25KG)</span>
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('fat_hygienic_handkerchiefs') alert alert-danger @enderror"
                                    name="fat_hygienic_handkerchiefs"
                                    value="{{ old('fat_hygienic_handkerchiefs', isset($collectingForm) ? $collectingForm->fat_hygienic_handkerchiefs : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <div class="form-group row fat-write-off">
                            <label for="fat_for_oil" class="col-md-4 col-form-label text-md-right">
                                Olej L
                            </label>
                            <div class="col-md-6">
                                <input
                                    type="number"
                                    step="1"
                                    min="0"
                                    class="form-control recurrency @error('fat_for_oil') alert alert-danger @enderror"
                                    name="fat_for_oil"
                                    value="{{ old('fat_for_oil', isset($collectingForm) ? $collectingForm->fat_for_oil : 0 )}}"
                                >
                            </div>
                            <div class="col-md-2">

                            </div>
                        </div>

                        <hr>

                        <div class="d-flex row justify-content-around">
                            <span id="location">GPS: {{ isset($collectingForm) ? $collectingForm->lat : '' }}, {{ isset($collectingForm) ? $collectingForm->lng : '' }}</span>
                        </div>

                        <div class="d-flex row justify-items-between mt-2 mb-2">
                            <div class="col-md-6">
                                <a href="{{ Session::get('signaturePadBackUrl') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-primary" id="submit-btn">
                                    <i class="fa fa-file-signature"> {{ isset($collectingForm) ? 'Uložiť zmeny' : 'Podpísať' }}</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        $('input[name="waste_switcher"]').on('change', function(){
            wasteSwitcher();
        });

        wasteSwitcher();

        function wasteSwitcher(){
            let waste = $('input[name="waste_switcher"]:checked').length;

            if(waste == 1){
                $('.waste-group').fadeIn();
            } else{
                $('.waste-group').fadeOut();
            }
        }

        $('input[name="extra_waste_switcher"]').on('change', function(){
            extraWasteSwitcher();
        });

        extraWasteSwitcher();

        function extraWasteSwitcher(){
            let extraWaste = $('input[name="extra_waste_switcher"]:checked').length;

            if(extraWaste == 1){
                $('.extra-waste-group').fadeIn();
            } else{
                $('.extra-waste-group').fadeOut();
            }
        }

        $('input[name="oil_switcher"]').on('change', function(){
            oilSwitcher();
        });

        oilSwitcher();

        function oilSwitcher(){
            let oil = $('input[name="oil_switcher"]:checked').length;

            if(oil == 1){
                $('.oil-group').fadeIn();
            } else{
                $('.oil-group').fadeOut();
            }

            oilWriteOff();
        }

        $('input[name="oil_write_off"]').on('change', function(){
            oilWriteOff();
        });

        function oilWriteOff(){
            let oil = $('input[name="oil_switcher"]:checked').length;
            let oilWriteOff = $('input[name="oil_write_off"]:checked').length;

            if(oil == 1 && oilWriteOff == 0){
                $('.oil-write-off').fadeIn();
            } else{
                $('.oil-write-off').fadeOut();
            }
        }

        $('input[name="fat_switcher"]').on('change', function(){
            fatSwitcher();
        });

        fatSwitcher();

        function fatSwitcher(){
            let fat = $('input[name="fat_switcher"]:checked').length;

            if(fat == 1){
                $('.fat-group').fadeIn();
            } else{
                $('.fat-group').fadeOut();
            }

            fatWriteOff();
        }

        $('input[name="fat_write_off"]').on('change', function(){
            fatWriteOff();
        });

        function fatWriteOff(){
            let fat = $('input[name="fat_switcher"]:checked').length;
            let writOff = $('input[name="fat_write_off"]:checked').length;

            if(fat == 1 && writOff == 0){
                $('.fat-write-off').fadeIn();
            } else{
                $('.fat-write-off').fadeOut();
            }
        }

        $('#submit-btn').click(function(){
            $( "#collecting-form" ).submit();
            $(this).prop("disabled", true);
        })
    </script>

{{--    Location lat lng--}}
    <script>
        var x = document.getElementById("location");

        getLocation();
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Tento prehliadač nepodporuje geolokáciu";
            }
        }

        function showPosition(position) {
            let lat = $('#lat').val();
            let lng = $('#lng').val();

            if( (lat == '') && (lng == '')) {
                x.innerHTML = "GPS: Lat: " + position.coords.latitude + ", Lng: " + position.coords.longitude;
                $('#lat').val(position.coords.latitude);
                $('#lng').val(position.coords.longitude);
            }
        }
    </script>

    <script>
        $('input[name="num_barels"]').on('change', function(){
            let isEditing = $(this).data('editing');
            let numBarels = parseInt($(this).val())
            let maxNumBarels = parseInt($(this).attr('max'))

            if(!isEditing && (numBarels > maxNumBarels)) {

                $(this).val(maxNumBarels)

                $('#extra_waste_switcher').attr("checked", "checked")
                extraWasteSwitcher()

                $('input[name="extra_num_barels"]').val(numBarels - maxNumBarels)
            }
        });
    </script>
@endsection
