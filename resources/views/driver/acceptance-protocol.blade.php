@extends('layouts.master')

@section('title')
    Preberací protokol
@endsection

@section('content')
    <div class="row col-12">
        <form action="{{ url('save-acceptance-protocol') }}" class="col-12" method="POST" id="collecting-form">
            @csrf
            <input type="hidden" name="contract_id" value="{{ $contract->id }}">
            <input type="hidden" name="user_id" value="{{ $contract->user_id }}">
            <input type="hidden" name="lat" value="" id="lat">
            <input type="hidden" name="lng" value="" id="lng">
            <div class="col=12 col-lg-6 col-md-6">
                <div class="card">
                    <div class="card-header">
                        Preberací protokol
                    </div>

                    <div class="card-body">

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Názov prevádzky: </div>

                            <div class="col-md-8">
                                {{ null != $contract->name ? $contract->name : $contract->user->name }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Ulica: </div>

                            <div class="col-md-8">
                                {{ $contract->adresa }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">PSČ: </div>

                            <div class="col-md-8">
                                {{ $contract->psc }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Mesto: </div>

                            <div class="col-md-8">
                                {{ $contract->mesto }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">IČO: </div>

                            <div class="col-md-8">
                                {{ $contract->user->ico }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">DIČ: </div>

                            <div class="col-md-8">
                                {{ $contract->user->dic }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Predpokladaný počet sudov na sklade 30 / 60 L: </div>

                            <div class="col-md-8">
                                {{ $contract->stored_barels_30 }} / {{ $contract->stored_barels_60 }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Aktuálny stav sudov na sklade 30 / 60 L: </div>

                            <div class="col-md-8">
                                {{ $contract->storageBarelMovement->sum('barel_30') }} / {{ $contract->storageBarelMovement->sum('barel_60') }}
                            </div>
                        </div>

                        <hr>

                        @if(Auth::user()->role_id != 3)
                            <div class="form-group row">
                                <label for="driver_id" class="col-md-4 col-form-label text-md-right">
                                    Vodič
                                </label>

                                <div class="col-md-8">
                                    <select name="driver_id"
                                            class="form-control @error('driver_id') alert alert-danger @enderror">
                                        @error('driver_id')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <option value=""></option>
                                        @foreach($driverList as $driver)
                                            <option value="{{ $driver->id }}"
                                                {{ old('driver_id', isset($acceptanceProtocol) ? $acceptanceProtocol->driver_id : '')  == $driver->id  ? 'selected' : '' }}>
                                                {{ $driver->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="driver_id" value="{{ Auth::user()->id }}">
                        @endif

                        <div class="form-group row">
                            <label for="extraction_date" class="col-md-4 col-form-label text-md-right">
                                Dátum odovzdania
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="datetime-local"
                                    class="form-control @error('extraction_date') alert alert-danger @enderror "
                                    name="extraction_date"
                                    value="{{ old('extraction_date', date('Y-m-d\TH:i'))}}"
                                >
                            </div>
                        </div>

                        <hr>

                        <div class="form-group row">
                            <label for="barel_30" class="col-md-4 col-form-label text-md-right">
                                Počet sudov 30L
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="number"
                                    step="1"
                                    class="form-control recurrency @error('barel_30') alert alert-danger @enderror"
                                    name="barel_30"
                                    value="{{ old('barel_30', 0)}}"
                                    required
                                >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="barel_60" class="col-md-4 col-form-label text-md-right">
                                Počet sudov 60L
                            </label>
                            <div class="col-md-8">
                                <input
                                    type="number"
                                    step="1"
                                    class="form-control recurrency @error('barel_60') alert alert-danger @enderror"
                                    name="barel_60"
                                    value="{{ old('barel_60',  0)}}"
                                    required
                                >
                            </div>
                        </div>


                        <hr>

                        <hr>

                        <div class="form-group row">
                            <label for="caught" class="col-md-4 col-form-label text-md-right">
                                Nezastihnutý
                            </label>

                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="hidden" name="caught" id="caught" value="1" id="caught">
                                    <input type="checkbox" name="caught" id="caught" value="0" id="caught">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-12 align-items-center" id="signature-pad">
                            @include('driver.partials.singnature-pad')
                        </div>

                        <hr>

                        <div class="d-flex row justify-content-around">
                            <span id="location">GPS: {{ isset($acceptanceProtocol) ? $acceptanceProtocol->lat : '' }}, {{ isset($acceptanceProtocol) ? $acceptanceProtocol->lng : '' }}</span>
                        </div>

                        <div class="d-flex row justify-items-between mt-2 mb-2">
                            <div class="col-md-6">
                                <a href="{{ Session::get('signaturePadBackUrl') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                            </div>
                            <div class="col-md-6 text-right">
                                <button type="submit" class="btn btn-primary" id="submit-btn">
                                    <i class="fa fa-file-signature"> {{ isset($acceptanceProtocol) ? 'Uložiť zmeny' : 'Podpísať' }}</i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    {{--    Location lat lng--}}
    <script>
        var x = document.getElementById("location");

        getLocation();
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Tento prehliadač nepodporuje geolokáciu";
            }
        }

        function showPosition(position) {
            let lat = $('#lat').val();
            let lng = $('#lng').val();

            if( (lat == '') && (lng == '')) {
                x.innerHTML = "GPS: Lat: " + position.coords.latitude + ", Lng: " + position.coords.longitude;
                $('#lat').val(position.coords.latitude);
                $('#lng').val(position.coords.longitude);
            }
        }
    </script>
@endsection
