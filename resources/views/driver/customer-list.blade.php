@extends('layouts.master')

@section('title')
    Zberné listy
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zberné listy</h1>
        <a href="{{ url('new-customer') }}">
            <button class="btn btn-primary">
                <i class="fa fa-user-plus"> Pridať klienta</i>
            </button>
        </a>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam klientov</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Meno</th>
                        <th>Prevádzka</th>
                        <th>Email</th>
                        <th>Telefón</th>
                        <th>Nový zberný list</th>
                        <th>Zberné listy</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Meno</th>
                        <th>Prevádzka</th>
                        <th>Email</th>
                        <th>Telefón</th>
                        <th>Nový zberný list</th>
                        <th>Zberné listy</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($userList as $user)
                        <tr id="user-id-{{ $user->id }}">
                            <td>{{ $user->name }}</td>
                            <td>{{ isset($user->lastContract) ? $user->lastContract->branch_name : '' }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                @if(!empty($user->telefon))
                                    <a href="tel:00421{{ $user->telefon }}">+421 / {{ $user->telefon }}</a>
                                @endif
                            </td>
                            <td class="text-center">
                                @if(isset($user->lastContract) && (!$user->lastContract->closed_at || $user->lastContract->closed_at > date('Y-m-d')))
                                    <a href="{{ url('removal-confirmation/' . $user->lastContract->id) }}">
                                        <i class="fas fa-plus text-success"></i>
                                    </a>
                                @endif
                            </td>
                            <td class="text-center">

                                <a href="{{ url('collecting-form-list/' . $user->id) }}">
                                    <i class="fas fa-list"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
