@extends('layouts.master')

@section('title')
    Potvrdenie o prevzatí
@endsection

@section('content')
    <div class="row col-12">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Podpis zberného listu
                </div>

                <div class="card-body">
                    <form action="{{ url('save-removal-confirmation-signature') }}" class="col-12" method="POST">
                        @csrf
                        <input type="hidden" name="id" value="{{ $collectionForm->id }}">
                        <input type="hidden" name="caught" value="1">

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Názov prevádzky: </div>

                            <div class="col-md-8">
                                {{ null != $collectionForm->contract->name ? $collectionForm->contract->name : $collectionForm->contract->user->name }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-4 text-md-right">Dátum odovzdania: </div>

                            <div class="col-md-8">
                                {{ date('d-m-Y H:m:s', strtotime($collectionForm->extraction_date)) }}
                            </div>
                        </div>



                        @if($collectionForm->waste_weight > 0)
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-4 text-md-right"><strong>ODPAD</strong></div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Počet sudov: </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->num_barels }} KS
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Veľkosť sudov: </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->barel_size }} L
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Váha: </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->waste_weight }} KG
                                </div>
                            </div>
                        @endif

                        @if($collectionForm->extra_waste_weight > 0)
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-4 text-md-right"><strong>ODPAD NAD ZMLUVNÉ MNOŽSTVO</strong></div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Počet sudov: </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->extra_num_barels }} KS
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Veľkosť sudov: </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->extra_barel_size }} L
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Váha: </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->extra_waste_weight }} KG
                                </div>
                            </div>
                        @endif

                        @if($collectionForm->oil_capacity > 0)
                            <hr>
                            <div class="form-group row">
                                <div class="col-md-4 text-md-right"><strong>OLEJ</strong></div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 text-md-right"> </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->oil_capacity }} L
                                </div>
                            </div>


                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Odpis: </div>

                                <div class="col-md-8">
                                    @if($collectionForm->write_off == 1)
                                        <span class="text-success">Áno</span>
                                    @else
                                        <span class="text-warning">Nie</span>
                                    @endif
                                </div>
                            </div>
                            @if($collectionForm->write_off != 1)
                                <div class="form-group row">
                                    <div class="col-md-4 text-md-right">Materiál: </div>
                                    <div class="col-md-8">
                                            @if($collectionForm->toilet_paper > 0)
                                                <div>
                                                    Toaletný papier {{ $collectionForm->toilet_paper }} KS
                                                </div>
                                            @endif
                                            @if($collectionForm->hygienic_handkerchiefs > 0)
                                                <div>
                                                    Hygienické vreckovky {{ $collectionForm->hygienic_handkerchiefs }} KS
                                                </div>
                                            @endif
                                            @if($collectionForm->kitchen_towels > 0)
                                                <div>
                                                    Kuchynské utierky {{ $collectionForm->kitchen_towels }} KS
                                                </div>
                                            @endif
                                            @if($collectionForm->oil_for_oil > 0)
                                                <div>
                                                    Olej {{ $collectionForm->oil_for_oil }} KS
                                                </div>
                                            @endif
                                    </div>
                                </div>
                            @endif
                        @endif

                        @if($collectionForm->fat_weight > 0)
                            <hr>
                            <div class="form-group row mt-2">
                                <div class="col-md-4 text-md-right"><strong>TUK</strong></div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 text-md-right"> </div>

                                <div class="col-md-8">
                                    {{ $collectionForm->fat_weight }} KG
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4 text-md-right">Odpis: </div>

                                <div class="col-md-8">
                                    @if($collectionForm->fat_write_off == 1)
                                        <span class="text-success">Áno</span>
                                    @else
                                        <span class="text-warning">Nie</span>
                                    @endif
                                </div>
                            </div>
                            @if($collectionForm->fat_write_off != 1)
                                <div class="form-group row">
                                    <div class="col-md-4 text-md-right">Materiál: </div>
                                    <div class="col-md-8">
                                        @if($collectionForm->fat_toilet_paper > 0)
                                            <div>
                                                Toaletný papier {{ $collectionForm->fat_toilet_paper }} KS
                                            </div>
                                        @endif
                                        @if($collectionForm->fat_hygienic_handkerchiefs > 0)
                                            <div>
                                                Hygienické vreckovky {{ $collectionForm->fat_hygienic_handkerchiefs }} KS
                                            </div>
                                        @endif
                                        @if($collectionForm->fat_kitchen_towels > 0)
                                            <div>
                                                Kuchynské utierky {{ $collectionForm->fat_kitchen_towels }} KS
                                            </div>
                                        @endif
                                        @if($collectionForm->fat_for_oil > 0)
                                            <div>
                                                Olej {{ $collectionForm->fat_for_oil }} KS
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endif
                        @endif

                        <hr>

                        <div class="form-group row">
                            <label for="caught" class="col-md-4 col-form-label text-md-right">
                                Nezastihnutý
                            </label>

                            <div class="col-md-8 text-center">
                                <label class="switch">
                                    <input type="hidden" name="caught" id="caught" value="1" id="caught">
                                    <input type="checkbox" name="caught" id="caught" value="0" id="caught">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-12 align-items-center" id="signature-pad">
                            @include('driver.partials.singnature-pad')
                        </div>

                        <hr>
                        <div class="d-flex row justify-items-between mt-5 mb-2">
                            <div class="col-sm-12 col-md-6">
                                <a href="{{ url('road-list') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                            </div>
                            <div class="col-sm-12 col-md-6 text-right">
                                @if(empty($collectionForm->signature))
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-file-signature"> Podpísať</i>
                                    </button>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
