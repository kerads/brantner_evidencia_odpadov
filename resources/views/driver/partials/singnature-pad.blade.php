<input type="hidden" name="signature" value="{{ isset($collectionForm) ? $collectionForm->signature : ( isset($user) ? $user->signature : null) }}">
@if  ($errors->get('signature'))
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ __('flash-message.sign_document') }}
    </div>
@endif
@if(isset($user) && !empty($user->signature))
    <div class="border">
        <img src="{{ $user->signature }}" alt="">
    </div>
@endif

@if(isset($collectionForm) && empty($collectionForm->signature))
    <div class='js-signature' data-border="1px solid black" data-line-color="#000000" data-line-color="#bc0000"></div>
    <span class="btn btn-light  " id="clear-canvas">nový podpis</span>

    <span id="saveBtn" class="btn btn-default" onclick="saveSignature();">Uložiť podpis</span>
    <div id="signature"></div>
@elseif(!isset($collectionForm))
    <div class='js-signature' data-border="1px solid black" data-line-color="#000000" data-line-color="#bc0000"></div>
    <span class="btn btn-light  " id="clear-canvas">nový podpis</span>

    <span id="saveBtn" class="btn btn-default" onclick="saveSignature();">Uložiť podpis</span>
    <div id="signature"></div>
@elseif(isset($collectionForm))
    <div class="border">
        <img src="{{ $collectionForm->signature }}" alt="">
    </div>
@endif



@section('script')
    <script src="{{ asset('public/js/sign-jquery_2_2_4.js')}}"></script>
    <script src="{{ asset('public/js/jq-signature.js')}}"></script>
    <script>
        $('.js-signature').jqSignature({ width:300 });

        $('#clear-canvas').on('click', function(){
            $('.js-signature').jqSignature('clearCanvas');
        })

        function clearCanvas() {
            $('#signature').html('<p><em>Your signature will appear here when you click "Save Signature"</em></p>');
            $('.js-signature').eq(1).jqSignature('clearCanvas');
        }

        function saveSignature() {
            $('#signature').empty();
            var dataUrl = $('.js-signature').jqSignature('getDataURL');
            var img = $('<img>').attr('src', dataUrl);
            $('#signature').append($('<p>').text("Podpis:"));
            $('#signature').append(img);
            $('input[name=signature]').val(dataUrl);
        }

        $('.js-signature').eq(1).on('jq.signature.changed', function() {
            $('#saveBtn').attr('disabled', false);
        });


        $('input[name="caught"]').on('change', function(){
            signaturePad();
        });

        signaturePad();

        function signaturePad(){
            let caught = $('input[name="caught"]:checked').length;

            if(caught == 0){
                $('#signature-pad').fadeIn();
            } else{
                $('#signature-pad').fadeOut();
            }
        }
    </script>
@endsection
