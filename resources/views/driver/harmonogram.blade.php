@extends('layouts.master')

@section('title')
    Harmonogram
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zmluvy</h1>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center py-3">
            <h6 class="m-0 font-weight-bold text-primary">Harmonogram</h6>
            <div>
                <form action="{{ url('road-list') }}" method="POST" class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    @csrf
                    <div class="input-group">
                        <input
                            name="date"
                            type="date"
                            class="form-control small"
                            aria-label="Search"
                            aria-describedby="basic-addon2"
                            value="{{ $date }}"
                        >
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">
                                <i class="fas fa-search fa-sm"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Výrobný príkaz</th>
                        <th>Meno prevádzky</th>
                        <th>Adresa</th>
                        <th>PSČ</th>
                        <th>Mesto</th>
                        <th>Telefón</th>
                        <th>Vedúci telefón</th>
                        <th>Sudy na sklade <br> 30 / 60 L</th>
                        <th>Výmena sudov</th>
                        <th>Veľkosť sudov</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Výrobný príkaz</th>
                        <th>Meno prevádzky</th>
                        <th>Adresa</th>
                        <th>PSČ</th>
                        <th>Mesto</th>
                        <th>Telefón</th>
                        <th>Vedúci telefón</th>
                        <th>Sudy na sklade <br> 30 / 60 L</th>
                        <th>Výmena sudov</th>
                        <th>Veľkosť sudov</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($contractList as $contract)
                        @if(!$contract->extracted)
                            <tr id="contract-id-{{ $contract->id }}">
                                <th class="text-center">
                                    <a href="{{ url('removal-confirmation/' . $contract->id) }}">
                                        <i class="fas fa-plus text-success"></i>
                                    </a>
                                </th>
                                <td>{{ !empty($contract->branch_name) ? $contract->branch_name : $contract->user->name}}</td>
                                <td>{{ $contract->adresa }}</td>
                                <td>{{ $contract->psc }}</td>
                                <td>{{ $contract->mesto }}</td>
                                <td>
                                    @if(!empty($contract->user->telefon))
                                        <a href="tel:+421{{ $contract->user->telefon }}">+421{{ $contract->user->telefon }}</a>
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($contract->user->z_telefon))
                                        <a href="tel:+421{{ $contract->user->z_telefon }}">+421{{ $contract->user->z_telefon }}</a>
                                    @endif
                                </td>
                                <td>
                                    {{ $contract->stored_barels_30 ?? 0 }} / {{ $contract->stored_barels_60 ?? 0 }}
                                    <a href="{{ url('acceptance-protocol/' . $contract->id) }}">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                </td>
                                <td>{{ $contract->num_barels }}</td>
                                <td>{{ $contract->barel_size }}</td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });

        // delete
        $(".delete").click( function(e){
            e.preventDefault();
            var id = $(this).data('id');
            if (confirm("Naozaj chcete odstrániť zmluvu " + id + " ?")) {
                $.ajax({
                    type: "POST",
                    url: "/delete-contract",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                }).done(function( data ) {
                    removeRow(data);
                });
            }

            function removeRow(data) {
                $("#contract-id-" + data).remove();
            }
        });
    </script>
@endsection
