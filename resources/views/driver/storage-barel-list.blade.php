@extends('layouts.master')

@section('title')
    Zberné nádoby
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zberné nádoby</h1>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam ZN.</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Meno</th>
                        <th>Prevádzka</th>
                        <th>Adresa</th>
                        <th>Má byť ( 30 / 60 L)</th>
                        <th>Naskladnené ZN (30 / 60 L)</th>
                        <th>Vytvoriť PP</th>
                        <th>Preberacie protokoly list</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Meno</th>
                        <th>Prevádzka</th>
                        <th>Adresa</th>
                        <th>Má byť ( 30 / 60 L)</th>
                        <th>Naskladnené ZN (30 / 60 L)</th>
                        <th>Vytvoriť PP</th>
                        <th>Preberacie protokoly</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($contractList as $contract)
                        <tr>
                            <td>{{ $contract->user->name }}</td>
                            <td>{{ $contract->branch_name }}</td>
                            <td>{{ $contract->adresa }}, {{ $contract->mesto }}</td>
                            <td>{{ $contract->stored_barels_30 ?? 0 }} / {{ $contract->stored_barels_60 ?? 0 }}</td>
                            <td>
                                {{ $contract->storageBarelMovement->sum('barel_30') }} / {{ $contract->storageBarelMovement->sum('barel_60') }}
                            </td>
                            <td class="text-center">
                                <a href="{{ url('acceptance-protocol/' . $contract->id) }}">
                                    <i class="fa fa-plus green"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="{{ url('acceptance-protocol-list/' . $contract->id) }}">
                                    <i class="fa fa-list"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
