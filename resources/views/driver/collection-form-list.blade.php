@extends('layouts.master')

@section('title')
    Zberné listy
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zberné listy {{ $user->name }}</h1>
        @if(isset($user->lastContract))
            <a href="{{ url('removal-confirmation/' . $user->lastContract->id) }}">
                <button class="btn btn-primary">
                    <i class="fa fa-user-plus"> Nový zberný list</i>
                </button>
            </a>
        @endif
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Prevádzka</th>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Telefón</th>
                        <th>Dátum</th>
                        <th>Podpísaný</th>
                        <th>Detail</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Prevádzka</th>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Telefón</th>
                        <th>Dátum</th>
                        <th>Podpísaný</th>
                        <th>Detail</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($user->contract as $contract)
                        @foreach($contract->collectingForm as $collectingForm)
                            <tr>
                                <td>{{ !empty($contract->name) ? $contract->name : $user->name }}</td>
                                <td>{{ $collectingForm->id }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @if(!empty($user->telefon))
                                        <a href="tel:00421{{ $user->telefon }}">+421 / {{ $user->telefon }}</a>
                                    @endif
                                </td>
                                <td>
                                    {{ date('d-m-Y H:m:s', strtotime($collectingForm->extraction_date)) }}
                                </td>
                                <td class="text-center">
                                    @if($collectingForm->caught == 1)
                                        <i class="fa fa-check text-success"></i>
                                    @else
                                        <i class="fa fa-times text-danger"></i>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{ url('signature-pad/' . $collectingForm->id) }}">
                                        <i class="fa fa-info-circle text-primary"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex row justify-items-between mt-2 mb-2">
                <div class="col-md-6">
                    <a href="{{ url('company-collecting-form-list') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
