<!doctype html>
<html lang="sk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Akceptačný protokol</title>

    <style type="text/css">

        body {
            font-family: 'DejaVu Sans', sans-serif;
            font-size: 8;
            /*margin: 0px;*/
        }

        tr, td, div {
            padding: 10;
            margin: 0;
        }

        .full-width {
            width: 100%;
        }

        .border {
            border: 1px solid black;
        }

        th, td, div {
            vertical-align: top;
        }

        .align-left{
            text-align: left;
        }

        .align-right{
            text-align: right;
        }

        .align-center{
            text-align: center;
        }
    </style>

</head>
<body>
<div>
    <div class="row">
        <table class="full-width">
            <tr>
                <td colspan="3" class="align-left border" >
                    <strong>Dodávateľ </strong><br>
                    Brantner Poprad. s.r.o. <br>
                    Nová 76, 058 01 Poprad<br>
                    <strong>IČO:</strong> 36 444 618 <br>
                    <strong>DIČ:</strong> 2020016988 <br>
                    <strong>IČ DPH:</strong> SK 2020016988 <br>
                    <strong>Zapísaný:</strong>
                    <div>
                        <strong>Obchodný register:</strong>
                        <br>
                        OS Prešov
                        <br>
                        Oddiel: Sro. Vložka č.: 10137/P
                        <br>
                        <strong>Registračné číslo ŠVPS SR:</strong> COLTR16PP-SK
                    </div>
                </td>
                <td colspan="3" class="align-left border">
                    <strong>Odberateľ </strong><br>
                    {{ $acceptanceProtocol->contract->brnach_name !== null ? $acceptanceProtocol->contract->brnach_name : $acceptanceProtocol->user->name }}<br>
                    {{ $acceptanceProtocol->contract->adresa }}, {{ $acceptanceProtocol->contract->psc }} {{ $acceptanceProtocol->contract->mesto }}<br>
                    <strong>IČO:</strong> {{ $acceptanceProtocol->user->ico }} <br>
                    <strong>DIČ:</strong> {{ $acceptanceProtocol->user->dic }} <br>
                    <strong>IČ DPH:</strong> {{ $acceptanceProtocol->user->icdph }} <br>
                </td>
            </tr>


            <tr>
                <td colspan="5" class="align-left border" >
                    Názov tovaru
                </td>
                <td class="align-center border" >
                    Množstvo
                </td>
            </tr>

            @php
                $n = 10;
            @endphp
            @if($acceptanceProtocol->barel_30 != 0)
                <tr>
                    <td colspan="5" class="align-left border" >
                        Sud 30L
                    </td>
                    <td class="align-center border" >
                        5
                    </td>
                </tr>
            @else
                @php
                    $n++;
                @endphp
            @endif

            @if($acceptanceProtocol->barel_60 != 0)
                <tr>
                    <td colspan="5" class="align-left border" >
                        Sud 60L
                    </td>
                    <td class="align-center border" >
                        -1
                    </td>
                </tr>
            @else
                @php
                    $n++;
                @endphp
            @endif

            @for($i = 0; $i < $n; $i++)
                <tr>
                    <td colspan="5" class="align-left border" >

                    </td>
                    <td class="align-center border" >

                    </td>
                </tr>
            @endfor

            <tr>
                <td colspan="2" class="align-left">
                    Vyskladnil: <br>
                    {{ $acceptanceProtocol->driver->name }}
                </td>
                <td colspan="2" class="align-center">
                    Deň prevzatia: <br>
                    {{ $acceptanceProtocol->extraction_date == null ? '' :  date('d-m-Y',strtotime($acceptanceProtocol->extraction_date)) }}
                </td>
                <td colspan="2" class="align-right">
                    Podpis odberateľa: <br>
                    <img src="{{$acceptanceProtocol->signature}}"  style="width: 2.5cm; height:0.8cm;">
                </td>
            </tr>

        </table>
    </div>
</div>
</body>
</html>
