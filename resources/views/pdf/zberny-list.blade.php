<!doctype html>
<html lang="sk">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Zberný list {{ $form->id }}</title>

    <style type="text/css">

        body {
            width: 18.9cm;
            font-family: 'DejaVu Sans', sans-serif;
            font-size: 5;
            margin: 0px;
        }

        .row {
            margin: 2em 0em;
            display: flex;
            justify-content: space-between;
        }

        table {
            border-collapse: collapse;
        }

        tr, td, div {
            margin: 0;
            padding: 0;
            vertical-align: top;
        }

        td {
            padding-left: 5px;
        }

        .full-width {
            width: 18.8cm;
        }

        .w-100 {
            width: 100%;
        }

        .w-39 {
            width: 39%;
        }

        .w-22 {
            width: 22%;
        }

        .logo-w {
            width: 3.3cm;
            height: 1cm;
        }

        .company-stamp {
            width: 3.3cm;
            height: 0.8cm;
        }

        .border {
            border: 1px solid black;
        }

        .align-left{
            text-align: left;
        }

        .align-center{
            text-align: center;
        }
    </style>

</head>
<body>
<div>
    <div class="row margin-bottom-1">
        <table class="w-100">
            <tr>
                <td rowspan="2" class="align-left" >
                    <img src="{{ url('public/images/logo/brantner-logo.jpg') }}" class="logo-w">
                </td>
                <td rowspan="2" class="align-center" style="width: 10.7cm;">
                    <span style="font-size: 9px;"> Potvrdenie o prevzatí odpadu - OBCHODNÝ DOKLAD / ZBERNÝ LIST </span> <br>
                    <span style="font-size: 7px;"> pre vedľajšie živočíšne produkty - materiál kategórie 3 "neurčené na ľudskú spotrebu" </span>
                </td>
                <td>
                    <div class="border align-center" >
                        číslo dokladu: {{ $form->id }}
                    </div>

                    <div class="border align-center">
                        10 - 15
                    </div>
                </td>
            </tr>
        </table>
    </div>

    <div class="row">
        <table class="w-100">
            <tr style="height: 0.6cm;">
                <td colspan="2" class="border">
                    ODOVZDÁVAJUCI:
                </td>
                <td class="border w-31">
                    PREPRAVCA:
                </td>
            </tr>
            <tr>
                <td class="border w-22" style="height: 0.52cm; padding-top:4px;">
                    <strong>Názov/Meno prevádzkovateľa:</strong>
                </td>
                <td class="border w-39" style="height: 0.52cm; padding-top:4px;">
                    {{ $form->customer->name }}
                </td>
                <td rowspan="5" class="border w-31">
                    Brantner Poprad. s.r.o.
                    <br>
                    Nová 76, 058 01 Poprad
                    <br>
                    <strong>IČO:</strong> 36 444 618
                    <br>
                    <strong>Obchodný register:</strong>
                    <br>
                    OS Prešov
                    <br>
                    Oddiel: Sro. Vložka č.: 10137/P
                    <br>
                    <strong>Registračné číslo ŠVPS SR:</strong> COLTR16PP-SK
                </td>
            </tr>
            <tr>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    Adresa (sídlo) prevádzkovateľa:
                </td>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    {{ $form->customer->adresa }}, {{ $form->customer->psc }}, {{ $form->customer->mesto }}
                </td>
            </tr>
            <tr>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    IČO:
                </td>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    <strong>IČO:</strong> {{ $form->customer->ico }}
                </td>
            </tr>
            <tr>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    CEHZ/úradné číslo:
                </td>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                </td>
            </tr>
            <tr>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    <strong>Názov prevádzky:</strong>
                </td>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    {{ $form->contract->branch_name }}
                </td>
            </tr>
            <tr class="border">
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    Adresa prevádzky:
                </td>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    {{ $form->contract->adresa }}, {{ $form->contract->psc }} {{ $form->contract->mesto }}
                </td>
                <td rowspan="2" style="padding-left: 0;">
                    <table class="w-100">
                        <tr>
                            <td rowspan="2" class="border" style="width: 2.8cm; height: 1.3cm; text-align:center;">
                                Kontakt: <br>
                                0902 650 650 <br>
                                <span style="font-size: 6px">alena.porembova@brantner.sk</span>
                            </td>
                            <td class="border" style="width: 2.10cm; height: 0.52cm;padding-top:4px;">EVČ vozidla</td>
                            <td class="border" style="width: 2.10cm; height: 0.52cm;padding-top:4px;">Meno vodiča</td>
                        </tr>
                        <tr>
                            <td class="border" style="height: 0.52cm; padding-top:4px;">
                                {{$form->vrn}}
                            </td>
                            <td class="border" style="height: 0.52cm; padding-top:4px;">
                                {{ !empty($form->driver) ? $form->driver->name : '' }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                    Zastúpený osobou(Meno):
                </td>
                <td class="border" style="height: 0.52cm; padding-top:4px;">
                </td>
            </tr>
        </table>
    </div>

    <div class="row">
        <table class="w-100">
            <tr>
                <td colspan="8" class="border">
                    Druh a množstvo odpadu:
                </td>
            </tr>
            <tr>
                <td rowspan="2" class="border" style="width: 0.9cm; height: 0.78cm;">
                    <strong>Kat. č.</strong>
                </td>
                <td rowspan="2" class="border" style="width: 2.9cm; height: 0.78cm;">
                    Názov
                </td>
                <td rowspan="2" class="border" style="width: 1.3cm; height: 0.78cm; text-align:center;">
                    <span>Druh <br> odberu</span>
                </td>
                <td colspan="3" class="border" style="height: 0.37cm; text-align:center;">
                    <strong>Množstvo</strong>
                </td>
                <td rowspan="2" colspan="2" class="border" style="text-align:center;">
                    <strong>Popis:</strong>
                    <br>
                    <strong>Vedľajší živočíšny produkt</strong> (ES č. 1069/2009)
                </td>
            </tr>
            <tr>
                <td class="border" style="width: 1.7cm; height: 0.35cm; text-align:center;"><strong>KS</strong></td>
                <td class="border" style="width: 1.7cm; text-align:center;"><strong>KG</strong></td>
                <td class="border" style="width: 1.7cm; text-align:center;"><strong>/</strong></td>
            </tr>
            <tr>
                <td rowspan="2" class="border" style="height: 1.11cm; font-size: 5;">
                    20 01 08
                </td>
                <td rowspan="2" class="border" style="width: 2.9cm; height: 1.11cm;">
                    biologicky rozložiteľný <br>
                    kuchynský <br>
                    a reštauračný odpad
                </td>
                <td class="border" style="width: 1.3cm; height: 0.53cm; text-align:center;">
                    ZMLUVNÝ
                </td>
                <td class="border" style="width: 1.3cm; height: 0.53cm; text-align:center;">
                    @if($form->num_barels > 0 && $form->waste_type == '-1')
                        {{ $form->num_barels }} / {{ $form->barel_size }}  L
                    @endif
                </td>
                <td class="border" style="width: 1.3cm; height: 0.53cm; text-align:center;">
                    @if($form->waste_weight > 0 && $form->waste_type == '-1')
                        {{ $form->waste_weight }}
                    @endif
                </td>
                <td class="border" style="height: 0.53cm; text-align:center;"></td>
                <td rowspan="2" class="border" style="height: 1.12cm; text-align:center;">
                    kuchynský odpad
                </td>
                <td rowspan="3" class="border" style="height: 1.7cm; text-align:center;">
                    Materiál kategórie p) <br>
                    kuchybský odpad <br>
                    "neurčené na ľudskú spotrebu"
                </td>
            </tr>
            <tr>
                <td class="border" style="height: 0.53cm; text-align:center;">
                    MIMORIADNY
                </td>
                <td class="border" style="height: 0.53cm; text-align:center;">
                    @if($form->extra_num_barels > 0)
                        {{ $form->extra_num_barels }} / {{ $form->extra_barel_sizel }}  L
                    @endif
                </td>
                <td class="border" style="height: 0.53cm; text-align:center;">
                    @if($form->extra_waste_weight > 0)
                        {{ $form->extra_waste_weight }}
                    @endif
                </td>
                <td class="border" style="height: 0.53cm; text-align:center;"></td>
            </tr>
            <tr>
                <td class="border" style="height: 0.5cm; font-size: 5;">
                    20 01 25
                </td>
                <td class="border" style="height: 0.5cm;">
                    jedlé oleje a tuky
                </td>
                <td class="border" style="height: 0.51cm; text-align:center;">
                    Olej / Tuk
                </td>
                <td class="border" style="height: 0.51cm; text-align:center;">
                    @if($form->oil_capacity > 0)
                        {{ $form->oil_capacity }} L
                    @endif
                    /
                    @if($form->fat_weight > 0)
                        {{ $form->fat_weight }} KG
                    @endif
                </td>
                <td class="border" style="height: 0.51cm; text-align:center;">
                    @if($form->fat_weight > 0)
                        Tuk
                    @endif
                </td>
                <td class="border" style="height: 0.51cm; text-align:center;">
                    @if($form->fat_weight > 0)
                        {{ $form->fat_weight }} KG
                    @endif
                </td>
                <td class="border" style="height: 0.51cm; text-align:center;">
                    separovaný použitý potravinársky olej
                </td>
            </tr>
            <tr>
                <td class="border" style="height: 0.65cm;">
                    @if($form->waste_type != -1)
                        {{$form->waste_code}}
                    @endif
                </td>
                <td class="border">
                    @if($form->waste_type != -1)
                        {{$form->waste_name}}
                    @endif
                </td>
                <td class="border"></td>
                <td class="border">
                    @if($form->num_barels > 0 && $form->waste_type != '-1')
                        {{ $form->num_barels }} / {{ $form->barel_size }}  L
                    @endif
                </td>
                <td class="border">
                    @if($form->waste_weight > 0 && $form->waste_type != '-1')
                        {{ $form->waste_weight }}
                    @endif
                </td>
                <td class="border"></td>
                <td class="border">
                    produktu živočíšneho pôvodualebo potraviny <br>
                    obsahujúce produkty živočíšneho pôvodu
                </td>
                <td class="border">
                    Materiál kategórie 3 f) - <br>
                    "Neurčené na ľudskú spotrebu"
                </td>
            </tr>
            <tr>
                <td colspan="2" class="border" style="height: 0.5cm;">
                    <strong>
                        Dátum odovzdania
                    </strong>
                </td>
                <td colspan="4" class="border" style="padding-left: 0; height: 0.5cm;">
                    <table class="w-100">
                        <tr>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[8] }}</td>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[9] }}</td>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[5] }}</td>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[6] }}</td>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[0] }}</td>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[1] }}</td>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[2] }}</td>
                            <td class="border" style="height: 0.8cm; text-align:center;">{{ $form->arrDate[3] }}</td>
                        </tr>
                    </table>
                </td>
                <td rowspan="2" class="border" style="text-align:center; align-items: center;">
                    Podpis za prepravcu
                    @if(!empty($form->driver_signature))
                        <img src="{{ $form->driver_signature }}" style="width: 3.8cm; height:0.8cm;"/>
                    @endif
                    <p class="w-100">GPS: lat: {{ $form->lat }}, lng: {{ $form->lng }}</p>
                </td>
                <td rowspan="2" class="border" style="text-align:center; align-items: center;">
                    Pečiatka prepravcu
                    <img src="{{ url('public/images/peciatky/peciatka_brantner.png') }}" style="width:3.8cm;">
                </td>
            </tr>
            <tr>
                <td colspan="2" class="border" style="min-height: 1.2cm; vertical-align: top;">
                    Podpis za prevádzku <Br/>
                    @if(!empty($form->signature))
                        <img src="{{ $form->signature }}" style="width:3.8cm;"/>
                    @endif
                </td>
                <td colspan="4" class="border" style="min-height: 1.2cm; vertical-align: top; text-align:center; align-items: center;">
                    Pečiatka prevádzky <Br/>
                    @if(!empty($form->signature) && $form->contract->company_stamp)
                        <img src="{{ url('storage/app/public/firemne-peciatky/' . $form->customer_id . '/' . $form->contract->company_stamp) }}" class="company-stamp" style="width: 3.8cm; height:2cm;" />
                    @endif
                </td>
            </tr>
        </table>
    </div>

    <div class="row">
        <table class="w-100">
            <tr>
                <td class="border" style="width:4cm; height:0.6cm;">
                    <strong>PREBERAJÚCI:</strong>
                </td>
                <td colspan="3" class="border" style="width: 14.66cm; height:0.5cm; padding-top: 5px;">
                    <div class="border" style="width: 0.3cm; height: 0.3cm; margin-left: 10px; margin-right: 10px; background-color: black; display: inline-block;"></div>
                    <div style="margin-right: 20px; display: inline-block;">SKLAD</div>
                    <div class="border" style="width: 0.3cm; height: 0.3cm; margin-left: 10px; margin-right: 10px; display: inline-block;"></div>
                    <div style="margin-right: 20px; display: inline-block;">BIOPLYNOVÁ STANICA</div>
                    <div class="border" style="width: 0.3cm; height: 0.3cm; margin-left: 10px; margin-right: 10px; display: inline-block;"></div>
                    <div style="margin-right: 20px; display: inline-block;">KOMPOSTÁREŇ</div>
                </td>
            </tr>
            <tr>
                <td class="border" style="height:0.5cm;">
                    <strong>Názov</strong>
                </td>
                <td class="border" style="height:0.5cm;"></td>
                <td class="border">
                    <strong>Dátum prevzatia</strong>
                </td>
                <td class="border" style="width: 5cm; padding-left:0;">
                    <table class="w-100">
                        <tr>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[8] }}</td>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[9] }}</td>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[5] }}</td>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[6] }}</td>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[0] }}</td>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[1] }}</td>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[2] }}</td>
                            <td class="border" style="height: 0.5cm; text-align:center; align-items: center;">{{ $form->arrDate[3] }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="border" style="height:0.5cm;">
                    Adresa:
                </td>
                <td class="border"></td>
                <td rowspan="2" class="border" style="width: 4.8cm; height: 1.06cm; text-align:center;">
                    Podpis za preberajúceho
                </td>
                <td rowspan="2" class="border" style="width: 4.37cm; height: 1.06cm;text-align:center;">
                    Pečiatka preberajúceho
                </td>
            </tr>
            <tr>
                <td class="border" style="height:0.5cm;">
                    Úradné číslo:
                </td>
                <td class="border" style="height:0.5cm;">
                    STOD-PP55-SK
                </td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
