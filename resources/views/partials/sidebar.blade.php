<!-- Sidebar - Brand -->
<a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
    <div class="sidebar-brand-icon rotate-n-15">
        <i class="fas fa-laugh-wink"></i>
    </div>
    <div class="sidebar-brand-text mx-3">Evidencia odpadov</div>
</a>

@auth
    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ url('profile') }}">
            <i class="fas fa-fw fa-info"></i>
            <span>Profil</span></a>
    </li>

    @if(Auth::user()->hasPermission(2) || Auth::user()->hasPermission(1))
        <hr class="sidebar-divider">

        <div class="sidebar-heading">
            Užívateľ
        </div>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#role" aria-expanded="true" aria-controls="harmonogram">
                <i class="fas fa-users fa-cog"></i>
                <span>Užívateľia</span>
            </a>
            <div id="role" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Rola:</h6>
                    <a class="collapse-item" href="{{ url('customer-list') }}">Klient</a>
                    <a class="collapse-item" href="{{ url('driver-list') }}">Vodič</a>
                    <a class="collapse-item" href="{{ url('user-list') }}">Zamestnanec</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#reports" aria-expanded="true" aria-controls="harmonogram">
                <i class="fa fa-chart-bar"></i>
                <span>Reporty</span>
            </a>
            <div id="reports" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ url('reports/collecting-form-list') }}">Zberné listy</a>
                </div>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('price-list') }}">
                <i class="fas fa-fw fa-euro-sign"></i>
                <span>Cenník</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('stock-records') }}">
                <i class="fas fa-fw fa-list"></i>
                <span>Evidencia</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#warehouse" aria-expanded="true" aria-controls="harmonogram">
                <i class="fas fa-fw fa-warehouse"></i>
                <span>Skladové hospodárstvo</span>
            </a>
            <div id="warehouse" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ url('warehouse') }}">Sklad hygieny</a>
                    <a class="collapse-item" href="{{ url('stock-movement-list') }}">Zoznam pohybov</a>
                </div>
            </div>
        </li>
    @endif

    @if(Auth::user()->hasPermission(3) || Auth::user()->hasPermission(2) || Auth::user()->hasPermission(1))
        <hr class="sidebar-divider">

        <div class="sidebar-heading">
            Vodič
        </div>

        <li class="nav-item">
            <a class="nav-link" href="{{ url('road-list') }}">
                <i class="fas fa-fw fa-truck-moving"></i>
                <span>Harmonogram</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#harmonogram" aria-expanded="true" aria-controls="harmonogram">
                <i class="fas fa-fw fa-list"></i>
                <span>Zoznamy</span>
            </a>
            <div id="harmonogram" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ url('company-collecting-form-list') }}">Firmy</a>
                    <a class="collapse-item" href="{{ url('storage-barel-list') }}">Zberné nádoby</a>
                </div>
            </div>
        </li>
    @endif

    @if(Auth::user()->hasPermission(4))
        <hr class="sidebar-divider d-none d-md-block">

        <div class="sidebar-heading">
            Moja firma
        </div>

        @if(Auth::user()->child()->exists())
            <li class="nav-item">
                <a class="nav-link" href="{{ url('my-companies') }}">
                    <i class="fas fa-fw fa-building"></i>
                    <span>Firmy</span></a>
            </li>
        @endif

        <li class="nav-item">
            <a class="nav-link" href="{{ url('my-contracts') }}">
                <i class="fas fa-fw fa-list"></i>
                <span>Moje zmluvy</span></a>
        </li>
    @endif
@endauth

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<li class="nav-item">
    <a class="nav-link" href="{{ route('logout') }}"
        onclick="event.preventDefault();
        document.getElementById('logout-form-side').submit();">
        <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
        <span>Odhlásiť sa</span>
    </a>

    <form id="logout-form-side" action="{{ route('logout') }}" method="POST" class="d-none">
        @csrf
    </form>
</li>

<!-- Divider -->
<hr class="sidebar-divider d-none d-md-block">

<!-- Sidebar Toggler (Sidebar) -->
<div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
</div>

