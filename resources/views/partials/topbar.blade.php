<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
</button>

<div>
    <a href="{{ url('/') }}">
        <img src="{{ url('public/images/brantner-logo.png') }}" alt="Brantner logo">
    </a>
</div>

<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">

    <div class="topbar-divider d-none d-sm-block"></div>

    <!-- Nav Item - User Information -->
    @guest
        <li class="nav-item ">
            <a href="{{ url('/login') }}" class="nav-link">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Prihlásiť sa</span>
                <i class="fa fa-user"></i>
            </a>
        </li>
    @else
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="dropdownUserProfile" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600">{{ AUTH::User()->name }}</span>
                <i class="fa fa-bars"></i>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="#dropdownUserProfile">
                <a class="dropdown-item" href="{{ url('profile') }}">
                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                    Môj profil
                </a>
                @if(AUTH::User()->role_id == 4)
                    <a class="dropdown-item" href="{{ url('my-contracts') }}">
                        <i class="fas fa-list-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                        Moje zmluvy
                    </a>
                @endif
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Odhlásiť sa
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </div>
        </li>
    @endguest
</ul>
