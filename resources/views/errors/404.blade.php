@extends('layouts.master')

@section('title')
    {{ __('Not Found') }}
@endsection

@section('content')
    <div class="d-flex flex-column vh-85 bg-login-img align-items-center justify-content-around">
        <div class="col-12 align-items-center justify-content-around text-center pt-5">

            <img src="{{ url('public/images/404.png') }}" alt="404" lass="w-100">
            <h2 class="bg-white text-danger">{{ __('Not Found') }}</h2>
            <img src="{{ url('public/images/bug.png') }}" alt="bug" class="mt-5">

        </div>
    </div>
@endsection
