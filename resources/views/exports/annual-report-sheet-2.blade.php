<table style="font-family:arial;font-size:8px;">
    <tr>
        <td></td>
        <td colspan="70" style="text-align: right">List č. :</td>
        <td colspan="2" style="text-align: center;border: 1px solid black;">0</td>
        <td colspan="2" style="text-align: center;border: 1px solid black;">0</td>
        <td colspan="2" style="text-align: center;border: 1px solid black;">2</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="70" style="text-align: right">Počet listov:</td>
        <td colspan="2" style="text-align: center;border: 1px solid black;">0</td>
        <td colspan="2" style="text-align: center;border: 1px solid black;">0</td>
        <td colspan="2" style="text-align: center;border: 1px solid black;">2</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="11" style="">Evidenčné číslo:</td>
        <td colspan="3" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="3" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
        <td colspan="43"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-bottom: 1px solid black;"> </td>
    </tr>

    <tr>
        <td></td>
        <td style="border-left: 1px solid black"></td>
        <td colspan="29" ><strong>ORGANIZÁCIA</strong></td>
        <td colspan="3" style="text-align:center;">IČO</td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[0] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[1] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[2] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[3] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[4] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[5] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[6] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[7] }}</strong></td>
        <td colspan="27" style="text-align:right;border-right: 1px solid black;"><strong>PREVÁDZKÁREŇ / ZÁVOD  </strong></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-top: 1px solid black;"></td>
        <td colspan="37" style="border-top: 1px solid black;border-right: 1px solid black;">Obchodné meno:</td>
        <td style="border-top: 1px solid black;border-left: 1px solid black;"></td>
        <td colspan="37" style="border-top: 1px solid black;border-right: 1px solid black;">Názov:</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="4" style="border-bottom: 1px solid black;border-left: 1px solid black;"></td>
        <td colspan="34" style="border-bottom: 1px solid black;border-right: 1px solid black;">{{ $data->customer->parent->name }}</td>
        <td colspan="4" style="border-bottom: 1px solid black;border-left: 1px solid black;"></td>
        <td colspan="34" style="border-bottom: 1px solid black;border-right: 1px solid black;">{{ $data->customer->lastContract->branch_name }}</td>
    </tr>

    <tr>
        <td></td>
        <td colspan="76" style="height: 6px"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height: 30px; border-bottom: 1px solid black"></td>
    </tr>

    <tr>
        <td></td>
        <td colspan="4" style="text-align: center;border-left: 1px solid black;border-top: 1px solid black;border-right: 1px solid black;">Por.</td>
        <td colspan="12" style="text-align: center;border-left: 1px solid black;border-top: 1px solid black;border-right: 1px solid black;">Kód odpadu podľa</td>
        <td colspan="17" style="text-align: center;border-left: 1px solid black;border-top: 1px solid black;border-right: 1px solid black;">Názov odpadu podľa</td>
        <td colspan="5" style="text-align: center;border-left: 1px solid black;border-top: 1px solid black;border-right: 1px solid black;">Kateg.</td>
        <td rowspan="2" colspan="5" style="text-align: center;vertical-align: center;border: 1px solid black;">Y-kód</td>
        <td colspan="11" style="text-align: center;border: 1px solid black;">Hmotnosť odpadu</td>
        <td colspan="19" style="text-align: center;border: 1px solid black;">Spôsob nakladania s odpadom</td>
        <td rowspan="2" colspan="3" style="text-align: center;vertical-align: center;border: 1px solid black;">Pozn.</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="4" style="text-align: center;border-left: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;">číslo</td>
        <td colspan="12" style="text-align: center;border-left: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;">Katalógu odpadov</td>
        <td colspan="17" style="text-align: center;border-left: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;">Katalógu odpadov</td>
        <td colspan="5" style="text-align: center;border-left: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;">odpadu</td>
        <td colspan="11" style="text-align: center;border: 1px solid black;">(v tonách)</td>
        <td colspan="3" style="text-align: center;border: 1px solid black;">Kód</td>
        <td colspan="16" style="text-align: center;border: 1px solid black;">IČO, obchodné meno, sídlo</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="4" style="text-align: center;border: 1px solid black;">1</td>
        <td colspan="12" style="text-align: center;border: 1px solid black;">2</td>
        <td colspan="17" style="text-align: center;border: 1px solid black;">3</td>
        <td colspan="5" style="text-align: center;border: 1px solid black;">4</td>
        <td colspan="5" style="text-align: center;border: 1px solid black;">5</td>
        <td colspan="11" style="text-align: center;border: 1px solid black;">6</td>
        <td colspan="3" style="text-align: center;border: 1px solid black;">7</td>
        <td colspan="16" style="text-align: center;border: 1px solid black;">8</td>
        <td colspan="3" style="text-align: center;border: 1px solid black;">9</td>
    </tr>

{{--    waste--}}
    @include('exports.partials-sheet-2.waste-row', ['data' => $data])
{{--    fat and oil--}}
    @if($data->oil_weight > 0 || $data->fat_weight > 0)
        @php
            $data->fat = 1;
        @endphp
        @include('exports.partials-sheet-2.waste-row', ['data' => $data])
    @endif
{{--    empty rows--}}
    @php
        $data->data = 0;
    @endphp
    @for($data->row_count; $data->row_count < 11; $data->row_count)
        @include('exports.partials-sheet-2.waste-row', ['data' => $data])
    @endfor
</table>
