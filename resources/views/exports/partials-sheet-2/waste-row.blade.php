<tr>
    <td></td>
    <td rowspan="3" colspan="4" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->row_count }}.</td>
    @php
        $data->row_count= $data->row_count + 1;
    @endphp
    <td colspan="12" style="text-align: center;border: 1px solid black;"></td>
    <td rowspan="3" colspan="17" style="vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? ($data->fat == 1 ? $data->fat_note : $data->waste_note) : "" }}</td>
    <td rowspan="3" colspan="5" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? "O" : "" }}</td>
    <td rowspan="3" colspan="5" style="text-align: center;border: 1px solid black;"></td>
    <td rowspan="3" colspan="11" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? ($data->fat == 1 ? $data->oil_weight + $data->fat_weight : $data->waste_weight) : "" }}</td>
    <td rowspan="3" colspan="3" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? "Z" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td colspan="2" style="text-align: center;border: 1px solid black;"></td>
    <td rowspan="3" colspan="3" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? "PD" : "" }}</td>
</tr>
<tr>
    <td></td>
    <td colspan="12" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? ($data->fat == 1 ? $data->fat_catalog_num : $data->waste_catalog_num) : "" }}</td>
    <td rowspan="2" colspan="16" style="text-align: center;border: 1px solid black;">
{{--        @if($data->data)--}}
{{--            {{ $data->customer->lastContract->branch_name }} <br> {{ $data->customer->lastContract->adresa }}, {{ $data->customer->lastContract->mesto }} {{ $data->customer->lastContract->psc }}--}}
{{--        @endif--}}
    </td>
</tr>
<tr>
    <td></td>
    <td colspan="12" style="text-align: center;border: 1px solid black;"></td>
</tr>


<tr>
    <td></td>
    <td rowspan="3" colspan="4" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->row_count }}.</td>
    @php
        $data->row_count = $data->row_count + 1;
    @endphp
    <td colspan="12" style="text-align: center;border: 1px solid black;"></td>
    <td rowspan="3" colspan="17" style="vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? ($data->fat == 1 ? $data->fat_note : $data->waste_note) : "" }}</td>
    <td rowspan="3" colspan="5" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? "O" : "" }}</td>
    <td rowspan="3" colspan="5" style="text-align: center;border: 1px solid black;"></td>
    <td rowspan="3" colspan="11" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? ($data->fat == 1 ? $data->oil_weight + $data->fat_weight : $data->waste_weight) : "" }}</td>
    <td rowspan="3" colspan="3" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? "V" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "3" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "6" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "4" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "4" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "4" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "6" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "1" : "" }}</td>
    <td colspan="2" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? "8" : "" }}</td>
    <td rowspan="3" colspan="3" style="text-align: center;vertical-align: center;border: 1px solid black;">{{ $data->data == 1 ? "PRO" : "" }}</td>
</tr>
<tr>
    <td></td>
    <td colspan="12" style="text-align: center;border: 1px solid black;">{{ $data->data == 1 ? ($data->fat == 1 ? $data->fat_catalog_num : $data->waste_catalog_num) : "" }}</td>
    <td rowspan="2" colspan="16" style="text-align: center;border: 1px solid black;">
        @if($data->data == 1)
            Brantner Poprad s.r.o. <br> Nová 76, Poprad 058 01
        @endif
    </td>
</tr>
<tr>
    <td></td>
    <td colspan="12" style="text-align: center;border: 1px solid black;"></td>
</tr>
