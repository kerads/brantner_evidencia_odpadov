<table>
    <tr>
        <th>Názov prevádzkovateľa</th>
        <th>Názov prevádzky</th>
        <th>Adresa prevádzky</th>
        <th>Katalógové číslo</th>
        <th>Váha (t)</th>
        <th>Cena €</th>
        <th>Toaletný papier</th>
        <th>Hygienické vreckovky</th>
        <th>Kuchynské vreckovky</th>
        <th>Olej</th>
        <th>Počet sudov</th>
        <th>Počet sudov nad zm.množstvo</th>
        <th>Cena nad zm.množstvo €</th>
        <th>dezinfekcia €</th>
        <th>Počet zberov</th>
        <th>Ext. č. firmy</th>
        <th>Ext. č. pobočky</th>
        <th>Poznámka</th>
    </tr>
    @if(!empty($data))
        @include('user.partials.stock-record-line', ['list' => $data])
    @endif
</table>
