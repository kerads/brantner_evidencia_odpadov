<table style="font-family:arial;font-size:8px;">
    <tr>
        <td colspan="77"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="font-family:arial;font-size:12px;font-weight:bold;text-align:center;float:left;margin-bottom:10px" >OHLÁSENIE O VZNIKU ODPADU A NAKLADANÍ S NÍM</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:30px;"> </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="9">Typ dokladu</td>
        <td colspan="6" style="border: 1px solid black;">P</td>
        <td colspan="4"></td>
        <td colspan="57" style="border: 1px solid black;text-align:center;">VYPLNÍ ÚRAD</td>
    </tr>
    <tr>
        <td colspan="20" style="height:7px;"></td>
        <td colspan="57" style="border-left: 1px solid black;border-top: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="9">Rok</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">{{ $data->arrYear[0] }}</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">{{ $data->arrYear[1] }}</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">{{ $data->arrYear[2] }}</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">{{ $data->arrYear[3] }}</td>
        <td colspan="2"></td>
        <td colspan="1" style="border-left: 1px solid black;"></td>
        <td colspan="21" style="border-right: 1px solid black;">Odtlačok pečiatky úradu:</td>
        <td colspan="1" style="border-left: 1px solid black;"></td>
        <td colspan="13">Evidenčné číslo:</td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="1" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="19"></td>
        <td colspan="22" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
        <td colspan="1" style="border-left: 1px solid black;"></td>
        <td colspan="13">Dátum doručenia: </td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="1" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="19"></td>
        <td colspan="22" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
        <td colspan="35" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="19"></td>
        <td colspan="22" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
        <td colspan="1" style="border-left: 1px solid black;"></td>
        <td colspan="34" style="border-right: 1px solid black;">Doklad za úrad overil:</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="9">List č.:</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">0</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">0</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">1</td>
        <td colspan="4"></td>
        <td colspan="22" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
        <td colspan="35" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="9">Počet listov:</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">0</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">0</td>
        <td colspan="2" style="border: 1px solid black;text-align:center;">2</td>
        <td colspan="4"></td>
        <td colspan="22" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
        <td colspan="35" style="border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="19" style="height:30px;"></td>
        <td colspan="22" style="height:30px;border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="35" style="height:30px;border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-left: 1px solid black;border-right: 1px solid black;border-top: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black"></td>
        <td colspan="29" ><strong>ORGANIZÁCIA</strong></td>
        <td colspan="3" style="text-align:center;">IČO</td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[0] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[1] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[2] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[3] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[4] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[5] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[6] }}</strong></td>
        <td colspan="2" style="text-align:center;border: 1px solid black;"><strong>{{ $data->arrIco[7] }}</strong></td>
        <td colspan="27" style="text-align:right;border-right: 1px solid black;"><strong>PREVÁDZKÁREŇ / ZÁVOD  </strong></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="9">Obchodné meno:</td>
        <td colspan="28" style="border-right: 1px solid black;"></td>
        <td style="border-left: 1px solid black"></td>
        <td colspan="4">Názov:</td>
        <td colspan="33"  style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;border-bottom: 1px solid black;"><strong>{{ $data->customer->parent->name }}</strong></td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;border-bottom: 1px solid black;">{{ $data->customer->lastContract->branch_name }}</td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-top: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;border-top: 1px solid black;text-align: center;">Adresa:</td>
        <td style="border-left: 1px solid black;border-top: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;border-top: 1px solid black;">Adresa:</td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="5">Ulica:</td>
        <td colspan="32" style="border-right: 1px solid black;"> {{ $data->customer->parent->adresa }}</td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="5">Ulica:</td>
        <td colspan="32"  style="border-right: 1px solid black;">{{ $data->customer->lastContract->adresa }}</td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="5" style="border-bottom: 1px solid black;">Obec:</td>
        <td colspan="17" style="border-bottom: 1px solid black;">{{ $data->customer->parent->mesto }}</td>
        <td colspan="4" style="border-bottom: 1px solid black;">PSČ:</td>
        <td colspan="11" style="border-right: 1px solid black;border-bottom: 1px solid black;">{{ $data->customer->parent->psc }}</td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="5" style="border-bottom: 1px solid black;">Obec:</td>
        <td colspan="17" style="border-bottom: 1px solid black;">{{ $data->customer->lastContract->mesto }}</td>
        <td colspan="4" style="border-bottom: 1px solid black;">PSČ:</td>
        <td colspan="11" style="border-right: 1px solid black;border-bottom: 1px solid black;">{{ $data->customer->lastContract->psc }}</td>
    </tr>

    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-top: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;border-top: 1px solid black;text-align: center;">Štatutárny orgán</td>
        <td style="border-left: 1px solid black;border-top: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;border-top: 1px solid black;">Štatutárny orgán</td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="6">Meno:</td>
        <td colspan="31" style="border-right: 1px solid black;"></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="6">Meno:</td>
        <td colspan="31"  style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="6" style="">Telefón:</td>
        <td colspan="16" style="">00421908970</td>
        <td colspan="4" style="">Fax:</td>
        <td colspan="11" style="border-right: 1px solid black;"></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="6" style="">Telefón:</td>
        <td colspan="16" style=""></td>
        <td colspan="4" style="">Fax:</td>
        <td colspan="11" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="6" style="border-bottom: 1px solid black;">E-mail:</td>
        <td colspan="16" style="border-bottom: 1px solid black;"></td>
        <td colspan="4" style="border-bottom: 1px solid black;">URL:</td>
        <td colspan="11" style="border-right: 1px solid black;border-bottom: 1px solid black;"></td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="6" style="border-bottom: 1px solid black;">E-mail:</td>
        <td colspan="16" style="border-bottom: 1px solid black;"></td>
        <td colspan="4" style="border-bottom: 1px solid black;">URL:</td>
        <td colspan="11" style="border-right: 1px solid black;border-bottom: 1px solid black;"></td>
    </tr>

    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-top: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;border-top: 1px solid black;text-align: center;">Dátum podpisu</td>
        <td style="border-left: 1px solid black;border-top: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;border-top: 1px solid black;">Dátum podpisu</td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="17" style="border-right: 1px solid black;"></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="17" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;text-align: center;"></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;text-align: center;"></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;text-align: center;"></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;text-align: center;"></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;text-align: center;">Odtlačok pečiatky, meno, priezvisko a podpis</td>
        <td style="border-left: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;text-align: center;">Odtlačok pečiatky, meno, priezvisko a podpis</td>
    </tr>
    <tr>
        <td></td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="37" style="border-right: 1px solid black;border-bottom: 1px solid black;text-align: center;"></td>
        <td style="border-left: 1px solid black;border-bottom: 1px solid black;"></td>
        <td colspan="37"  style="border-right: 1px solid black;border-bottom: 1px solid black;"></td>
    </tr>

    <tr>
        <td></td>
        <td colspan="76" style="height:6px;"> </td>
    </tr>

    <tr>
        <td></td>
        <td colspan="76" style="border: 1px solid black;">OPIS ZARIADENIA NA ZHODNOCOVANIE/ZNEŠKODŇOVANIE ODPADOV</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="15" style="border-left: 1px solid black;">Číslo rozhodnutia:</td>
        <td colspan="36" style="border: 1px solid black;"></td>
        <td colspan="25" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="15" style="border-left: 1px solid black;">Kód nakladania:</td>
        <td colspan="3" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="54" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="15" style="border-left: 1px solid black;">Názov technológie:</td>
        <td colspan="61" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="17" style="border-left: 1px solid black;">Rok začatia prevádzky:</td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black;"></td>
        <td colspan="51" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-left: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="17" style="border-left: 1px solid black;">Kapacita zariadenia:</td>
        <td colspan="8" style="border: 1px solid black;"></td>
        <td colspan="16"></td>
        <td colspan="19">Hmotnosť odpadu (t/rok):</td>
        <td colspan="9" style="border: 1px solid black;">{{ $data->weight }}</td>
        <td colspan="7" style="border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;border-left: 1px solid black;border-right: 1px solid black;border-bottom: 1px solid black;"></td>
    </tr>




    <tr>
        <td></td>
        <td colspan="76" style="height:16px;"> </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="76" style="height:6px;"> </td>
    </tr>

    <tr>
        <td></td>
        <td colspan="76" style="border: 1px solid black;border-right: 1px solid black; height:27px; word-break:break-all; word-wrap:break-word;">MNOŽSTVO VÝROBKOV A MATERIÁLOV, KTORÉ SÚ VÝSLEDKOM PRÍPRAVY NA OPÄTOVNÉ POUŽITIE, RECYKLÁCIE ALEBO INÝCH ČINNOSTÍ ZHODNOCOVANIA ODPADU</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="20" style="border-right: 1px solid black;height:27px; font-size:9px; text-align: center; vertical-align: middle; word-break:break-all; word-wrap:break-word;">Názov výrobku/materiálu<br></td>
        <td colspan="20" style="border-right: 1px solid black;height:27px; font-size:9px; text-align: center; vertical-align: middle; word-break:break-all; word-wrap:break-word;">Hmotnosť výrobku/materiálu (t)</td>
        <td colspan="36" style="border-right: 1px solid black;height:27px; font-size:9px; text-align: center; vertical-align: middle; word-break:break-all; word-wrap:break-word;">Číslo certifikátu alebo iného dokladu a názov orgánu, ktorý ho vydal</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="20" style="border: 1px solid black;height:26px;"></td>
        <td colspan="20" style="border: 1px solid black;height:26px;"></td>
        <td colspan="36" style="border: 1px solid black;height:26px;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="20" style="border: 1px solid black;height:26px;"></td>
        <td colspan="20" style="border: 1px solid black;height:26px;"></td>
        <td colspan="36" style="border: 1px solid black;height:26px;"></td>
    </tr>
    <tr>
        <td></td>
        <td colspan="20" style="border: 1px solid black;height:26px;"></td>
        <td colspan="20" style="border: 1px solid black;height:26px;"></td>
        <td colspan="36" style="border: 1px solid black;height:26px;"></td>
    </tr>

</table>
