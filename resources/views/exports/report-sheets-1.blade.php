<table style="font-family:arial;font-size:8px;">
    <tr>
        <td colspan="45" style="font-family:arial;font-size:14px;font-weight:bold;text-align:center;" >EVIDENČNÝ LIST ODPADU</td>
    </tr>
    <tr>
        <td colspan="45" style="height:6px;"> </td>
    </tr>
    <tr>
        <td colspan="2">Kód činnosti</td>
        <td style="border: 1px solid black;text-align:center;">P</td>
        <td colspan="13"></td>
        <td colspan="15" style="border-left: 1px solid black;border-top: 1px solid black;">ORGANIZÁCIA / OBEC</td>
        <td colspan="14" style="border-top: 1px solid black;border-right: 1px solid black;text-align:right;">PREVÁDZKAREŇ / ZÁVOD</td>
    </tr>
    <tr>
        <td colspan="16"></td>
        <td colspan="29" style="border-left: 1px solid black;border-bottom: 1px solid black;border-right: 1px solid black;"></td>
    </tr>
    <tr>
        <td colspan="2">Kód odpadu</td>
        <td colspan="6" style="border: 1px solid black; text-align: center;">20 01 08</td>
        <td colspan="8"></td>
        <td colspan="9" style="border: 1px solid black;"></td>
        <td colspan="2" style="border: 1px solid black; text-align: center;">IČO</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[0] }}</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[1] }}</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[2] }}</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[3] }}</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[4] }}</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[5] }}</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[6] }}</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">{{ $data->arrIco[7] }}</td>
        <td colspan="10" style="border: 1px solid black;"></td>
    </tr>
    <tr>
        <td colspan="16"></td>
        <td colspan="29" rowspan="2" style="border-left: 1px solid black;border-right: 1px solid black; vertical-align: center;">
            Obchodné meno/Názov obce: {{ $data->name }}
        </td>
    </tr>
    <tr>
        <td colspan="2" rowspan="2" style="text-align: center; vertical-align: center;">Názov odpadu</td>
        <td colspan="13" rowspan="2" style="border: 1px solid black; text-align: center; vertical-align: center;">Biologicky rozložiteľný reštauračný <br> a kuchynský odpad</td>
        <td rowspan="2"></td>
    </tr>
    <tr>
        <td colspan="15" rowspan="2" style="border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; vertical-align: center;">Adresa:</td>
        <td colspan="14" rowspan="2" style="border-left: 1px solid black; border-top: 1px solid black; border-right: 1px solid black; vertical-align: center;">Adresa:</td>
    </tr>
    <tr>
        <td colspan="16"></td>
    </tr>
    <tr>
        <td colspan="2">Kategória odpadu</td>
        <td style="border: 1px solid black; text-align: center">O</td>
        <td colspan="13"></td>
        <td colspan="15" style="border-left: 1px solid black; border-right: 1px solid black;">Ulica: {{$data->adresa}}</td>
        <td colspan="14" style="border-left: 1px solid black; border-right: 1px solid black;">Ulica: {{$data->lastContract->adresa}}</td>
    </tr>
    <tr>
        <td colspan="16"></td>
        <td colspan="7" rowspan="2" style="border-left: 1px solid black; vertical-align: center">Obec: {{$data->mesto}}</td>
        <td colspan="8" rowspan="2" style="border-right: 1px solid black; vertical-align: center">PSČ: {{$data->psc}}</td>
        <td colspan="8" rowspan="2" style="border-left: 1px solid black;vertical-align: center">Obec: {{$data->lastContract->mesto}}</td>
        <td colspan="6" rowspan="2" style="border-right: 1px solid black; vertical-align: center">PSČ: {{$data->lastContract->psc}}</td>
    </tr>
    <tr>
        <td colspan="16"></td>
    </tr>
    <tr>
        <td colspan="16"></td>
        <td colspan="7" style="border-left: 1px solid black; vertical-align: center">Email: {{$data->email}}</td>
        <td colspan="8" style="border-right: 1px solid black; vertical-align: center">Fax: </td>
        <td colspan="8" style="border-left: 1px solid black;vertical-align: center">Email: </td>
        <td colspan="6" style="border-right: 1px solid black; vertical-align: center">Fax: </td>
    </tr>
    <tr>
        <td colspan="16"></td>
        <td colspan="7" style="border-left: 1px solid black; border-bottom: 1px solid black; vertical-align: center">Email: {{$data->email}}</td>
        <td colspan="8" style="border-right: 1px solid black; border-bottom: 1px solid black; vertical-align: center">URL: </td>
        <td colspan="8" style="border-left: 1px solid black; border-bottom: 1px solid black; vertical-align: center">Email: </td>
        <td colspan="6" style="border-right: 1px solid black; border-bottom: 1px solid black; vertical-align: center">URL: </td>
    </tr>
    <tr>
        <td colspan="45"></td>
    </tr>
    <tr>
        <td colspan="2" rowspan="2" style="border: 1px solid black; text-align: center; vertical-align: center;">Dátum</td>
        <td colspan="7" style="border: 1px solid black; text-align: center;">Odpad umiestnený</td>
        <td colspan="7" style="border: 1px solid black; text-align: center;">Hmotnosť odpadu (t)</td>
        <td colspan="4" rowspan="2" style="border: 1px solid black; text-align: center; vertical-align: center;">Kód nakladania</td>
        <td colspan="11" rowspan="2" style="border: 1px solid black; text-align: center; vertical-align: center;">IČO, obchodné meno predchádzajúceho/<br>/nasledujúceho držiteľa odpadu</td>
        <td colspan="11" rowspan="2" style="border: 1px solid black; text-align: center; vertical-align: center;">Poznámka</td>
        <td colspan="3" rowspan="2" style="border: 1px solid black; text-align: center; vertical-align: center;">Skratka</td>
    </tr>
    <tr>
        <td colspan="6" style="border: 1px solid black; text-align: center;">sklad</td>
        <td colspan="1" style="border: 1px solid black; text-align: center;">nádoba</td>
        <td colspan="3" style="border: 1px solid black; text-align: center;">vznik/ príjem</td>
        <td colspan="4" style="border: 1px solid black; text-align: center;">nakladanie</td>
    </tr>
    <tr>
        <td colspan="2" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">1</td>
        <td colspan="7" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">2</td>
        <td colspan="7" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">3</td>
        <td colspan="4" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">4</td>
        <td colspan="11" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">5</td>
        <td colspan="11" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">6</td>
        <td colspan="3" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">7</td>
    </tr>
    @foreach($data->list as $line)
        <tr>
            <td colspan="2" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">{{ date('d-m-Y H:m:s', strtotime($line->extraction_date)) }}</td>
            <td colspan="6" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
            <td colspan="1" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
            <td colspan="3" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">{{ $line->waste_weight / 1000 }}</td>
            <td colspan="4" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
            <td colspan="4" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">Z</td>
            <td colspan="11" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">{{ $data->ico}}, {{ $data->name }}, {{ $data->adresa}}, {{ $data->mesto }} {{ $data->psc }} </td>
            <td colspan="11" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">PD</td>
            <td colspan="3" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
        </tr>
        <tr>
            <td colspan="2" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">{{ date('d-m-Y H:m:s', strtotime($line->extraction_date)) }}</td>
            <td colspan="6" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
            <td colspan="1" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
            <td colspan="3" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
            <td colspan="4" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">{{ $line->waste_weight / 1000 }}</td>
            <td colspan="4" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">V</td>
            <td colspan="11" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">{{ $data->ico}}, {{ $data->name }}, {{ $data->adresa}}, {{ $data->mesto }} {{ $data->psc }} </td>
            <td colspan="11" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;">PRO</td>
            <td colspan="3" style="border: 1px solid black;border-bottom: 1px double black; text-align: center;"></td>
        </tr>
    @endforeach

</table>
