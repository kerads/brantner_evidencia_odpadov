<table>
    <tr>
        <th>Názov prevádzkovateľa</th>
        <th>Názov prevádzky</th>
        <th>Cena za odpad</th>
        <th>Olej v l.</th>
        <th>EUR</th>
        <th>Spolu</th>
        <th>Suma na vyplatenie</th>
    </tr>
    @php
        $total = 0;
    @endphp
    @if(!empty($data))
        @foreach($data as $line)
            @if(isset($line->write_off_oil->write_off_oil_total_price) && $line->price_total < $line->write_off_oil->write_off_oil_total_price)
                <tr>
                    <td>{{ $line->customer->name }}</td>
                    <td>{{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}</td>
                    <td>{{ $line->price_total }}</td>
                    <td>{{ $line->write_off_oil->write_off_oil_weight }}</td>
                    <td>{{ $line->contract->custom_oil_price }}</td>
                </tr>
                @php
                    $total = $total + ($line->write_off_oil->write_off_oil_total_price - $line->price_total);
                @endphp
            @endif
        @endforeach
    @endif
    <tr>
        <td colspan="7" class="text-align: right;">{{$total}}</td>
    </tr>
</table>
