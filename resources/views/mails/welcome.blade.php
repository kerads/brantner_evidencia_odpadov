<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="UTF-8">
</head>
<body>

    <p>Vitajte</p>
    <p>S radosťou Vám oznamujeme, že Vaša registrácia bola úspešná.</p>
    <p>Pred prvým prihlásením kliknite na tento link.</p>
    <p><strong><a href="{{ url('/password/reset') }}">{{ url('/password/reset') }}</a></strong></p>
    <p>Odošlite si edkaz na zmenu hesla( link bude platný 60 minút )</p>
    <p>A zadajte si vlastné heslo.</p>

    <p>s pozdravom</p>

    <p>Brantner Poprad, s.r.o.</p>

    <p>Email má informatývny charakter. V prípade záujmu kontaktujte prosím <a href="mailto:tomas.supek@brantner.sk">tomas.supek@brantner.sk</a> alebo telefonicky
        <a href="+421902650650">+421 / 902 650 650</a>.</p>

</body>
</html>
