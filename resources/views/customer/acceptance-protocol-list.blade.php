@extends('layouts.master')

@section('title')
    Preberacie protokoly
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Preberacie protokoly: {{ AUTH::User()->name }}, {{ AUTH::User()->ico }}, {{ $contract->branch_name }}</h1>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Na sklade</h6>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table width="100%" cellspacing="0">
                    <tr>
                        <td>Zberné nádoby 30L: <strong> {{ $contract->storageBarelMovement->sum('barel_30') }}</strong></td>
                        <td>Zberné nádoby 60L: <strong>{{ $contract->storageBarelMovement->sum('barel_60') }} </strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam</h6>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Dátum</th>
                        <th>Zberné nádoby 30L</th>
                        <th>Zberné nádoby 60L</th>
                        <th>PDF</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Dátum</th>
                        <th>Zberné nádoby 30L</th>
                        <th>Zberné nádoby 60L</th>
                        <th>PDF</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($contract->storageBarelMovement as $acceptanceProtocol)
                        <tr>
                            <td>{{ date('d-m-Y', strtotime($acceptanceProtocol->extraction_date)) }}</td>
                            <td>{{ $acceptanceProtocol->barel_30 }}</td>
                            <td>{{ $acceptanceProtocol->barel_60 }}</td>
                            <td class="text-center">
                                <a href="{{ url('acceptance-protocol-pdf/' . $acceptanceProtocol->id) }}">
                                    <i class="fa fa-file-pdf"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="d-flex row justify-items-between mt-2 mb-2">
        <div class="col-md-6">
            @if($contract->user_id == Auth::user()->id)
                <a href="{{ url('my-contracts') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
            @else
                <a href="{{ Session::get('signaturePadBackUrl') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
            @endif
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
