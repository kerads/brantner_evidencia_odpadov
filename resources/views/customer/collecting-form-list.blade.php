@extends('layouts.master')

@section('title')
    Zberné listy
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zberné listy</h1>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam zberných listov: {{ $customer->name }}, {{ $customer->ico }}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
                                <th>Upraviť</th>
                            @endif
                            <th>PDF</th>
                            <th>ID</th>
                            <th>Dátum</th>
                            <th>Počet sudov (KS)</th>
                            <th>Veľkosť sudov (l)</th>
                            <th>Váha odpadu (kg)</th>
                            <th>N.Z.M. Počet sudov (KS)</th>
                            <th>N.Z.M. Veľkosť sudov (l)</th>
                            <th>N.Z.M. Váha odpadu (kg)</th>
                            <th>Olej (l)</th>
                            <th>Olej - váha (kg)</th>
                            <th>Olej odpis</th>
                            <th>Tuky - váha (kg)</th>
                            <th>Podpísaná</th>
                            <th>GPS - lat</th>
                            <th>GPS - lng</th>
                                @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
                                    <th>Vymazať</th>
                                @endif
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
                                <th>Upraviť</th>
                            @endif
                            <th>PDF</th>
                            <th>ID</th>
                            <th>Dátum</th>
                            <th>Počet sudov (KS)</th>
                            <th>Veľkosť sudov (l)</th>
                            <th>Váha odpadu (kg)</th>
                            <th>N.Z.M. Počet sudov (KS)</th>
                            <th>N.Z.M. Veľkosť sudov (l)</th>
                            <th>N.Z.M. Váha odpadu (kg)</th>
                            <th>Olej (l)</th>
                            <th>Olej - váha (kg)</th>
                            <th>Olej odpis</th>
                            <th>Tuky - váha (kg)</th>
                            <th>Podpísaná</th>
                                <th>GPS - lat</th>
                                <th>GPS - lng</th>
                                @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
                                    <th>Vymazať</th>
                                @endif
                        </tr>
                    </tfoot>
                    <tbody>
                    @foreach($collectingFormList as $collectingForm)
                        <tr id="collecting-form-id-{{$collectingForm->id}}">
                            @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
                                <td>
                                    <a href="{{ url('edit-collecting-form/' . $collectingForm->id ) }}"> {{ $collectingForm->id }}</a>
                                </td>
                            @endif
                            <td>

                                <form action="{{ Auth::user()->role_id == 4 ? url('my-collecting-form-pdf') : url('collecting-form-pdf') }}" id="collecting_form_{{ $collectingForm->id }}" method="post">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ $collectingForm->id }}">
                                    <button type="submit" form="collecting_form_{{ $collectingForm->id }}"><i class="fas fa-file-pdf"></i></button>
                                </form>
                            </td>
                            <td>{{ $collectingForm->id }}</td>
                            <td>{{ date('d-m-Y H:m:s', strtotime($collectingForm->extraction_date)) }}</td>
                            <td>{{ $collectingForm->num_barels }}</td>
                            <td>{{ $collectingForm->barel_size }}</td>
                            <td>{{ $collectingForm->waste_weight }}</td>
                            <td>{{ $collectingForm->extra_num_barels }}</td>
                            <td>{{ $collectingForm->extra_barel_size }}</td>
                            <td>{{ $collectingForm->extra_waste_weight }}</td>
                            <td>{{ $collectingForm->oil_capacity }}</td>
                            <td>{{ $collectingForm->oil_capacity * 0.9 }}</td>
                            <td class="text-center">
                                @if($collectingForm->write_off == 1)
                                    <i class="fas fa-check text-success"></i>
                                @else
                                    <i class="fas fa-times text-danger"></i>
                                @endif
                            </td>
                            <td>{{ $collectingForm->fat_weight }}</td>
                            <td class="text-center">
                                @if($collectingForm->caught == 1)
                                    <i class="fas fa-check text-success"></i>
                                @else
                                    <i class="fas fa-times text-danger"></i>
                                @endif
                            </td>
                                <td>{{ $collectingForm->lat }}</td>
                                <td>{{ $collectingForm->lng }}</td>
                                @if(Auth::user()->role_id == 2 || Auth::user()->role_id == 1)
                                    <td class="text-center">
                                        <a href="#" class="delete" data-id="{{ $collectingForm->id }}">
                                            <i class="fas fa-trash-alt text-danger"></i>
                                        </a>
                                    </td>
                                @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex row justify-items-between mt-2 mb-2">
                <div class="col-md-6">
                    @if($customer->id == Auth::user()->id)
                        <a href="{{ url('my-contracts') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                    @else
                        <a href="{{ url('subsidiary/' . $customer->id) }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable({
                "order": [[ 0, 'desc' ]]
            });
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                setSearchSession()
            })
        });

        // delete
        $(".delete").click( function(e){
            e.preventDefault();
            var id = $(this).data('id');
            if (confirm("Naozaj chcete odstrániť zberný list " + id + " ?")) {
                $.ajax({
                    type: "POST",
                    url: "/delete-collecting-form",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                }).done(function( data ) {
                    removeRow();
                });
            }

            function removeRow() {
                $("#collecting-form-id-" + id).remove();
            }
        });
    </script>
@endsection
