@extends('layouts.master')

@section('title')
    Zmluva
@endsection

@section('content')
    <div class="row col-12">
            <div class="row col-12">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Zmluva : {{ Auth::user()->name }}, IČO: {{ Auth::user()->ico }}
                        </div>

                        <div class="card-body">

                            <div class="form-group row">
                                <label for="branch_name" class="col-md-4 col-form-label text-md-right">Názov prevádzky</label>

                                <div class="col-md-8">
                                    <input
                                        id="branch_name"
                                        type="text"
                                        class="form-control"
                                        name="branch_name"
                                        value="{{ $contract->branch_name }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="external_id" class="col-md-4 col-form-label text-md-right">Externé číslo</label>

                                <div class="col-md-8">
                                    <input id="external_id"
                                           type="number"
                                           class="form-control"
                                           name="external_id"
                                           value="{{ $contract->external_id }}"
                                           disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="adresa" class="col-md-4 col-form-label text-md-right">Ulica</label>

                                <div class="col-md-8">
                                    <input id="adresa"
                                           type="text"
                                           class="form-control"
                                           name="adresa"
                                           value="{{ $contract->adresa }}"
                                           disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="psc" class="col-md-4 col-form-label text-md-right">PSČ</label>

                                <div class="col-md-8">
                                    <input id="psc"
                                           type="text"
                                           class="form-control"
                                           name="psc"
                                           value="{{ $contract->psc }}"
                                           disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mesto" class="col-md-4 col-form-label text-md-right">Mesto</label>

                                <div class="col-md-8">
                                    <input id="mesto"
                                           type="text"
                                           class="form-control"
                                           name="mesto"
                                           value="{{ $contract->mesto }}"
                                           disabled
                                    />
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="validity_recurrency_until_id" class="col-md-4 col-form-label text-md-right">
                                    Platnosť
                                </label>

                                <div class="col-md-8">
                                    <select name="validity_recurrency_until_id"
                                            class="form-control"
                                            disabled
                                    >
                                        <option value="-1"></option>
                                        @foreach($recurrencyList as $vru)
                                            <option value="{{ $vru->id }}"
                                                {{ $contract->validity_recurrency_until_id  == $vru->id  ? 'selected' : '' }}>
                                                {{ __("orders." . $vru->name) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="start_date" class="col-md-4 col-form-label text-md-right">
                                    Začiatok zmluvy
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="date"
                                        class="form-control"
                                        name="start_date"
                                        value="{{ $contract->start_date }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row" id="recurrency_until_date">
                                <label for="recurrency_until_date" class="col-md-4 col-form-label text-md-right">
                                    Opakovať do dátumu
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="date"
                                        class="form-control"
                                        name="recurrency_until_date"
                                        value="{{ $contract->recurrency_until_date }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row" id="recurrency_num_of_repetitions">
                                <label for="recurrency_num_of_repetitions" class="col-md-4 col-form-label text-md-right">
                                    Celkový počet opakovaní
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        class="form-control recurrency"
                                        name="recurrency_num_of_repetitions"
                                        value="{{ $contract->recurrency_num_of_repetitions }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row" id="repeating_cycle_id">
                                <label for="repeating_cycle_id" class="col-md-4 col-form-label text-md-right">
                                    Cyklus opakovania
                                </label>
                                <div class="col-md-8">
                                    <select name="repeating_cycle_id"
                                            class="form-control recurrency"
                                            disabled
                                    >
                                        <option value="-1"></option>
                                        @foreach($repeatingCycleList as $cycle)
                                            <option value="{{ $cycle->id }}"
                                                {{ $contract->repeating_cycle_id  == $cycle->id  ? 'selected' : '' }}>
                                                {{ $cycle->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="recurrency_week_days">
                                <label for="recurrency_week_days" class="col-xl-12 col-lg-12 col-md-12 text-right">
                                    Deň vývozu
                                </label>
                                <div class="d-flex flex-column align-items-end col-xl-12 col-lg-12 col-md-12">
                                    @for ($i = 1; $i < 8; $i++) {{-- 0 is Monday --}}
                                        <label class="checkbox-custom-label" for="recurrency_week_days[{{ $i }}]">
                                            <input
                                                type="checkbox"
                                                class="checkbox-custom"
                                                name="recurrency_week_days[]"
                                                id="recurrency_week_days[{{ $i }}]"
                                                value="{{ $i }}"
                                                @if(in_array($i, ($contract->stringToArray($contract->recurrency_week_days) ))) checked @endif
                                                disabled
                                            >
                                            {{ __('orders.' . jddayofweek($i-1,1)) }}
                                        </label>
                                    @endfor
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="num_barels" class="col-md-4 col-form-label text-md-right">
                                    Počet sudov za týždeň <span id="min-max-barels">(min/priemer/max) </span>ks
                                </label>
                                <div class="col-md-8 flex-row">
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-4 recurrency"
                                        id="min_num_barels"
                                        name="min_num_barels"
                                        value="{{ $contract->min_num_barels }}"
                                        disabled
                                    />
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control recurrency"
                                        id="num_barels"
                                        name="num_barels"
                                        value="{{ $contract->num_barels }}"
                                        disabled
                                    />
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-4 recurrency"
                                        id="max_num_barels"
                                        name="max_num_barels"
                                        value="{{ $contract->max_num_barels }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="stored_barels_30" class="col-md-4 col-form-label text-md-right">
                                    Počet sudov na sklade 30L
                                </label>
                                <div class="col-md-8 flex-row">
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-12 recurrency"
                                            id="stored_barels_30"
                                        name="stored_barels_30"
                                        value="{{ $contract->stored_barels_30 }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="stored_barels_60" class="col-md-4 col-form-label text-md-right">
                                    Počet sudov na sklade 60L
                                </label>
                                <div class="col-md-8 flex-row">
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-12 recurrency"
                                        id="stored_barels_60"
                                        name="stored_barels_60"
                                        value="{{ $contract->stored_barels_60 }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="validity_recurrency_until_id" class="col-md-4 col-form-label text-md-right">
                                    Veľkosť sudov
                                </label>

                                <div class="col-md-8">
                                    <select name="barel_size"
                                            class="form-control"
                                            disabled
                                    >
                                        <option value=""></option>
                                        <option value="30"
                                            {{ $contract->barel_size == 30  ? 'selected' : '' }}>
                                            <span>30 L</span>
                                        </option>
                                        <option value="60"
                                            {{ $contract->barel_size == 60  ? 'selected' : '' }}>
                                            <span>60 L</span>
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="price_type">
                                <label for="price_type" class="col-md-4 col-form-label text-md-right">
                                    Cena
                                </label>
                                <div class="col-md-8">
                                    <select
                                        id="price_type"
                                        name="price_type"
                                        class="form-control recurrency"
                                        disabled
                                    >
                                        <option value="1"
                                            {{ $contract->price_type == 1  ? 'selected' : '' }}>
                                            Cenníka
                                        </option>
                                        <option value="2"
                                            {{ $contract->price_type == 2  ? 'selected' : '' }}>
                                            Zmluvná cena
                                        </option>
                                        <option value="3"
                                            {{ $contract->price_type == 3  ? 'selected' : '' }}>
                                            Paušál
                                        </option>
                                        <option value="4"
                                            {{ $contract->price_type == 4  ? 'selected' : '' }}>
                                            Ohraničený paušál
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="price_id">
                                <label for="price_id" class="col-md-4 col-form-label text-md-right">
                                    Cena vývoz €
                                </label>

                                <div class="col-md-8">
                                    <select name="price_id"
                                            class="form-control"
                                            disabled
                                    >
                                        <option value="-1"></option>
                                        @foreach($priceList as $price)
                                            <option value="{{ $price->id }}"
                                                {{ old('price_id', isset($contract) ? $contract->price_id : '')  == $price->id  ? 'selected' : '' }}>
                                                {{ $price->name }} - {{ $price->price }} €
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_price">
                                <label for="custom_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency"
                                        name="custom_price"
                                        value="{{ $contract->custom_price }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row" id="discount">
                                <label for="discount" class="col-md-4 col-form-label text-md-right">
                                    Zľava %
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        min="0"
                                        max="100"
                                        class="form-control recurrency"
                                        name="discount"
                                        value="{{ $contract->discount }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="custom_above_quantity_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena odpad nad zmluvné množstvo
                                </label>
                                <input type="hidden" name="custom_above_quantity_price" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input
                                            type="checkbox"
                                            name="custom_above_quantity_price"
                                            value="1"
                                            id="custom_above_quantity_price"
                                            {{ $contract->custom_above_quantity_price == 1 ? 'checked' : ''}}
                                            disabled
                                        />
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="above_quantity_price">
                                <label for="above_quantity_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena za nádobu nad zmluvné množstvo €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency"
                                        name="above_quantity_price"
                                        value="{{ $contract->above_quantity_price }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="custom_desinfection" class="col-md-4 col-form-label text-md-right">
                                    Paušálna dezinfekcia
                                </label>
                                <input type="hidden" name="custom_desinfection" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input
                                            type="checkbox"
                                            name="custom_desinfection"
                                            value="1"
                                            id="custom_desinfection"
                                            {{ $contract->custom_desinfection == 1 ? 'checked' : ''}}
                                            disabled
                                        />
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_desinfection_price">
                                <label for="custom_desinfection_price" class="col-md-4 col-form-label text-md-right">
                                    Paušálna cena za dezinfekciu €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency"
                                        name="custom_desinfection_price"
                                        value="{{ $contract->custom_desinfection_price }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Iný druh odpadu
                                </label>
                                <div class="col-md-8 text-center">
                                    @foreach($contract->customWasteType as $wasteType)
                                            {{ $wasteType->code }} - {{ $wasteType->name }} <br/>
                                    @endforeach

                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="oil_switcher" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena oleja
                                </label>
                                <input type="hidden" name="oil_switcher" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input
                                            type="checkbox"
                                            name="oil_switcher"
                                            value="1"
                                            id="oil_switcher"
                                            {{ $contract->custom_oil_price != null ? 'checked' : ''}}
                                            disabled
                                        />
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_oil_price">
                                <label for="custom_oil_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena za olej €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency"
                                        name="custom_oil_price"
                                        value="{{ $contract->custom_oil_price }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="fat_switcher" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena tuku
                                </label>
                                <input type="hidden" name="fat_switcher" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input
                                            type="checkbox"
                                            name="fat_switcher"
                                            value="1"
                                            id="fat_switcher"
                                            {{ $contract->custom_fat_price != null ? 'checked' : ''}}
                                            disabled
                                        />
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_fat_price">
                                <label for="custom_fat_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena za tuk €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency"
                                        name="custom_fat_price"
                                        value="{{ $contract->custom_fat_price }}"
                                        disabled
                                    />
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="poznamka" class="col-xl-12 col-lg-12 col-md-12">Poznámka</label>
                                <div class="col-12">
                                    <textarea
                                        id="poznamka"
                                        class="form-control"
                                        name="poznamka"
                                        rows="4"
                                        disabled
                                    >{{ $contract->poznamka }}</textarea>
                                </div>
                            </div>

                            <hr>

                            <div class="d-flex row justify-items-between mt-2 mb-2">
                                <div class="col-md-4">
                                    <a href="{{ url('my-contracts') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                                </div>
                                <div class="col-md-4 text-center">
                                    <div class="form-group row">
                                        <label for="closed_at" class="col-md-4 col-form-label text-md-right">
                                            Dátum ukončenia
                                        </label>
                                        <div class="col-md-8">
                                            <input
                                                type="date"
                                                class="form-control"
                                                name="closed_at"
                                                value="{{ $contract->closed_at }}"
                                                disabled
                                            />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 text-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        // Recurrency until
        $('select[name="validity_recurrency_until_id"]').on('change', function(){
            recurrencyUntil();
        });

        recurrencyUntil();

        function recurrencyUntil(){
            var recurrencyUntil = $( 'select[name="validity_recurrency_until_id"]').val();

            if(recurrencyUntil == 2){ // do datumu
                $('#recurrency_until_date').show();
                $('#recurrency_num_of_repetitions').hide();
            } else if(recurrencyUntil == 3){  // pocet opakovani
                $('#recurrency_until_date').hide();
                $('#recurrency_num_of_repetitions').show();
            } else{
                $('#recurrency_until_date').hide();
                $('#recurrency_num_of_repetitions').hide();
            }
        }

        // Price switcher prepinanie medzi cenami
        $('select[name="price_type"]').on('change', function(){
            priceSwitcher();
        })

        priceSwitcher();

        function priceSwitcher(){
            // let customPrice = $('input[name="price_switcher"]:checked').length;
            let customPrice = $('select[name="price_type"] option:selected').val();

            if(customPrice == 1){
                $('#custom_price').fadeOut();
                $('#price_id').fadeIn();
                $('#discount').fadeIn();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            } else if(customPrice == 2 ){
                $('#custom_price').fadeIn();
                $('#price_id').fadeOut();
                $('#discount').fadeOut();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            } else if(customPrice == 3){
                $('#custom_price').fadeIn();
                $('#price_id').fadeOut();
                $('#discount').fadeOut();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            } else if(customPrice == 4){
                $('#custom_price').fadeIn();
                $('#price_id').fadeOut();
                $('#discount').fadeOut();
                $('#min_num_barels').fadeIn();
                $('#max_num_barels').fadeIn();
                $('#min-max-barels').fadeIn();
            } else {
                $('select[name="price_type"] option:selected').val(1)
                $('#custom_price').fadeOut();
                $('#price_id').fadeIn();
                $('#discount').fadeIn();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            }
        }

        $('input[name="custom_above_quantity_price"]').on('change', function(){
            aboveQuantityPrice();
        });

        aboveQuantityPrice();

        function aboveQuantityPrice(){
            let customAboveQtyPrice = $('input[name="custom_above_quantity_price"]:checked').length;

            if(customAboveQtyPrice == 1){
                $('#above_quantity_price').fadeIn();
            } else{
                $('#above_quantity_price').fadeOut();
            }
        }

        $('input[name="custom_desinfection"]').on('change', function(){
            customDesinfection();
        });

        customDesinfection();

        function customDesinfection(){
            let customDesinfection = $('input[name="custom_desinfection"]:checked').length;

            if(customDesinfection == 1){
                $('#custom_desinfection_price').fadeIn();
            } else{
                $('#custom_desinfection_price').fadeOut();
            }
        }

        $('input[name="oil_switcher"]').on('change', function(){
            oilSwitcher();
        });

        oilSwitcher();

        function oilSwitcher(){
            let customOil = $('input[name="oil_switcher"]:checked').length;

            if(customOil == 1){
                $('#custom_oil_price').fadeIn();
            } else{
                $('#custom_oil_price').fadeOut();
            }
        }

        $("#close_contract").on('click', function(){
            let id = $(this).data('id');
            let date = $('input[name="closed_at"]').val();

            if(!$(this).is('[disabled=""]')){
                $.ajax({
                    type: "POST",
                    url: "/close-contract",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                        closed_at: date,
                    },
                }).done(function (data) {
                    $("#close_contract").attr( "disabled", "disabled" );
                    $("#close_contract").addClass( "disabled");
                });
            };
        });

        // pridat iny druh odpadu
        $("#new-custom-waste").click( function(e){
            let isContract = $(this).data('is-contract');

            if(isContract == 1) {
                if(!confirm('Ak budete pokračovať, prídete o všetky neuložené zmeny. Naozaj chcete pokračovať?')){
                    e.preventDefault();
                }
            } else {
                e.preventDefault();
                alert('Najprv uložte zmluvu, až tak môžete pridať druh odpadu');
            }
            console.log(isContract);
        });
    </script>
@endsection
