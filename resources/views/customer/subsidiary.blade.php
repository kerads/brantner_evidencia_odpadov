@extends('layouts.master')

@section('title')
    Moje zmluvy
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zmluvy</h1>
        <a href="{{ url('new-contract/' . AUTH::User()->id) }}">
            <button class="btn btn-primary">
                <i class="fa fa-user-plus"> Pridať Zmluvu</i>
            </button>
        </a>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zmluvy zoznam: {{ AUTH::User()->name }}, {{ AUTH::User()->ico }}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Meno prevádzky</th>
                        <th>Adresa</th>
                        <th>PSČ</th>
                        <th>Mesto</th>
                        <th>Začiatok</th>
                        <th>Počet sudov</th>
                        <th>Veľkosť sudov</th>
                        <th>Zmluva</th>
                        <th>Zrušená dňa</th>
                        <th>Zberné listy</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Meno prevádzky</th>
                        <th>Adresa</th>
                        <th>PSČ</th>
                        <th>Mesto</th>
                        <th>Začiatok</th>
                        <th>Počet sudov</th>
                        <th>Veľkosť sudov</th>
                        <th>Zmluva</th>
                        <th>Zrušená dňa</th>
                        <th>Zberné listy</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($user->contract as $contract)
                        @if($contract->deleted == 0)
                            <tr id="contract-id-{{ $contract->id }}">
                                <td>{{ !empty($contract->branch_name) ? $contract->branch_name : AUTH::User()->name }}</td>
                                <td>{{ $contract->adresa }}</td>
                                <td>{{ $contract->psc }}</td>
                                <td>{{ $contract->mesto }}</td>
                                <td>{{ $contract->start_date }}</td>
                                <td>{{ $contract->num_barels }}</td>
                                <td>{{ $contract->barel_size }}</td>
                                <td class="text-center">
                                    @if(isset($contract->contract) && $contract->contract != null)
                                        <a href="{{ url('storage/app/public/zmluvy/' . AUTH::User()->id . '/' . $contract->contract) }}" target="_blank">
                                            <i class="fas fa-file-pdf"></i>
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    @if($contract->closed == 1)
                                        <span class=" text-danger">{{ $contract->closed_at }}</span>
                                    @else
                                        <i class="fa fa-running text-success"> aktívna</i>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{ url('my-collecting-forms/' . $contract->id) }}">
                                        <i class="fas fa-list"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="d-flex row justify-items-between mt-4 mb-2">
                <div class="col-md-12">
                    <a href="{{ url('my-companies') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
