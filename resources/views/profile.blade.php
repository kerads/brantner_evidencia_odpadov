<?php
//echo '<pre>';
//var_dump(AUTH::User());
//echo '<pre>';
//exit;
?>
@extends('layouts.master')

@section('title')
    Profil
@endsection

@section('content')
    <div class="row col-12">
        <div class="card col-12 p-0">
            <div class="card-header col-12">
                Môj profil
            </div>

            <div class="card-body">

                <div class="form-group row">
                    <div class="col-md-4 col-form-label text-md-right">Meno</div>

                    <div class="col-md-8 form-control">{{ AUTH::User()->name }}</div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 col-form-label text-md-right">E-Mail</div>

                    <div class="col-md-8 form-control">{{ AUTH::User()->email }}</div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4 col-form-label text-md-right">Telefón</div>

                    <div class="col-md-8 form-control">{{ AUTH::User()->telefon }}</div>
                </div>

                @if(AUTH::User()->role_id == 3)
                    <hr>
                    <div class="form-group row">

                        <form method="POST" action="{{ url('driver-update') }}" class="col-12 bordered" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="id" value="{{AUTH::User()->id}}">
                            <input type="hidden" name="role_id" value="{{AUTH::User()->role_id}}">

                            <div class="form-group row">
                                <label for="vrn" class="col-md-4 col-form-label text-md-right">ŠPZ</label>

                                <div class="col-md-8">
                                    <input id="vrn"
                                           type="text"
                                           class="form-control @error('vrn') is-invalid @enderror"
                                           name="vrn"
                                           maxlength="7"
                                           value="{{ old('vrn', AUTH::User()->vrn ) }}"
                                           >

                                    @error('vrn')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="signature" class="col-md-4 col-form-label text-md-right">Podpis</label>

                                <div class="col-md-8">
                                    @include('driver.partials.singnature-pad', ['user' => AUTH::User()])
                                </div>
                            </div>

                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-user-edit"> Uložiť</i>
                                </button>
                            </div>
                        </form>
                    </div>
                @endif

                @if(AUTH::User()->role_id == 4)
                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">IČO</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->ico }}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">DIČ</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->dic }}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">IČDPH</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->icdph }}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">Ulica</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->adresa }}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">PSČ</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->psc }}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">Mesto</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->mesto }}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">Zodpovedný pracovník</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->zodpovedny_veduci }}</div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4 col-form-label text-md-right">Telefón - z. pracovník</div>

                        <div class="col-md-8 form-control">{{ AUTH::User()->z_telefon }}</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
