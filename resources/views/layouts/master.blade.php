<!doctype html>
{{--<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">--}}
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Radoslav Kecka rkecka@gmail.com">

    <!-- CSRF Token -->
{{--    <meta name="csrf-token" content="{{ csrf_token() }}">--}}

    <title>@yield('title')</title>

    <!-- Scripts -->
{{--    <script src="{{ asset('public/js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('public/themes/fontawesome-free-5.14.0-web/css/all.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/themes/sb-admin-2/css/sb-admin-2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/eo.css') }}">
    @yield('head')

</head>

<body id="page-top">
    <div id="wrapper">
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            @include('partials.sidebar')
        </ul>

        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    @include('partials.topbar')
                </nav>

                <div class="container-fluid">
                    @include('partials.flash-message')
                    @yield('content')
                </div>
            </div>

            <footer class="sticky-footer bg-white">
                @include('partials.footer')
            </footer>
        </div>
    </div>

    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="login.html">Logout</a>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ url('public/themes/sb-admin-2/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('vendor/phpunit/php-code-coverage/src/Report/Html/Renderer/Template/js/popper.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ url('public/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ url('public/themes/sb-admin-2/js/sb-admin-2.min.js') }}"></script>
    <script src="{{ url('public/js/app.js') }}"></script>
    @yield('script')
</body>
</html>
