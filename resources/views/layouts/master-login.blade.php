<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Radoslav Kecka rkecka@gmail.com">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @yield('title')
    </title>

    <!-- Scripts -->
{{--    <script src="{{ asset('public/js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('public/themes/fontawesome-free-5.14.0-web/css/all.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('public/themes/sb-admin-2/css/sb-admin-2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/eo.css') }}">

</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            @include('partials.top-menu')
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

    <footer>
            @include('partials.footer')
    </footer>
</body>
</html>
