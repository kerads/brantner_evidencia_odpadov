@extends('layouts.master')

@section('title')
    Zberné listy
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zberné listy</h1>
        <form action="{{ url('export-collecting-form-list') }}" method="GET">
            @csrf
            <div class="input-group d-flex justify-content-center align-items-center">
                <label for="date_from_excel" class="mr-2">Export do excelu <i class="fa fa-table"></i></label>
                <input
                    id="date_from_excel"
                    type="datetime-local"
                    name="date_from"
                    class="form-control small"
                    value="{{ old('date_from', $date_from) }}">
                <input
                    type="datetime-local"
                    name="date_to"
                    class="form-control small"
                    value="{{ old('date_to', $date_to) }}">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                        <i class="fas fa-download fa-sm"></i>
                    </button>
                </div>
            </div>
        </form>

    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zberné listy</h6>
            <form action="{{ url('reports/collecting-form-list') }}" method="POST">
                @csrf
                <div class="input-group">
                    <input
                        type="datetime-local"
                        name="date_from"
                        class="form-control small"
                        value="{{ old('date_from', $date_from) }}">
                    <input
                        type="datetime-local"
                        name="date_to"
                        class="form-control small"
                        value="{{ old('date_to', $date_to) }}">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Dátum prevzatia</th>
                            <th>Názov prevádzkovateľa</th>
                            <th>Názov prevádzky</th>
                            <th>Adresa prevádzky</th>
                            <th>Váha (t)</th>
                            <th>Počet sudov</th>
                            <th>Olej</th>
                            <th>Toal. papier</th>
                            <th>Hyg. vreckovky</th>
                            <th>Kuch. utierky</th>
                            <th>Olej za olej</th>
                            <th>Tuk</th>
                            <th>Toal. papier</th>
                            <th>Hyg. vreckovky</th>
                            <th>Kuch. utierky</th>
                            <th>Tuk za olej</th>
                            <th>Vodič</th>
                            <th>ŠPZ</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Dátum prevzatia</th>
                            <th>Názov prevádzkovateľa</th>
                            <th>Názov prevádzky</th>
                            <th>Adresa prevádzky</th>
                            <th>Váha (t)</th>
                            <th>Počet sudov</th>
                            <th>Olej</th>
                            <th>Toal. papier</th>
                            <th>Hyg. vreckovky</th>
                            <th>Kuch. utierky</th>
                            <th>Olej za olej</th>
                            <th>Tuk</th>
                            <th>Toal. papier</th>
                            <th>Hyg. vreckovky</th>
                            <th>Kuch. utierky</th>
                            <th>Tuk za olej</th>
                            <th>Vodič</th>
                            <th>ŠPZ</th>
                        </tr>
                    </tfoot>
                    <tbody>
                    @include('user.partials.collecting-form-line', ['collectingForms' => $collectingForms])
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#table').DataTable();

            let currentPage = table.page.info().page;
            $('#table').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
