@extends('layouts.master')

@section('title')
    Test
@endsection

@section('content')
    <div class="row col-12">
        <div class="demo">
            tralala 1
        </div>
    </div>
@endsection

@section('script')
    <script>
        var x = document.getElementById("demo");

        getLocation();
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }

        function showPosition(position) {
            console.log('position');
            console.log(position);
            console.log(position.coords);
            console.log(position.coords.latitude);
            console.log(position.coords.longitude);
            // x.innerHTML = "Latitude: " + position.coords.latitude +
            //     "<br>Longitude: " + position.coords.longitude;
        }
    </script>
@endsection
