@if(!empty($collectingForms))
    @foreach($collectingForms as $line)
        <tr>
            <td>{{ $line->id }}</td>
            <td>{{ $line->extraction_date }}</td>
            <td>{{ $line->customer->name }}</td>
            <td>
                {{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}
            </td>
            <td>
                {{ $line->contract->mesto }}, {{ $line->contract->psc }}, {{ $line->contract->adresa }}
            </td>
            <td>{{ $line->waste_weight + $line->extra_waste_weight }}</td>
            <td>{{ $line->num_barels + $line->extra_num_barels }}</td>
            <td>{{ $line->oil_capacity }}</td>
            <td>{{ $line->toilet_paper }}</td>
            <td>{{ $line->hygienic_handkerchiefs }}</td>
            <td>{{ $line->kitchen_towels }}</td>
            <td>{{ $line->oil_for_oil }}</td>
            <td>{{ $line->fat_weight }}</td>
            <td>{{ $line->fat_toilet_paper }}</td>
            <td>{{ $line->fat_hygienic_handkerchiefs }}</td>
            <td>{{ $line->fat_kitchen_towels }}</td>
            <td>{{ $line->fat_for_oil }}</td>
            <td>{{ $line->driver->name }}</td>
            <td>{{ $line->vrn }}</td>
        </tr>
    @endforeach
@endif
