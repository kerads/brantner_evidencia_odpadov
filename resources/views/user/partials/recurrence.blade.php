<div
    class="d-flex flex-row align-items-center col-xl-6 col-lg-6">
    <label for="recurring_order" class="col-xl-6 col-lg-6">
        {{ __('orders.Recurring order') }}
    </label>
    <div class="checkbox">
        <label class="checkbox-custom-label" for="approved">
        </label>
        <input
            type="checkbox"
            class="checkbox-custom recurrency @error('recurring_order') alert alert-danger @enderror"
            name="recurring_order"
            id="recurring_order"
            value="1"
            {{ old('recurring_order', !isset($user) ? '' : $user->recurring_order) == 1 ? 'checked' : '' }}
        >
    </div>
</div>

<div class="w-100 col-xl-6 col-lg-6" id="validity_recurrency_until">
    <div id="dynamic_recurrency" class="w-100 flex-wrap mt-3 mb-3 h-25">
        <div class="row">
            <label for="validity_recurrency_until" class="col-xl-6 col-lg-6">
                Opakovanie
            </label>
            <select name="validity_recurrency_until"
                    class="form-control recurrency col-xl-6 col-lg-6 @error('validity_recurrency_until') alert alert-danger @enderror">
                <option value="-1"></option>
                <option value="Jednorazova"
                    {{ old('validity_recurrency_until', isset($user) ? $user->validity_recurrency_until : '')  == 'jednorazova'  ? 'selected' : '' }}>
                    Jednorázová
                </option>
                <option value="Do zrušenia"
                    {{ old('validity_recurrency_until', isset($user) ? $user->validity_recurrency_until : '')  == 'Do zrušenia'  ? 'selected' : '' }}>
                    Do zrušenia
                </option>
                <option value="Do dátumu"
                    {{ old('validity_recurrency_until', isset($user) ? $user->validity_recurrency_until : '')  == 'Do dátumu'  ? 'selected' : '' }}>
                    Do dátumu
                </option>
                <option value="Počet opakovaní"
                    {{ old('validity_recurrency_until', isset($user) ? $user->validity_recurrency_until : '')  == 'Počet opakovaní'  ? 'selected' : '' }}>
                    Počet opakovaní
                </option>
            </select>
        </div>
    </div>
</div>

<div class="w-100 col-xl-6 col-lg-6" id="recurrency_until_date">
    <div class="w-100 flex-wrap mt-3 mb-3 h-25">
        <div class="row">
            <label for="recurrency_until_date" class="col-xl-6 col-lg-6">
                {{ __('orders.Recurrency until date') }}
            </label>
            <input
                type="date"
                class="form-control recurrency col-xl-6 col-lg-6 @error('recurrency_until_date') alert alert-danger @enderror"
                name="recurrency_until_date"
                value="{{ old('recurrency_until_date', !isset($user) ? '' : $user->recurrency_until_date)}}"
            >
        </div>
    </div>
</div>

<div class="w-100 col-xl-6 col-lg-6" id="recurrency_num_of_repetitions">
    <div class="w-100 flex-wrap mt-3 mb-3 h-25">
        <div class="row">
            <label for="recurrency_num_of_repetitions" class="col-xl-6 col-lg-6">
                {{ __('orders.Recurrency num of repetitions') }}
            </label>
            <input
                type="number"
                class="form-control recurrency col-xl-6 col-lg-6 @error('recurrency_num_of_repetitions') alert alert-danger @enderror"
                name="recurrency_num_of_repetitions"
                value="{{ old('recurrency_num_of_repetitions', !isset($user) ? '' : $user->recurrency_num_of_repetitions)}}"
            >
        </div>
    </div>
</div>


<div class="w-100 col-xl-6 col-lg-6" id="recurrency_circle">
    <div class="w-100 flex-wrap mt-3 mb-3 h-25">
        <div class="row">
            <label for="recurrency_circle" class="col-xl-6 col-lg-6">
                {{ __('orders.Recurency circle') }}
            </label>
            <select name="recurrency_circle"
                    class="form-control recurrency col-xl-6 col-lg-6 @error('recurrency_circle') alert alert-danger @enderror">
                <option value=""></option>
                <option value="week"
                    {{ old('recurrency_circle', isset($user) ? $user->recurrency_circle : '')  == 'week'  ? 'selected' : '' }}>
                    {{ __('orders.week' ) }}
                </option>
                <option value="month"
                    {{ old('recurrency_circle', isset($user->recurrency_circle) ? $user->recurrency_circle : '')  == 'month'  ? 'selected' : '' }}>
                    {{ __('orders.month' ) }}
                </option>
            </select>
        </div>
    </div>
</div>

<div class="w-100 col-xl-6 col-lg-6" id="recurrency_month_days">
    <div id="recurrency_month_days" class="w-100 flex-wrap mt-3 mb-3 h-25">
        <div class="row">
            <label for="recurrency_month_days" class="col-xl-6 col-lg-6">
                {{ __('orders.Recurency month days') }}
            </label>
            <div class="d-flex flex-column">
                @for ($i = 1; $i < 32; $i++)
                    @if(($i % 7) == 1)
                        <div class="d-flex">
                            @endif
                            <label class="checkbox-custom-label w-3em text-right" for="recurrency_month_days[{{ $i }}]">
                                {{ $i }}
                                <input
                                    type="checkbox"
                                    class="checkbox-custom  @error('contractor') alert alert-danger @enderror"
                                    name="recurrency_month_days[]"
                                    id="recurrency_month_days[{{ $i }}]"
                                    value="{{ $i }}"
                                    {{ array_key_exists($i, old('recurrency_month_days', !isset($user->recurrency_month_days) ? [''] : $user->recurrency_month_days)) ? 'checked' : '' }}
                                >
                            </label>
                            @if(($i % 7) == 0 || $i == 31)
                        </div>
                    @endif
                @endfor
            </div>
        </div>
    </div>
</div>

<div class="w-100 col-xl-6 col-lg-6" id="recurrency_week_days">
    <div id="recurrency_week_days" class="w-100 flex-wrap mt-3 mb-3 h-25">
        <div class="row">
            <label for="recurrency_week_days" class="col-xl-6 col-lg-6">
                {{ __('orders.Recurency week days') }}
            </label>
            <div class="d-flex flex-column">
                @for ($i = 0; $i < 7; $i++) {{-- 0 is Monday --}}
                <label class="checkbox-custom-label" for="recurrency_week_days[{{ $i }}]">
                    <input
                        type="checkbox"
                        class="checkbox-custom  @error('contractor') alert alert-danger @enderror"
                        name="recurrency_week_days[]"
                        id="recurrency_week_days[{{ $i }}]"
                        value="{{ $i }}"
                        {{ array_key_exists($i, old('recurrency_week_days', !isset($user->recurrency_week_days) ? [''] : $user->recurrency_week_days)) ? 'checked' : '' }}
                    >
                    {{ __('orders.' . jddayofweek($i,1)) }}
                </label>
                @endfor
            </div>
        </div>
    </div>
</div>
