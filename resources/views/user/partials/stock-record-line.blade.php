@foreach($list as $line)
    @if($line->waste_weight > 0)
        <tr>
            <td>{{ $line->customer->name }}</td>
            <td>
                {{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}
            </td>
            <td>
                {{ $line->contract->mesto }}, {{ $line->contract->psc }}, {{ $line->contract->adresa }}
            </td>
            <td>20 01 08</td>
            <td>{{ $line->waste_weight + $line->extra_waste_weight }}</td>
            <td>{{ $line->price_total }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $line->num_barels }}</td>
            <td>{{ $line->extra_num_barels }}</td>
            <td>{{ $line->extra_price_total }}</td>
            <td>{{ $line->desinfection_price }}</td>
            <td>
                @if($line->contract->custom_price == null)
                    {{ isset($line->contract->price->name) ? $line->contract->price->name : '' }}
                @else
                    Odvoz {{ $line->contract->num_barels }} ZN zmluvná cena
                @endif
            </td>
            <td>{{ $line->customer->external_id }}</td>
            <td>{{ $line->contract->external_id }}</td>
            <td>{{ $line->contract->poznamka }}</td>
        </tr>
    @endif

{{--    @if($line->fat_weight > 0)--}}
{{--        <tr style="background-color: #f6c23e; color: #ffffff;">--}}
{{--            <td>{{ $line->customer->name }}</td>--}}
{{--            <td>--}}
{{--                {{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}--}}
{{--            </td>--}}
{{--            <td>--}}
{{--                {{ $line->contract->mesto }}, {{ $line->contract->psc }}, {{ $line->contract->adresa }}--}}
{{--            </td>--}}
{{--            <td>20 01 25</td>--}}
{{--            <td>{{ $line->fat_weight }}</td>--}}
{{--            <td>0</td>--}}
{{--            <td></td>--}}
{{--            <td></td>--}}
{{--            <td></td>--}}
{{--            <td></td>--}}
{{--            <td></td>--}}
{{--            <td>0</td>--}}
{{--            <td></td>--}}
{{--            <td></td>--}}
{{--            <td style="background-color: #f6c23e; color: #ffffff;">--}}
{{--                Odber Tukov--}}
{{--            </td>--}}
{{--            <td>{{ $line->customer->external_id }}</td>--}}
{{--            <td>{{ $line->contract->external_id }}</td>--}}
{{--            <td>{{ $line->contract->poznamka }}</td>--}}
{{--        </tr>--}}
{{--    @endif--}}

    @if(isset($line->write_off_oil))
        <tr style="background-color: #1cc88a; color: #ffffff;">
            <td>{{ $line->customer->name }}</td>
            <td>
                {{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}
            </td>
            <td>
                {{ $line->contract->mesto }}, {{ $line->contract->psc }}, {{ $line->contract->adresa }}
            </td>
            <td>20 01 25</td>
            <td>{{ $line->write_off_oil->write_off_oil_weight }}</td>
            <td>{{ $line->write_off_oil->write_off_oil_total_price }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>0</td>
            <td></td>
            <td></td>
            <td style="background-color: #1cc88a; color: #ffffff;">
                Olej odpis
            </td>
            <td>{{ $line->customer->external_id }}</td>
            <td>{{ $line->contract->external_id }}</td>
            <td>{{ $line->contract->poznamka }}</td>
        </tr>
    @endif

    @if(isset($line->write_on_oil))
        <tr style="background-color: #0c3a0e; color: #ffffff;">
            <td>{{ $line->customer->name }}</td>
            <td>
                {{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}
            </td>
            <td>
                {{ $line->contract->mesto }}, {{ $line->contract->psc }}, {{ $line->contract->adresa }}
            </td>
            <td>20 01 25</td>
            <td>{{ $line->write_on_oil->write_oil_weight }}</td>
            <td>
                0
            </td>
            <td>
                {!!  $line->write_on_oil->toilet_paper ?? 0  !!}
            </td>
            <td>
                {!! $line->write_on_oil->hygienic_handkerchiefs ?? 0!!}
            </td>
            <td>
                {!! $line->write_on_oil->kitchen_towels ?? 0 !!}
            </td>
            <td>
                {!! $line->write_on_oil->oil_for_oil ?? 0 !!}
            </td>
            <td></td>
            <td>0</td>
            <td></td>
            <td></td>
            <td style="background-color: #0c3a0e; color: #ffffff;">
                Olej bezodplatne
            </td>
            <td>{{ $line->customer->external_id }}</td>
            <td>{{ $line->contract->external_id }}</td>
            <td>{{ $line->contract->poznamka }}</td>
        </tr>
    @endif

    @if(isset($line->write_off_fat))
        <tr style="background-color: #f6c23e; color: #ffffff;">
            <td>{{ $line->customer->name }}</td>
            <td>
                {{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}
            </td>
            <td>
                {{ $line->contract->mesto }}, {{ $line->contract->psc }}, {{ $line->contract->adresa }}
            </td>
            <td>20 01 25</td>
            <td>{{ $line->write_off_fat->write_off_fat_weight }}</td>
            <td>{{ $line->write_off_fat->write_off_fat_total_price }}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>0</td>
            <td></td>
            <td></td>
            <td style="background-color: #1cc88a; color: #ffffff;">
                Tuk odpis
            </td>
            <td>{{ $line->customer->external_id }}</td>
            <td>{{ $line->contract->external_id }}</td>
            <td>{{ $line->contract->poznamka }}</td>
        </tr>
    @endif

    @if(isset($line->write_on_fat))
        <tr style="background-color: #6c510c; color: #ffffff;">
            <td>{{ $line->customer->name }}</td>
            <td>
                {{ empty($line->contract->branch_name) ? $line->customer->name : $line->contract->branch_name }}
            </td>
            <td>
                {{ $line->contract->mesto }}, {{ $line->contract->psc }}, {{ $line->contract->adresa }}
            </td>
            <td>20 01 25</td>
            <td>{{ $line->write_on_fat->write_fat_weight }}</td>
            <td>
                0
            </td>
            <td>
                {!!  $line->write_on_fat->fat_toilet_paper ?? 0  !!}
            </td>
            <td>
                {!! $line->write_on_fat->fat_hygienic_handkerchiefs ?? 0!!}
            </td>
            <td>
                {!! $line->write_on_fat->fat_kitchen_towels ?? 0 !!}
            </td>
            <td>
                {!! $line->write_on_fat->fat_for_oil ?? 0 !!}
            </td>
            <td></td>
            <td>0</td>
            <td></td>
            <td></td>
            <td style="background-color: #0c3a0e; color: #ffffff;">
                Tuk bezodplatne
            </td>
            <td>{{ $line->customer->external_id }}</td>
            <td>{{ $line->contract->external_id }}</td>
            <td>{{ $line->contract->poznamka }}</td>
        </tr>
    @endif
@endforeach
