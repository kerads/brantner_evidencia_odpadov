<div class="form-group row">
    <label for="branch_name" class="col-md-4 col-form-label text-md-right">Názov prevádzky</label>

    <div class="col-md-8">
        <input
            id="branch_name"
            type="text"
            class="form-control @error('branch_name') is-invalid @enderror"
            name="typprevadzky"
            value="{{ old('branch_name', isset($user->contract) ? $user->contract->branch_name : '') }}"
            required>

        @error('branch_name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="adresa" class="col-md-4 col-form-label text-md-right">Ulica</label>

    <div class="col-md-8">
        <input id="adresa"
               type="text"
               class="form-control @error('adresa') is-invalid @enderror"
               name="adresa"
               value="{{ old('adresa', isset($user->contract) ? $user->contract->adresa : '') }}"
               required>

        @error('adresa')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="psc" class="col-md-4 col-form-label text-md-right">PSČ</label>

    <div class="col-md-8">
        <input id="psc"
               type="text"
               class="form-control @error('psc') is-invalid @enderror"
               name="psc"
               value="{{ old('psc', isset($user->contract) ? $user->contract->psc : '') }}"
               required>

        @error('psc')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="mesto" class="col-md-4 col-form-label text-md-right">Mesto</label>

    <div class="col-md-8">
        <input id="mesto"
               type="text"
               class="form-control @error('mesto') is-invalid @enderror"
               name="mesto"
               value="{{ old('mesto', isset($user->contract) ? $user->contract->mesto : '') }}"
               required>

        @error('mesto')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="validity_recurrency_until_id" class="col-md-4 col-form-label text-md-right">
        Platnosť
    </label>

    <div class="col-md-8">
        <select name="validity_recurrency_until_id"
                class="form-control @error('validity_recurrency_until_id') alert alert-danger @enderror">
            @error('validity_recurrency_until_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <option value="-1"></option>
            @foreach($recurrencyList as $vru)
                <option value="{{ $vru->id }}"
                    {{ old('validity_recurrency_until_id', isset($user->contract) ? $user->contract->validity_recurrency_until_id : '')  == $vru->id  ? 'selected' : '' }}>
                    {{ __("orders." . $vru->name) }}
                </option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="start_date" class="col-md-4 col-form-label text-md-right">
        Začiatok zmluvy
    </label>
    <div class="col-md-8">
        <input
            type="date"
            class="form-control @error('start_date') alert alert-danger @enderror"
            name="start_date"
            value="{{ old('start_date', !isset($user->contract) ? '' : $user->contract->start_date)}}"
        >
    </div>
</div>

<div class="form-group row" id="recurrency_until_date">
    <label for="recurrency_until_date" class="col-md-4 col-form-label text-md-right">
        Opakovať do dátumu
    </label>
    <div class="col-md-8">
        <input
            type="date"
            class="form-control @error('recurrency_until_date') alert alert-danger @enderror"
            name="recurrency_until_date"
            value="{{ old('recurrency_until_date', !isset($user->contract) ? '' : $user->contract->recurrency_until_date)}}"
        >
    </div>
</div>

<div class="form-group row" id="recurrency_num_of_repetitions">
    <label for="recurrency_num_of_repetitions" class="col-md-4 col-form-label text-md-right">
        Celkový počet opakovaní
    </label>
    <div class="col-md-8">
        <input
            type="number"
            class="form-control recurrency @error('recurrency_num_of_repetitions') alert alert-danger @enderror"
            name="recurrency_num_of_repetitions"
            value="{{ old('recurrency_num_of_repetitions', !isset($user->contract) ? '' : $user->contract->recurrency_num_of_repetitions)}}"
        >
    </div>
</div>

<div class="form-group row" id="recurrency_circle">
    <label for="recurrency_circle" class="col-md-4 col-form-label text-md-right">
        Cyklus opakovania
    </label>
    <div class="col-md-8">
        <select name="recurrency_circle"
                class="form-control recurrency @error('recurrency_circle') alert alert-danger @enderror">
            <option value=""></option>
            <option value="week"
                {{ old('recurrency_circle', isset($user->contract) ? $user->contract->recurrency_circle : '')  == 'week'  ? 'selected' : '' }}>
                {{ __('orders.week' ) }}
            </option>
            <option value="month"
                {{ old('recurrency_circle', isset($user->contract->recurrency_circle) ? $user->contract->recurrency_circle : '')  == 'month'  ? 'selected' : '' }}>
                {{ __('orders.month' ) }}
            </option>
        </select>
    </div>
</div>

<div class="form-group row" id="recurrency_month_days">
    <label for="recurrency_month_days" class="col-xl-12 col-lg-12 col-md-12">
        Dni opakovania v mesiaci
    </label>
    <div class="d-flex flex-column col-xl-12 col-lg-12">
        @for ($i = 1; $i < 32; $i++)
            @if(($i % 7) == 1)
                <div class="d-flex">
            @endif
            <label class="checkbox-custom-label w-3em text-right" for="recurrency_month_days[{{ $i }}]">
                {{ $i }}
                <input
                    type="checkbox"
                    class="checkbox-custom  @error('recurrency_month_days') alert alert-danger @enderror"
                    name="recurrency_month_days[]"
                    id="recurrency_month_days[{{ $i }}]"
                    value="{{ $i }}"
                    {{ array_key_exists($i, old('recurrency_month_days', !isset($user->contract->recurrency_month_days) ? [''] : $user->contract->recurrency_month_days)) ? 'checked' : '' }}
                >
            </label>
            @if(($i % 7) == 0 || $i == 31)
                </div>
            @endif
        @endfor
    </div>
</div>

<div class="form-group row" id="recurrency_week_days">
    <label for="recurrency_week_days" class="col-xl-12 col-lg-12 col-md-12">
        Dni opakovania v týždni
    </label>
    <div class="d-flex flex-column col-xl-12 col-lg-12 col-md-12">
        @for ($i = 0; $i < 7; $i++) {{-- 0 is Monday --}}
        <label class="checkbox-custom-label" for="recurrency_week_days[{{ $i }}]">
            <input
                type="checkbox"
                class="checkbox-custom  @error('recurrency_week_days') alert alert-danger @enderror"
                name="recurrency_week_days[]"
                id="recurrency_week_days[{{ $i }}]"
                value="{{ $i }}"
                {{ array_key_exists($i, old('recurrency_week_days', !isset($user->contract->recurrency_week_days) ? [''] : $user->contract->recurrency_week_days)) ? 'checked' : '' }}
            >
            {{ __('orders.' . jddayofweek($i,1)) }}
        </label>
        @endfor
    </div>
</div>

<div class="form-group row">
    <label for="num_barels" class="col-md-4 col-form-label text-md-right">
        Počet sudov KS
    </label>
    <div class="col-md-8">
        <input
            type="number"
            step="1"
            min="1"
            class="form-control recurrency @error('num_barels') alert alert-danger @enderror"
            name="num_barels"
            value="{{ old('num_barels', !isset($user->contract) ? '' : $user->contract->num_barels)}}"
        >
    </div>
</div>

<div class="form-group row">
    <label for="validity_recurrency_until_id" class="col-md-4 col-form-label text-md-right">
        Veľkosť sudov
    </label>

    <div class="col-md-8">
        <select name="barel_size"
                class="form-control @error('barel_size') alert alert-danger @enderror">
            @error('barel_size')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
            <option value="-1"></option>
            <option value="30"
                {{ old('barel_size', isset($user->contract) ? $user->contract->barel_size : '')  == 30  ? 'selected' : '' }}>
                30 L
            </option>
            <option value="60"
                {{ old('barel_size', isset($user->contract) ? $user->contract->barel_size : '')  == 60  ? 'selected' : '' }}>
                60 L
            </option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">
        Cena vývoz €
    </label>
    <div class="col-md-8">
        <input
            type="number"
            step="0.01"
            class="form-control recurrency @error('price') alert alert-danger @enderror"
            name="price"
            value="{{ old('price', !isset($user->contract) ? '' : $user->contract->price)}}"
        >
    </div>
</div>

<div class="form-group row">
    <label for="price_desinfection" class="col-md-4 col-form-label text-md-right">
        Cena dezinfekcia €
    </label>
    <div class="col-md-8">
        <input
            type="number"
            step="0.01"
            class="form-control recurrency @error('price_desinfection') alert alert-danger @enderror"
            name="price_oil"
            value="{{ old('price_desinfection', !isset($user->contract) ? '' : $user->contract->price_desinfection)}}"
        >
    </div>
</div>

<div class="form-group row">
    <label for="suncharge_beyond" class="col-md-4 col-form-label text-md-right">
        Príplatokt nad rámec €
    </label>
    <div class="col-md-8">
        <input
            type="number"
            step="0.01"
            class="form-control recurrency @error('suncharge_beyond') alert alert-danger @enderror"
            name="suncharge_beyond"
            value="{{ old('suncharge_beyond', !isset($user->contract) ? '' : $user->contract->suncharge_beyond)}}"
        >
    </div>
</div>

<div class="form-group row">
    <label for="contract" class="col-md-4 col-form-label text-md-right">Zmluva</label>
    <div class="col-md-8">
        <div class="custom-file">
            <input
                id="contract"
                type="file"
                name="contract"
                class="custom-file-input @error('contract') is-invalid @enderror">
            <label class="custom-file-label" for="contract">{{ old('contract', isset($user->contract) ? $user->contract->contract : 'Vyberte zmluvu...') }}</label>
            @error('contract')
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
</div>

<div class="form-group row">
    <label for="poznamka" class="col-xl-12 col-lg-12 col-md-12">Poznámka</label>
    <div class="col-12">
        <textarea
            id="poznamka"
            class="form-control @error('-') is-invalid @enderror"
            name="poznamka"
            rows="4">{{ old('poznamka', isset($user->contract) ? $user->contract->poznamka : '') }}</textarea>
        @error('poznamka')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

@section('script')
    <script>
        // Recurrency
        $('select[name="validity_recurrency_until_id"]').on('change', function(){
            recurrencyUntil();
        });

        $('select[name="recurrency_circle"]').on('change', function(){
            recurrencyCircle();
        });

        recurrencyUntil();

        function recurrencyUntil(){
            var recurrencyUntil = $( 'select[name="validity_recurrency_until_id"]').val();

            if(recurrencyUntil == 2){ // do datumu
                $('#recurrency_until_date').show();
                $('#recurrency_num_of_repetitions').hide();
                $('#recurrency_circle').show();
            } else if(recurrencyUntil == 3){  // pocet opakovani
                $('#recurrency_until_date').hide();
                $('#recurrency_num_of_repetitions').show();
                $('#recurrency_circle').show();
            } else if(recurrencyUntil == 1){ // do zrusenia
                $('#recurrency_until_date').hide();
                $('#recurrency_num_of_repetitions').hide();
                $('#recurrency_circle').show();
            // } else if(selected == 4){ // jednorazova
            //     $('#recurrency_until_date').hide();
            //     $('#recurrency_num_of_repetitions').hide();
            //     $('#recurrency_circle').hide();
            } else{
                $('#recurrency_until_date').hide();
                $('#recurrency_num_of_repetitions').hide();
                $('#recurrency_circle').hide();
            }

            recurrencyCircle();
        }

        function recurrencyCircle(){ // dni v mesiaci / tyzdni
            var recurrencyUntil = $( 'select[name="validity_recurrency_until_id"]').val();
            var recurrencyCircle = $( 'select[name="recurrency_circle"]').val();

             if(recurrencyCircle == 'week' && recurrencyUntil != 4){
                 $('#recurrency_month_days').hide();
                 $('#recurrency_week_days').show();
             } else if(recurrencyCircle == 'month' && recurrencyUntil != 4) {
                 $('#recurrency_month_days').show();
                 $('#recurrency_week_days').hide();
             } else {
                 $('#recurrency_month_days').hide();
                 $('#recurrency_week_days').hide();
             }
        }
    </script>
@endsection
