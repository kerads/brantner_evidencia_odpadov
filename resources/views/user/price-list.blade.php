@extends('layouts.master')

@section('title')
    Cenník
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Cenník</h1>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Cenník</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Popis</th>
                        <th>KS</th>
                        <th>Zmluva</th>
                        <th>Cena  €</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Popis</th>
                        <th>KS</th>
                        <th>Zmluva</th>
                        <th>Cena €</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($priceList as $price)
                        <tr id="user-id-{{ $price->id }}">
                            <td>{{ $price->name }}</td>
                            <td>
                                <input
                                    type="number"
                                    min="1"
                                    step="1"
                                    class="form-control recurrency @error('ks') alert alert-danger @enderror"
                                    name="ks"
                                    value="{{ old('ks', $price->ks)}}"
                                    data-id="{{ $price->id }}"
                                >
                            </td>
                            <td class="text-center">
                                <label class="switch">
                                    <input
                                        type="checkbox"
                                        name="contract"
                                        value="1"
                                        id="contract"
                                        {{ $price->contract == 1 ? "checked" : "" }}
                                        data-id="{{ $price->id }}"
                                    >
                                    <span class="slider round"></span>
                                </label>
                            </td>
                            <td>
                                <input
                                    type="number"
                                    min="0.00"
                                    step="0.01"
                                    class="form-control recurrency @error('price') alert alert-danger @enderror"
                                    name="price"
                                    value="{{ old('price', $price->price)}}"
                                    data-id="{{ $price->id }}"
                                >
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });


        $('input[name="ks"]').on('change', function(e){
            let id = $(this).data('id');
            let name = $(this).attr('name');
            let value = $(this).val();
            save(id, name, value);
        })

        $('input[name="contract"]').on('change', function(e){
            let id = $(this).data('id');
            let name = $(this).attr('name');
            let checked = $(this).is(':checked');
            let value = checked ? 1 : 0 ;
            save(id, name, value);
        })

        $('input[name="price"]').on('change', function(e){
            let id = $(this).data('id');
            let name = $(this).attr('name');
            let value = $(this).val();
            save(id, name, value);
        })

        $('input[name="ks"]').on('change', function(e){
            let id = $(this).data('id');
            let name = $(this).attr('name');
            let value = $(this).val();
            save(id, name, value);
        })

        function save(id, name, value){
            $.ajax({
                type: "POST",
                url: "/edit-price",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id,
                    name: name,
                    value: value,
                },
            }).done(function( data ) {
                console.log('data')
                console.log(data)
            });
        }
    </script>
@endsection
