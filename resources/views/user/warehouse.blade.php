@extends('layouts.master')

@section('title')
    Sklad hygieny
@endsection

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center py-3">
            <h6 class="m-0 font-weight-bold text-primary">Sklad hygieny</h6>
            <a href="{{ url('add-stock-movement') }}">
                <button class="btn btn-primary">
                    <i class="fa fa-user-plus fa-sm"> Doplniť sklad</i>
                </button>
            </a>
        </div>
        <div class="card-body">
            <div class="flex-row border-bottom mb-4">
                <div>
                    <Strong>Aktuálny stav</Strong>
                </div>
                <div class="{{ ($lastStorageRecord->toilet_paper) < 0 ? 'text-danger' : 'text-success' }}">
                    Toaletý papier: {{ $lastStorageRecord->toilet_paper}}
                </div>
                <div class="{{ ($lastStorageRecord->hygienic_handkerchiefs) < 0 ? 'text-danger' : 'text-success' }}">
                    Hygienicke vreckovky: {{ $lastStorageRecord->hygienic_handkerchiefs }}
                </div>
                <div class="{{ ($lastStorageRecord->kitchen_towels) < 0 ? 'text-danger' : 'text-success' }}">
                    Kuchynské utierky: {{ $lastStorageRecord->kitchen_towels }}
                </div>
                <div class="{{ ($lastStorageRecord->oil) < 0 ? 'text-danger' : 'text-success' }}">
                    Olej: {{ $lastStorageRecord->oil }}
                </div>
            </div>
            <div class="border-bottom mb-4">
                <div>
                    <strong>Posledné záznamy</strong>
                </div>
                <div>
                    <table class="table border">
                        <tr>
                            <th class="text-center">Upraviť</th>
                            <th class="text-center">Ku dňu</th>
                            <th class="text-center">Toaletný papier</th>
                            <th class="text-center">Hygienické vreckovky</th>
                            <th class="text-center">Kuchynské utierky</th>
                            <th class="text-center">Olej</th>
                            <th class="text-center">Odstrániť</th>
                        </tr>

                        @foreach($lastFiveStorageRecord as $storageRecord)
                            <tr class="{{ 'record-id-' . $storageRecord->id }}">
                                <td class="text-center">
                                    <a href="{{ url('edit-stock-movement/' . $storageRecord->id) }}">
                                        <i class="fa fa-edit"\>{{ $storageRecord->id }}</i>
                                    </a>
                                </td>
                                <td class="text-center">{{ date('d.m.Y', strtotime($storageRecord->change_date)) }}</td>
                                <td class="text-center">{{ $storageRecord->toilet_paper }}</td>
                                <td class="text-center">{{ $storageRecord->hygienic_handkerchiefs }}</td>
                                <td class="text-center">{{ $storageRecord->kitchen_towels }}</td>
                                <td class="text-center">{{ $storageRecord->oil }}</td>
                                <td class="text-center">
                                    <a href="{{ url('delete-stock-movement/' . $storageRecord->id) }}" class="delete" data-id="{{ $storageRecord->id }}">
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div>
                <div class="flex-row justify-content-between">
                    <strong>Koncový stav za mesiace</strong>
                    <a href="{{ url('add-warehouse-month-record') }}"
                       id="month-record"
                       class="btn btn-warning"
                       data-date="{{ date('Y-m-d', strtotime('+1 month',strtotime($lastFiveStorageMonths->first()->summary_date))) }}"
                       data-f-date="{{ date('m.Y', strtotime($lastFiveStorageMonths->first()->summary_date)) }}"
                    >
                        <i class="fab fa-deezer">
                            Vytvoriť mesačný výpis {{ date('m.Y', strtotime($lastFiveStorageMonths->first()->summary_date)) }}
                        </i>
                    </a>
                </div>
                <div>
                    <table class="table border">
                        <tr>
                            <th class="text-center">Za mesiac</th>
                            <th class="text-center">Ku dňu</th>
                            <th class="text-center">Toaletný papier</th>
                            <th class="text-center">Hygienické vreckovky</th>
                            <th class="text-center">Kuchynské utierky</th>
                            <th class="text-center">Olej</th>
                        </tr>

                        @foreach($lastFiveStorageMonths as $storageRecord)
                            <tr>
                                <td class="text-center">{{ date('m.Y', strtotime($storageRecord->summary_date . "-1 months")) }}</td>
                                <td class="text-center">{{ date('d.m.Y', strtotime($storageRecord->summary_date)) }}</td>
                                <td class="text-center">{{ $storageRecord->toilet_paper }}</td>
                                <td class="text-center">{{ $storageRecord->hygienic_handkerchiefs }}</td>
                                <td class="text-center">{{ $storageRecord->kitchen_towels }}</td>
                                <td class="text-center">{{ $storageRecord->oil }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        // delete
        $(".delete").click( function(e){
            e.preventDefault();
            let id = $(this).data('id');
            if (confirm("Naozaj chcete odstrániť záznam č. " + id + " ?")) {
                window.location = $(this).attr('href');
            }
        });

        $("#month-record").click( function(e){
            e.preventDefault();

            let currentDate = getDate();
            let recordDate = $(this).data('date');
            let formatedDate = $(this).data('f-date');

            if(currentDate < recordDate) {
                alert("Ešte nie je možné ukončiť mesiac!")
            } else {
                if(confirm("Naozaj chcete vytvoriť nový mesačný výpis za mesiac " + formatedDate + "? Už nebude možné pridávať staršie záznamy.")){
                    window.location = $(this).attr('href');
                }
            }
        });

        function getDate() {
            let d = new Date();
            let day = d.getDate();
            let month = d.getMonth() + 1;
            let year = d.getFullYear();
            if (day < 10) {
                day = "0" + day;
            }
            if (month < 10) {
                month = "0" + month;
            }
            let date =  year + "-" + month +  "-" + day;

            return date;
        }
    </script>
@endsection
