@extends('layouts.master')

@section('title')
    Zoznam klientov
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zoznam klientov</h1>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam klientov</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Prevádzka</th>
                        <th>Prehľad</th>
                        <th>Detail</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Meno</th>
                        <th>Prehľad</th>
                        <th>Detail</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($customerList as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ isset($user->userInfo->typprevadzky) ? $user->userInfo->typprevadzky : '' }}</td>
                            <td>
                                @if(isset($user->userInfo->zmluva) && $user->userInfo->zmluva != null)
                                    <a href="{{ url('storage/app/public/zmluvy/' . $user->userInfo->zmluva) }}">
                                        <i class="fas fa-file-pdf"></i>
                                    </a>
                                @endif
                            </td>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
