@extends('layouts.master')

@section('title')
    Zoznam klientov
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zoznam klientov</h1>
        <a href="{{ url('new-customer/4') }}">
            <button class="btn btn-primary">
                <i class="fa fa-user-plus"> Pridať klienta</i>
            </button>
        </a>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam klientov</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Meno</th>
                        <th>Email</th>
                        <th>Zmluvy</th>
                        <th>Upraviť</th>
                        <th>Zmazať</th>
                        <th>Ročné ohlásenia</th>
                        <th>Evidenčné listy</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Meno</th>
                        <th>Email</th>
                        <th>Zmluvy</th>
                        <th>Upraviť</th>
                        <th>Zmazať</th>
                        <th>Ročné ohlásenia</th>
                        <th>Evidenčné listy</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($userList as $user)
                        <tr id="user-id-{{ $user->id }}">
                            <td class="{{ isset($user->lastContract) && (isset($user->lastContract->closed_at) && ($user->lastContract->closed_at < date('Y-m-d')) ) ? 'text-danger' : '' }}">{{ $user->name }}</td>
                            <td class="{{ isset($user->lastContract) && (isset($user->lastContract->closed_at) && ($user->lastContract->closed_at < date('Y-m-d')) ) ? 'text-danger' : '' }}">{{ $user->email }}</td>
                            <td class="text-center {{ isset($user->lastContract) && (isset($user->lastContract->closed_at) && ($user->lastContract->closed_at < date('Y-m-d')) ) ? 'text-danger' : '' }}">
                                <a href="{{ url('customer/' . $user->id . '/contract-list') }}">
                                    <i class="fas fa-file-pdf"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="{{ url('edit-customer/' . $user->id) }}">
                                    <i class="fas fa-user-edit"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#" class="delete" data-id="{{ $user->id }}" data-name="{{ $user->name }}">
                                    <i class="fas fa-trash-alt text-danger"></i>
                                </a>
                            </td>
                            <td>
                                @if(!empty($user->firstContract()))
                                    <form action="{{ url('get-annual-report') }}" method="GET" class="d-none d-sm-inline-block form-inline navbar-search w-130">
                                        <input type="hidden" name="customer_id" value="{{ $user->id }}">

                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control bg-light border-0 small col-12"
                                                   name="year"
                                                   min="{{ date('Y', strtotime($user->firstContract()->start_date)) }}"
                                                   max="{{ date('Y') }}"
                                                   value="{{ old('year', date('Y')) }}"
                                                   aria-label="Search" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="success">
                                                    <i class="fas fa-download fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </td>
                            <td>
                                @if(!empty($user->firstContract()))
                                    <form action="{{ url('export-records-sheets') }}" method="GET" class="d-none d-sm-inline-block form-inline navbar-search w-130">
                                        <input type="hidden" name="user_id" value="{{ $user->id }}">

                                        <div class="input-group">
                                            <input type="number"
                                                   class="form-control bg-light border-0 small col-12"
                                                   name="year"
                                                   min="{{ date('Y', strtotime($user->firstContract()->start_date)) }}"
                                                   max="{{ date('Y') }}"
                                                   value="{{ old('year', date('Y')) }}"
                                                   aria-label="Search" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="success">
                                                    <i class="fas fa-download fa-sm"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });

        // delete
        $(".delete").click( function(e){
            e.preventDefault();
            var id = $(this).data('id');
            var name = $(this).data('name');
            if (confirm("Naozaj chcete odstrániť uživateľa " + id + " - " + name + " ?")) {
                $.ajax({
                    type: "POST",
                    url: "/delete-customer",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                }).done(function( data ) {
                    removeRow(data);
                });
            }

            function removeRow(data) {
                $("#user-id-" + data).remove();
            }
        });
    </script>
@endsection
