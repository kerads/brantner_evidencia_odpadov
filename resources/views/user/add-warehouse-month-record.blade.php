@extends('layouts.master')

@section('title')
    Doplniť sklad
@endsection

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Vytvorenie záznamu</h6>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ url('save-stock-movement') }}" class="col-12">
                @csrf
                @if(isset($record))
                    <inpur type="hidden" name="id" value="{{ $record->id }}"></inpur>
                @endif
                <div class="row col-12">
                    <div class="form-group row col-12">
                        <label for="extraction_date" class="col-md-4 col-form-label text-md-right">
                            Dátum doplnenia skladu
                        </label>
                        <div class="col-md-8">
                            <input
                                type="date"
                                name="change_date"
                                value="{{ old('change_date', date('Y-m-d')) }}"
                                class="form-control recurrency @error('change_date') alert alert-danger @enderror "
                            >
                        </div>
                    </div>
                    <div class="form-group row col-12">
                        <label for="toilet_paper" class="col-md-4 col-form-label text-md-right">
                            Toaletný papier
                        </label>
                        <div class="col-md-8">
                            <input
                                id="toilet_paper"
                                name="toilet_paper"
                                type="number"
                                value="{{ old('toilet_paper', isset($record) ? $record->toilet_paper : 0) }}"
                                step="1"
                                class="form-control recurrency @error('toilet_paper') alert alert-danger @enderror"
                            >
                        </div>
                    </div>
                    <div class="form-group row col-12">
                        <label for="hygienic_handkerchiefs" class="col-md-4 col-form-label text-md-right">
                            Hygienické vreckovky
                        </label>
                        <div class="col-md-8">
                            <input
                                id="hygienic_handkerchiefs"
                                name="hygienic_handkerchiefs"
                                type="number"
                                value="{{ old('hygienic_handkerchiefs', isset($record) ? $record->hygienic_handkerchiefs : 0) }}"
                                step="1"
                                class="form-control recurrency @error('hygienic_handkerchiefs') alert alert-danger @enderror"
                            >
                        </div>
                    </div>
                    <div class="form-group row col-12">
                        <label for="kitchen_towels" class="col-md-4 col-form-label text-md-right">
                            Kuchynské utierky
                        </label>
                        <div class="col-md-8">
                            <input
                                id="kitchen_towels"
                                name="kitchen_towels"
                                type="number"
                                value="{{ old('kitchen_towels', isset($record) ? $record->kitchen_towels : 0) }}"
                                step="1"
                                class="form-control recurrency @error('kitchen_towels') alert alert-danger @enderror"
                            >
                        </div>
                    </div>
                </div>
                <div class="d-flex row justify-items-between mt-2 mb-2">
                    <div class="col-md-6">
                        <a href="{{ url(session()->has('backUrl') ? session()->get('backUrl') : 'warehouse') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                    </div>
                    <div class="col-md-6 text-right">
                        <button class="btn btn-primary" type="submit">
                            @if(isset($record))
                                <i class="fa fa-save"/> Upraviť</i>
                            @else
                                <i class="fa fa-plus"/> Vytvoriť</i>
                            @endif
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('script')
@endsection
