@extends('layouts.master')

@section('title')
    Skladové záznami
@endsection

@section('content')
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center py-3">
            <h6 class="m-0 font-weight-bold text-primary">Skladové záznami</h6>
            <a href="{{ url('add-stock-movement') }}">
                <button class="btn btn-primary">
                    <i class="fa fa-user-plus fa-sm"> Doplniť sklad</i>
                </button>
            </a>
        </div>
        <div class="card-body">
            <div class="border-bottom mb-4">
                <div>
                    <strong>Posledné záznamy</strong>
                </div>
                <div>
                    <table class="table border">
                        <tr>
                            <th class="text-center">Upraviť</th>
                            <th class="text-center">Ku dňu</th>
                            <th class="text-center">Toaletný papier</th>
                            <th class="text-center">Hygienické vreckovky</th>
                            <th class="text-center">Kuchynské utierky</th>
                            <th class="text-center">Odstrániť</th>
                        </tr>

                        @foreach($storageRecords as $storageRecord)
                            <tr class="{{ 'record-id-' . $storageRecord->id }}">
                                <td class="text-center">
                                    <a href="{{ url('edit-stock-movement/' . $storageRecord->id) }}">
                                        <i class="fa fa-edit"\>{{ $storageRecord->id }}</i>
                                    </a>
                                </td>
                                <td class="text-center">{{ date('Y-m-d', strtotime($storageRecord->change_date)) }}</td>
                                <td class="text-center">{{ $storageRecord->toilet_paper }}</td>
                                <td class="text-center">{{ $storageRecord->hygienic_handkerchiefs }}</td>
                                <td class="text-center">{{ $storageRecord->kitchen_towels }}</td>
                                <td class="text-center">
                                    <a href="{{ url('delete-stock-movement/' . $storageRecord->id) }}" class="delete" data-id="{{ $storageRecord->id }}">
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        // delete
        $(".delete").click( function(e){
            e.preventDefault();
            let id = $(this).data('id');
            if (confirm("Naozaj chcete odstrániť záznam č. " + id + " ?")) {
                window.location = $(this).attr('href');
            }
        });
    </script>
@endsection
