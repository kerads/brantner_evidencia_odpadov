@extends('layouts.master')

@section('title')
    {{ isset($contract->id) ? 'Upraviť zmluvu' : 'Nová zmluva' }}
@endsection

@section('content')
    <div class="row col-12">
        <form method="POST" action="{{ url('save-contract') }}" class="col-12" enctype="multipart/form-data">
            <input type="hidden" name="user_id" value="{{ $user->id }}">
            @csrf
            @if(isset($contract->id))
                <input type="hidden" name="id" value="{{ $contract->id }}">
            @endif
            <div class="row col-12">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            {{ isset($contract->id) ? 'Upraviť zmluvu' : 'Nová zmluva' }} : {{ $user->name }}, IČO: {{ $user->ico }}
                        </div>

                        <div class="card-body">

                            <div class="form-group row">
                                <label for="branch_name" class="col-md-4 col-form-label text-md-right">Názov prevádzky</label>

                                <div class="col-md-8">
                                    <input
                                        id="branch_name"
                                        type="text"
                                        class="form-control @error('branch_name') is-invalid @enderror"
                                        name="branch_name"
                                        value="{{ old('branch_name', isset($contract) ? $contract->branch_name : '') }}"
                                        required>

                                    @error('branch_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="external_id" class="col-md-4 col-form-label text-md-right">Externé číslo</label>

                                <div class="col-md-8">
                                    <input id="external_id"
                                           type="number"
                                           class="form-control @error('external_id') is-invalid @enderror"
                                           name="external_id"
                                           value="{{ old('external_id', isset($contract->external_id) ? $contract->external_id : '') }}">

                                    @error('external_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="adresa" class="col-md-4 col-form-label text-md-right">Ulica</label>

                                <div class="col-md-8">
                                    <input id="adresa"
                                           type="text"
                                           class="form-control @error('adresa') is-invalid @enderror"
                                           name="adresa"
                                           value="{{ old('adresa', isset($contract) ? $contract->adresa : '') }}"
                                           required>

                                    @error('adresa')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="psc" class="col-md-4 col-form-label text-md-right">PSČ</label>

                                <div class="col-md-8">
                                    <input id="psc"
                                           type="text"
                                           class="form-control @error('psc') is-invalid @enderror"
                                           name="psc"
                                           value="{{ old('psc', isset($contract) ? $contract->psc : '') }}"
                                           required>

                                    @error('psc')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="mesto" class="col-md-4 col-form-label text-md-right">Mesto</label>

                                <div class="col-md-8">
                                    <input id="mesto"
                                           type="text"
                                           class="form-control @error('mesto') is-invalid @enderror"
                                           name="mesto"
                                           value="{{ old('mesto', isset($contract) ? $contract->mesto : '') }}"
                                           required>

                                    @error('mesto')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="validity_recurrency_until_id" class="col-md-4 col-form-label text-md-right">
                                    Platnosť
                                </label>

                                <div class="col-md-8">
                                    <select name="validity_recurrency_until_id"
                                            class="form-control @error('validity_recurrency_until_id') alert alert-danger @enderror">
                                        @error('validity_recurrency_until_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <option value="-1"></option>
                                        @foreach($recurrencyList as $vru)
                                            <option value="{{ $vru->id }}"
                                                {{ old('validity_recurrency_until_id', isset($contract) ? $contract->validity_recurrency_until_id : '')  == $vru->id  ? 'selected' : '' }}>
                                                {{ __("orders." . $vru->name) }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="start_date" class="col-md-4 col-form-label text-md-right">
                                    Začiatok zmluvy
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="date"
                                        class="form-control @error('start_date') alert alert-danger @enderror"
                                        name="start_date"
                                        value="{{ old('start_date', !isset($contract) ? '' : $contract->start_date)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row" id="recurrency_until_date">
                                <label for="recurrency_until_date" class="col-md-4 col-form-label text-md-right">
                                    Opakovať do dátumu
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="date"
                                        class="form-control @error('recurrency_until_date') alert alert-danger @enderror"
                                        name="recurrency_until_date"
                                        value="{{ old('recurrency_until_date', !isset($contract) ? '' : $contract->recurrency_until_date)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row" id="recurrency_num_of_repetitions">
                                <label for="recurrency_num_of_repetitions" class="col-md-4 col-form-label text-md-right">
                                    Celkový počet opakovaní
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        class="form-control recurrency @error('recurrency_num_of_repetitions') alert alert-danger @enderror"
                                        name="recurrency_num_of_repetitions"
                                        value="{{ old('recurrency_num_of_repetitions', !isset($contract) ? '' : $contract->recurrency_num_of_repetitions)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row" id="repeating_cycle_id">
                                <label for="repeating_cycle_id" class="col-md-4 col-form-label text-md-right">
                                    Cyklus opakovania
                                </label>
                                <div class="col-md-8">
                                    <select name="repeating_cycle_id"
                                            class="form-control recurrency @error('repeating_cycle_id') alert alert-danger @enderror">
                                        <option value="-1"></option>
                                        @foreach($repeatingCycleList as $cycle)
                                            <option value="{{ $cycle->id }}"
                                                {{ old('repeating_cycle_id', isset($contract) ? $contract->repeating_cycle_id : '')  == $cycle->id  ? 'selected' : '' }}>
                                                {{ $cycle->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="recurrency_week_days">
                                <label for="recurrency_week_days" class="col-xl-12 col-lg-12 col-md-12 text-right">
                                    Deň vývozu
                                </label>
                                <div class="d-flex flex-column align-items-end col-xl-12 col-lg-12 col-md-12">
                                    @for ($i = 1; $i < 8; $i++) {{-- 0 is Monday --}}
                                        <label class="checkbox-custom-label" for="recurrency_week_days[{{ $i }}]">
                                            <input
                                                type="checkbox"
                                                class="checkbox-custom  @error('recurrency_week_days') alert alert-danger @enderror"
                                                name="recurrency_week_days[]"
                                                id="recurrency_week_days[{{ $i }}]"
                                                value="{{ $i }}"
                                                @if(in_array($i, (old('recurrency_week_days', isset($contract->recurrency_week_days) ? $contract->stringToArray($contract->recurrency_week_days) : [])))) checked @endif
                                            >
                                            {{ __('orders.' . jddayofweek($i-1,1)) }}
                                        </label>
                                    @endfor
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="num_barels" class="col-md-4 col-form-label text-md-right">
                                    Počet sudov za týždeň <span id="min-max-barels">(min/priemer/max) </span>ks
                                </label>
                                <div class="col-md-8 flex-row">
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-4 recurrency @error('min_num_barels') alert alert-danger @enderror"
                                        id="min_num_barels"
                                        name="min_num_barels"
                                        value="{{ old('min_num_barels', !isset($contract) ? '' : $contract->min_num_barels)}}"
                                    >
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control recurrency @error('num_barels') alert alert-danger @enderror"
                                        id="num_barels"
                                        name="num_barels"
                                        value="{{ old('num_barels', !isset($contract) ? '' : $contract->num_barels)}}"
                                        required
                                    >
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-4 recurrency @error('max_num_barels') alert alert-danger @enderror"
                                        id="max_num_barels"
                                        name="max_num_barels"
                                        value="{{ old('max_num_barels', !isset($contract) ? '' : $contract->max_num_barels)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="stored_barels_30" class="col-md-4 col-form-label text-md-right">
                                    Počet sudov na sklade 30L
                                </label>
                                <div class="col-md-8 flex-row">
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-12 recurrency @error('stored_barels_30') alert alert-danger @enderror"
                                            id="stored_barels_30"
                                        name="stored_barels_30"
                                        value="{{ old('stored_barels_30', !isset($contract) ? '' : $contract->stored_barels_30)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="stored_barels_60" class="col-md-4 col-form-label text-md-right">
                                    Počet sudov na sklade 60L
                                </label>
                                <div class="col-md-8 flex-row">
                                    <input
                                        type="number"
                                        step="1"
                                        min="1"
                                        class="form-control col-md-12 recurrency @error('stored_barels_60') alert alert-danger @enderror"
                                        id="stored_barels_60"
                                        name="stored_barels_60"
                                        value="{{ old('stored_barels_60', !isset($contract) ? '' : $contract->stored_barels_60)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="validity_recurrency_until_id" class="col-md-4 col-form-label text-md-right">
                                    Veľkosť sudov
                                </label>

                                <div class="col-md-8">
                                    <select name="barel_size"
                                            class="form-control @error('barel_size') alert alert-danger @enderror">
                                        @error('barel_size')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <option value=""></option>
                                        <option value="30"
                                            {{ old('barel_size', isset($contract) ? $contract->barel_size : '')  == 30  ? 'selected' : '' }}>
                                            <span>30 L</span>
                                        </option>
                                        <option value="60"
                                            {{ old('barel_size', isset($contract) ? $contract->barel_size : '')  == 60  ? 'selected' : '' }}>
                                            <span>60 L</span>
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="price_type">
                                <label for="price_type" class="col-md-4 col-form-label text-md-right">
                                    Cena
                                </label>
                                <div class="col-md-8">
                                    <select
                                        id="price_type"
                                        name="price_type"
                                        class="form-control recurrency @error('price_type') alert alert-danger @enderror"
                                    >
                                        <option value="1"
                                            {{ old('price_type', isset($contract) ? $contract->price_type : 1)  == 1  ? 'selected' : '' }}>
                                            Cenníka
                                        </option>
                                        <option value="2"
                                            {{ old('price_type', isset($contract) ? $contract->price_type : '')  == 2  ? 'selected' : '' }}>
                                            Zmluvná cena
                                        </option>
                                        <option value="3"
                                            {{ old('price_type', isset($contract) ? $contract->price_type : '')  == 3  ? 'selected' : '' }}>
                                            Paušál
                                        </option>
                                        <option value="4"
                                            {{ old('price_type', isset($contract) ? $contract->price_type : '')  == 4  ? 'selected' : '' }}>
                                            Ohraničený paušál
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="price_id">
                                <label for="price_id" class="col-md-4 col-form-label text-md-right">
                                    Cena vývoz €
                                </label>

                                <div class="col-md-8">
                                    <select name="price_id"
                                            class="form-control @error('price_id') alert alert-danger @enderror">
                                        @error('price_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        <option value="-1"></option>
                                        @foreach($priceList as $price)
                                            <option value="{{ $price->id }}"
                                                {{ old('price_id', isset($contract) ? $contract->price_id : '')  == $price->id  ? 'selected' : '' }}>
                                                {{ $price->name }} - {{ $price->price }} €
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_price">
                                <label for="custom_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency @error('custom_price') alert alert-danger @enderror"
                                        name="custom_price"
                                        value="{{ old('custom_price', !isset($contract) ? '' : $contract->custom_price)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row" id="discount">
                                <label for="discount" class="col-md-4 col-form-label text-md-right">
                                    Zľava %
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        min="0"
                                        max="100"
                                        class="form-control recurrency @error('discount') alert alert-danger @enderror"
                                        name="discount"
                                        value="{{ old('discount', isset($contract) ? $contract->discount : '0')}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="custom_above_quantity_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena odpad nad zmluvné množstvo
                                </label>
                                <input type="hidden" name="custom_above_quantity_price" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input type="checkbox" name="custom_above_quantity_price" value="1" id="custom_above_quantity_price" {{ isset($contract) && $contract->custom_above_quantity_price == 1 ? 'checked' : ''}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="above_quantity_price">
                                <label for="above_quantity_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena za nádobu nad zmluvné množstvo €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency @error('above_quantity_price') alert alert-danger @enderror"
                                        name="above_quantity_price"
                                        value="{{ old('above_quantity_price', !isset($contract) ? '' : $contract->above_quantity_price)}}"
                                    >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="custom_desinfection" class="col-md-4 col-form-label text-md-right">
                                    Paušálna dezinfekcia
                                </label>
                                <input type="hidden" name="custom_desinfection" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input type="checkbox" name="custom_desinfection" value="1" id="custom_desinfection" {{ isset($contract) && $contract->custom_desinfection == 1 ? 'checked' : ''}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_desinfection_price">
                                <label for="custom_desinfection_price" class="col-md-4 col-form-label text-md-right">
                                    Paušlálna dezinfekcie cena
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency @error('custom_desinfection_price') alert alert-danger @enderror"
                                        name="custom_desinfection_price"
                                        value="{{ old('custom_desinfection_price', !isset($contract) ? '' : $contract->custom_desinfection_price)}}"
                                    >
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Pridať iný druh odpadu
                                </label>
                                <div class="col-md-8 text-center">
                                    <a href="{{ url('new-custom-waste/' . (isset($contract) ? $contract->id : '')) }}" id="new-custom-waste" data-is-contract="{{ isset($contract) ? 1 : 0 }}">
                                        <i class="fa fa-plus-circle text-success fa-2x"></i>
                                    </a>
                                </div>
                            </div>

                            @if(isset($contract))
                                @foreach($contract->customWasteType as $wasteType)
                                    <div id="custom-waste-types" class="form-group row">
                                        <div class="col-md-4"></div>
                                        <div class="col-md-1 text-center">
                                            <a href="{{ url('edit-custom-waste/' . $wasteType->id) }}">
                                                <i class="fa fa-pencil-alt text-primary"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-1">{{ $wasteType->code }}</div>
                                        <div class="col-md-6">{{ $wasteType->name }}</div>
                                    </div>
                                @endforeach
                            @endif

                            <hr>

                            <div class="form-group row">
                                <label for="oil_switcher" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena oleja
                                </label>
                                <input type="hidden" name="oil_switcher" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input type="checkbox" name="oil_switcher" value="1" id="oil_switcher" {{ isset($contract) && $contract->custom_oil_price != null ? 'checked' : ''}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_oil_price">
                                <label for="custom_oil_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena za olej €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency @error('custom_oil_price') alert alert-danger @enderror"
                                        name="custom_oil_price"
                                        value="{{ old('custom_oil_price', !isset($contract) ? '' : $contract->custom_oil_price)}}"
                                    >
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="fat_switcher" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena tuku
                                </label>
                                <input type="hidden" name="fat_switcher" value="0">
                                <div class="col-md-8 text-center">
                                    <label class="switch">
                                        <input type="checkbox" name="fat_switcher" value="1" id="fat_switcher" {{ isset($contract) && $contract->custom_fat_price != null ? 'checked' : ''}}>
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group row" id="custom_fat_price">
                                <label for="custom_fat_price" class="col-md-4 col-form-label text-md-right">
                                    Zmluvná cena za tuk €
                                </label>
                                <div class="col-md-8">
                                    <input
                                        type="number"
                                        step="0.01"
                                        class="form-control recurrency @error('custom_fat_price') alert alert-danger @enderror"
                                        name="custom_fat_price"
                                        value="{{ old('custom_fat_price', !isset($contract) ? '' : $contract->custom_fat_price)}}"
                                    >
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="contract" class="col-md-4 col-form-label text-md-right">Zmluva</label>
                                <div class="col-md-8">
                                    <div class="custom-file">
                                        <input
                                            id="contract"
                                            type="file"
                                            name="contract"
                                            class="custom-file-input @error('contract') is-invalid @enderror"
                                            lang="sk">
                                        <label class="custom-file-label" for="contract">{{ old('contract', isset($contract) ? $contract->contract : 'Vyberte zmluvu...') }}</label>
                                        @error('contract')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="company_stamp" class="col-md-4 col-form-label text-md-right">Pečiatka prevádzky</label>
                                <div class="col-md-8">
                                    <div class="custom-file">
                                        <input
                                            id="company_stamp"
                                            type="file"
                                            name="company_stamp"
                                            class="custom-file-input @error('company_stamp') is-invalid @enderror"
                                            lang="sk">
                                        <label class="custom-file-label" for="company_stamp">{{ old('company_stamp', isset($contract) ? $contract->company_stamp : 'Pridajte pečiatku prevádzky...') }}</label>
                                        @error('company_stamp')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group row">
                                <label for="poznamka" class="col-xl-12 col-lg-12 col-md-12">Poznámka</label>
                                <div class="col-12">
                                    <textarea
                                        id="poznamka"
                                        class="form-control @error('-') is-invalid @enderror"
                                        name="poznamka"
                                        rows="4">{{ old('poznamka', isset($contract) ? $contract->poznamka : '') }}</textarea>
                                    @error('poznamka')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <hr>

                            <div class="d-flex row justify-items-between mt-2 mb-2">
                                <div class="col-md-4">
                                    <a href="{{ url('customer/' . $user->id . '/contract-list') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                                </div>
                                <div class="col-md-4 text-center">
                                    @if(isset($contract))
                                        <div class="form-group row">
                                            <label for="closed_at" class="col-md-4 col-form-label text-md-right">
                                                Dátum ukončenia
                                            </label>
                                            <div class="col-md-8">
                                                <input
                                                    type="date"
                                                    class="form-control @error('closed_at') alert alert-danger @enderror"
                                                    name="closed_at"
                                                    value="{{ old('closed_at', $contract->closed_at)}}"
                                                >
                                            </div>
                                        </div>
                                        <span class="btn btn-danger @if($contract->closed == 1 || $contract->validity_recurrency_until_id == 4 )disabled @endif"
                                              id="close_contract"
                                              data-id="{{ $contract->id }}"
                                              @if($contract->closed == 1 || $contract->validity_recurrency_until_id == 4 )
                                                  onclick="return false;"
                                              @endif
                                            ><i class="fa fa-times-circle"> Ukončiť zmluvu</i>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-md-4 text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-user-plus"> Uložiť zmluvu</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        // Recurrency until
        $('select[name="validity_recurrency_until_id"]').on('change', function(){
            recurrencyUntil();
        });

        recurrencyUntil();

        function recurrencyUntil(){
            var recurrencyUntil = $( 'select[name="validity_recurrency_until_id"]').val();

            if(recurrencyUntil == 2){ // do datumu
                $('#recurrency_until_date').show();
                $('#recurrency_num_of_repetitions').hide();
            } else if(recurrencyUntil == 3){  // pocet opakovani
                $('#recurrency_until_date').hide();
                $('#recurrency_num_of_repetitions').show();
            } else{
                $('#recurrency_until_date').hide();
                $('#recurrency_num_of_repetitions').hide();
            }
        }

        // Price switcher prepinanie medzi cenami
        $('select[name="price_type"]').on('change', function(){
            priceSwitcher();
        })

        priceSwitcher();

        function priceSwitcher(){
            // let customPrice = $('input[name="price_switcher"]:checked').length;
            let customPrice = $('select[name="price_type"] option:selected').val();

            if(customPrice == 1){
                $('#custom_price').fadeOut();
                $('#price_id').fadeIn();
                $('#discount').fadeIn();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            } else if(customPrice == 2 ){
                $('#custom_price').fadeIn();
                $('#price_id').fadeOut();
                $('#discount').fadeOut();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            } else if(customPrice == 3){
                $('#custom_price').fadeIn();
                $('#price_id').fadeOut();
                $('#discount').fadeOut();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            } else if(customPrice == 4){
                $('#custom_price').fadeIn();
                $('#price_id').fadeOut();
                $('#discount').fadeOut();
                $('#min_num_barels').fadeIn();
                $('#max_num_barels').fadeIn();
                $('#min-max-barels').fadeIn();
            } else {
                $('select[name="price_type"] option:selected').val(1)
                $('#custom_price').fadeOut();
                $('#price_id').fadeIn();
                $('#discount').fadeIn();
                $('#min_num_barels').fadeOut();
                $('#max_num_barels').fadeOut();
                $('#min-max-barels').fadeOut();
            }
        }

        $('input[name="custom_above_quantity_price"]').on('change', function(){
            aboveQuantityPrice();
        });

        aboveQuantityPrice();

        function aboveQuantityPrice(){
            let customAboveQtyPrice = $('input[name="custom_above_quantity_price"]:checked').length;

            if(customAboveQtyPrice == 1){
                $('#above_quantity_price').fadeIn();
            } else{
                $('#above_quantity_price').fadeOut();
            }
        }

        $('input[name="custom_desinfection"]').on('change', function(){
            customDesinfectionPrice();
        });

        customDesinfectionPrice();

        function customDesinfectionPrice(){
            let customDesinfectionPrice = $('input[name="custom_desinfection"]:checked').length;

            if(customDesinfectionPrice == 1){
                $('#custom_desinfection_price').fadeIn();
            } else{
                $('#custom_desinfection_price').fadeOut();
            }
        }

        $('input[name="oil_switcher"]').on('change', function(){
            oilSwitcher();
        });

        oilSwitcher();

        function oilSwitcher(){
            let customOil = $('input[name="oil_switcher"]:checked').length;

            if(customOil == 1){
                $('#custom_oil_price').fadeIn();
            } else{
                $('#custom_oil_price').fadeOut();
            }
        }

        $('input[name="fat_switcher"]').on('change', function(){
            fatSwitcher();
        });

        fatSwitcher();

        function fatSwitcher(){
            let customFat = $('input[name="fat_switcher"]:checked').length;

            if(customFat == 1){
                $('#custom_fat_price').fadeIn();
            } else{
                $('#custom_fat_price').fadeOut();
            }
        }

        $("#close_contract").on('click', function(){
            let id = $(this).data('id');
            let date = $('input[name="closed_at"]').val();

            if(!$(this).is('[disabled=""]')){
                $.ajax({
                    type: "POST",
                    url: "/close-contract",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                        closed_at: date,
                    },
                }).done(function (data) {
                    $("#close_contract").attr( "disabled", "disabled" );
                    $("#close_contract").addClass( "disabled");
                });
            };
        });

        // pridat iny druh odpadu
        $("#new-custom-waste").click( function(e){
            let isContract = $(this).data('is-contract');

            if(isContract == 1) {
                if(!confirm('Ak budete pokračovať, prídete o všetky neuložené zmeny. Naozaj chcete pokračovať?')){
                    e.preventDefault();
                }
            } else {
                e.preventDefault();
                alert('Najprv uložte zmluvu, až tak môžete pridať druh odpadu');
            }
            console.log(isContract);
        });
    </script>
@endsection
