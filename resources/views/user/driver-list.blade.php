@extends('layouts.master')

@section('title')
    Zoznam vodičov
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zoznam vodičov</h1>
        <a href="{{ url('new-customer/3') }}">
            <button class="btn btn-primary">
                <i class="fa fa-user-plus"> Pridať vodiča</i>
            </button>
        </a>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zoznam vodičov</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Meno</th>
                        <th>Email</th>
                        <th>Telefón</th>
                        <th>Upraviť</th>
                        <th>Zmazať</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Meno</th>
                        <th>Email</th>
                        <th>Telefón</th>
                        <th>Upraviť</th>
                        <th>Zmazať</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($userList as $user)
                        <tr id="user-id-{{ $user->id }}">
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->telefon }}</td>
                            <td class="text-center">
                                <a href="{{ url('edit-customer/' . $user->id) }}">
                                    <i class="fas fa-user-edit"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="#" class="delete" data-id="{{ $user->id }}" data-name="{{ $user->name }}">
                                    <i class="fas fa-trash-alt text-danger"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });

        // delete
        // TODO dokoncit delete uzivatela
        $(".delete").click( function(e){
            e.preventDefault();
            var id = $(this).data('id');
            var name = $(this).data('name');
            if (confirm("Naozaj chcete odstrániť uživateľa " + id + " - " + name + " ?")) {
                $.ajax({
                    type: "POST",
                    url: "/delete-customer",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                }).done(function( data ) {
                    removeRow(data);
                });
            }

            function removeRow(data) {
                $("#user-id-" + data).remove();
            }
        });
    </script>
@endsection
