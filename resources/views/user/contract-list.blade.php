@extends('layouts.master')

@section('title')
    Zmluvy
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Zmluvy</h1>
        <a href="{{ url('new-contract/' . $user->id) }}">
            <button class="btn btn-primary">
                <i class="fa fa-user-plus"> Pridať Zmluvu</i>
            </button>
        </a>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Zmluvy zoznam: {{ $user->name }}, {{ $user->ico }}</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="userList" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Meno prevádzky</th>
                        <th>Adresa</th>
                        <th>PSČ</th>
                        <th>Mesto</th>
                        <th>Začiatok</th>
                        <th>Sudy na sklade 30 / 60 L</th>
                        <th>Výmena sudov</th>
                        <th>Veľkosť sudov</th>
                        <th>Zmluva</th>
                        <th>Upraviť</th>
                        <th>Zrušená dňa</th>
                        <th>Zmazať</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Meno prevádzky</th>
                        <th>Adresa</th>
                        <th>PSČ</th>
                        <th>Mesto</th>
                        <th>Začiatok</th>
                        <th>Sudy na sklade 30 / 60 L</th>
                        <th>Výmena sudov</th>
                        <th>Veľkosť sudov</th>
                        <th>Zmluva</th>
                        <th>Upraviť</th>
                        <th>Zrušená dňa</th>
                        <th>Zmazať</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($user->contract as $contract)
                        @if($contract->deleted == 0)
                            <tr id="contract-id-{{ $contract->id }}">
                                <td>{{ $contract->branch_name }}</td>
                                <td>{{ $contract->adresa }}</td>
                                <td>{{ $contract->psc }}</td>
                                <td>{{ $contract->mesto }}</td>
                                <td>{{ $contract->start_date }}</td>
                                <td>{{ $contract->stored_barels_30 }} / {{ $contract->stored_barels_60 }}</td>
                                <td>{{ $contract->num_barels }}</td>
                                <td>{{ $contract->barel_size }}</td>
                                <td class="text-center">
                                    @if(isset($contract->contract) && $contract->contract != null)
                                        <a href="{{ url('storage/app/public/zmluvy/' . $user->id . '/' . $contract->contract) }}" target="_blank">
                                            <i class="fas fa-file-pdf"></i>
                                        </a>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td class="text-center">
                                    <a href="{{ url('edit-contract/' . $contract->id) }}">
                                        <i class="fas fa-user-edit"></i>
                                    </a>
                                </td>
                                <td>{{ $contract->closed_at }}</td>
                                <td class="text-center">
                                    <a href="#" class="delete" data-id="{{ $contract->id }}">
                                        <i class="fas fa-trash-alt text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="d-flex row justify-items-between mt-2 mb-2">
                <div class="col-md-6">
                    <a href="{{ url('customer-list') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#userList').DataTable();

            let currentPage = table.page.info().page;
            $('#userList').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });

        // delete
        $(".delete").click( function(e){
            e.preventDefault();
            var id = $(this).data('id');
            if (confirm("Naozaj chcete odstrániť zmluvu " + id + " ?")) {
                $.ajax({
                    type: "POST",
                    url: "/delete-contract",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                }).done(function( data ) {
                    removeRow(data);
                });
            }

            function removeRow(data) {
                $("#contract-id-" + data).remove();
            }
        });
    </script>
@endsection
