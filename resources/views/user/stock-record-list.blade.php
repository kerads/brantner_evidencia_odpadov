@extends('layouts.master')

@section('title')
    Evidencia
@endsection

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Evidencia</h1>
        <form action="{{ url('export-stock-records') }}" method="GET">
            @csrf
            <div class="input-group d-flex justify-content-center align-items-center">
                <label for="year_month" class="mr-2">Export do excelu <i class="fa fa-table"></i></label>
                <input
                    type="month"
                    id="export-year-month"
                    name="year_month"
                    class="form-control small ml-2"
                    value="{{ old('year_month', $year_month) }}">
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">
                        <i class="fas fa-download fa-sm"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header d-flex justify-content-between align-items-center py-3">
            <h6 class="m-0 font-weight-bold text-primary">Evidencia skladových zásob</h6>
            <form action="{{ url('stock-records') }}" method="POST">
                @csrf
                <div class="input-group">
                    <input
                        type="month"
                        name="year_month"
                        class="form-control small"
                        value="{{ old('year_month', $year_month) }}">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Názov prevádzkovateľa</th>
                        <th>Názov prevádzky</th>
                        <th>Adresa prevádzky</th>
                        <th>Katalógové číslo</th>
                        <th>Váha (t)</th>
                        <th>Cena €</th>
                        <th>Toaletný papier</th>
                        <th>Hygienické vreckovky</th>
                        <th>Kuchynské vreckovky</th>
                        <th>Olej</th>
                        <th>Počet sudov</th>
                        <th>Počet sudov nad zm.množstvo</th>
                        <th>Cena nad zm.množstvo €</th>
                        <th>dezinfekcia €</th>
                        <th>Počet zberov</th>
                        <th>Ext. č. firmy</th>
                        <th>Ext. č. pobočky</th>
                        <th>Poznámka</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Názov prevádzkovateľa</th>
                        <th>Názov prevádzky</th>
                        <th>Adresa prevádzky</th>
                        <th>Katalógové číslo</th>
                        <th>Váha (t)</th>
                        <th>Cena €</th>
                        <th>Toaletný papier</th>
                        <th>Hygienické vreckovky</th>
                        <th>Kuchynské vreckovky</th>
                        <th>Olej</th>
                        <th>Počet sudov</th>
                        <th>Počet sudov nad zm.množstvo</th>
                        <th>Cena nad zm.množstvo €</th>
                        <th>dezinfekcia €</th>
                        <th>Počet zberov</th>
                        <th>Ext. č. firmy</th>
                        <th>Ext. č. pobočky</th>
                        <th>Poznámka</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @if(!empty($collectingFormList))
                        @include('user.partials.stock-record-line', ['list' => $collectingFormList])
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('public/themes/sb-admin-2/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        // tabulka
        $(document).ready(function() {
            let table = $('#table').DataTable();

            let currentPage = table.page.info().page;
            $('#table').on( 'page.dt', function () {
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
            getSearchSession(table)

            $('input[type=search]').on('keyup', function(){
                currentPage = table.page.info().page;
                setSearchSession(currentPage)
            })
        });
    </script>
@endsection
