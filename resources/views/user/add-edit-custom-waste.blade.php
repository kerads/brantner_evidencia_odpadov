@extends('layouts.master')

@section('title')
    {{ isset($wasteType->id) ? 'Upraviť druh odpadu' : 'Pridať nový druh odpadu' }}
@endsection

@section('content')
    <div class="row col-12">
        <form method="POST" action="{{ url('save-custom-waste') }}" class="col-12" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="user_id" value="{{ $contract->user_id }}">
            <input type="hidden" name="contract_id" value="{{ $contract->id }}">

            <div class="row col-12">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="card-header">
                            {{ isset($wasteType->id) ? 'Upraviť druh odpadu' : 'Pridať nový druh odpadu' }}
                        </div>

                        <div class="card-body">
                            @if($contract->customWasteType()->exists())
                                @foreach($contract->customWasteType as $waste)
                                    <div class="form-group row">
                                        <div class="col-md-2 text-center">
                                            <a href="{{ url('edit-custom-waste/' . $waste->id) }}">
                                                <i class="fa fa-pencil-alt text-primary"></i>
                                            </a>
                                        </div>
                                        <div class="col-md-4">{{ $waste->code }}</div>
                                        <div class="col-md-4">{{ $waste->name }}</div>
                                        <div class="col-md-2 text-center">
                                            <a href="{{ url('delete-custom-waste/' . $waste->id) }}" class="delete">
                                                <i class="fa fa-trash text-danger"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                @endforeach
                            @endif

                            <div class="form-group row">
                                <label for="code" class="col-md-4 col-form-label text-md-right">Kód odpadu</label>

                                <div class="col-md-8">
                                    <input
                                        id="code"
                                        type="text"
                                        class="form-control @error('code') is-invalid @enderror"
                                        name="code"
                                        value="{{ old('code', isset($customWaste) ? $customWaste->code : '') }}"
                                        required>

                                    @error('code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Názov odpadu</label>

                                <div class="col-md-8">
                                    <input id="name"
                                           type="text"
                                           class="form-control @error('name') is-invalid @enderror"
                                           name="name"
                                           value="{{ old('name', isset($customWaste->name) ? $customWaste->name : '') }}">

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>

                            <hr>

                            <div class="d-flex row justify-items-between mt-2 mb-2">
                                <div class="col-md-6">
                                    <a href="{{ url('edit-contract/' . $contract->id) }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                                </div>
                                <div class="col-md-6 text-right">
                                    <button type="submit" class="btn btn-primary">
                                        <i class="fa fa-save"> {{ isset($customWaste->id) ? 'Upraviť odpad' : 'Uložiť odpad' }}</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script>
        // delete
        $(".delete").click( function(e){
            e.preventDefault();
            if (confirm("Naozaj chcete odstrániť tento záznam ?")) {
                window.location = $(this).attr('href');
            }
        });
    </script>
@endsection
