@extends('layouts.master')

@section('title')
    {{ isset($user->id) ? 'Upraviť klienta' : 'Nový klient' }}
@endsection

@section('content')
    <div class="row col-12">
        <form method="POST" action="{{ url('save-customer') }}" class="col-12" enctype="multipart/form-data">
            @csrf
            @if(isset($user->id))
                <input type="hidden" name="id" value="{{$user->id}}">
                <input type="hidden" name="role_id" value="{{$user->role_id}}">
            @else
                <input type="hidden" name="role_id" value="{{$role_id}}">
            @endif

            <div class="card shadow mb-4">
                <div class="card-header col-12 d-flex justify-content-between align-items-center py-3">
                    <h6 class=" col-md-4 font-weight-bold text-primary">{{ isset($user->id) ? 'Upraviť klienta' : 'Nový klient' }}</h6>

                    <div>

                    </div>
                    @if((isset($user) && $user->role_id == 4) || (isset($role_id) && $role_id == 4) )
                        <div class="form-group col-md-8 ">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="parent_id">Materská firma</label>
                                </div>

                                <div calss="parent_id w-100">
                                    <select name="parent_id"
                                            id="parent_id"
                                            class="selectpicker border @error('parent_id') alert alert-danger @enderror"
                                            data-show-subtext="true"
                                            data-live-search="true"
                                    >
                                        @error('parent_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <option value=""> - </option>
                                        @foreach($userList as $parentUser)
                                            <option data-tokens="{{ $parentUser->id }} - {{ $parentUser->name }} : {{ $parentUser->ico }}" value="{{ $parentUser->id }}"
                                                {{ old('parent_id', isset($user) ? $user->parent_id : '')  == $parentUser->id  ? 'selected' : '' }}>
                                                {{ $parentUser->id }} - {{ $parentUser->name }} : {{ $parentUser->ico }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    @endif

                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Meno</label>

                        <div class="col-md-8">
                            <input id="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name', isset($user) ? $user->name : '') }}"
                                   autofocus
                                   required>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail</label>

                        <div class="col-md-8">
                            <input id="email"
                                   type="email"
                                   class="form-control @error('email') is-invalid @enderror"
                                   name="email"
                                   value="{{ old('email', isset($user) ? $user->email : '') }}"
                                   required>

                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="telefon" class="col-md-4 col-form-label text-md-right">Telefón</label>

                        <div class="col-md-8 input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">+421/</div>
                            </div>
                            <input
                                id="telefon"
                                type="number"
                                class="form-control @error('telefon') is-invalid @enderror"
                                name="telefon"
                                value="{{ old('telefon', isset($user->telefon) ? $user->telefon : '') }}"
                                {{ $required }}>

                            @error('telefon')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    @if((isset($role_id) ? $role_id : $user->role_id) == 3)
                        <div class="form-group row">
                            <label for="vrn" class="col-md-4 col-form-label text-md-right">ŠPZ</label>

                            <div class="col-md-8">
                                <input id="vrn"
                                       type="text"
                                       class="form-control @error('vrn') is-invalid @enderror"
                                       name="vrn"
                                       maxlength="7"
                                       value="{{ old('vrn', isset($user) ? $user->vrn : '') }}"
                                       required>

                                @error('vrn')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="signature" class="col-md-4 col-form-label text-md-right">Podpis</label>

                            <div class="col-md-8">
                                @include('driver.partials.singnature-pad')
                            </div>
                        </div>
                    @endif

                    @if((isset($role_id) ? $role_id : $user->role_id) == 4)
                        <div class="form-group row">
                            <label for="external_id" class="col-md-4 col-form-label text-md-right">Externé číslo</label>

                            <div class="col-md-8">
                                <input id="external_id"
                                       type="number"
                                       class="form-control @error('external_id') is-invalid @enderror"
                                       name="external_id"
                                       value="{{ old('external_id', isset($user->external_id) ? $user->external_id : '') }}">

                                @error('external_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ico" class="col-md-4 col-form-label text-md-right">IČO</label>

                            <div class="col-md-8">
                                <input id="ico"
                                       type="number"
                                       class="form-control @error('ico') is-invalid @enderror"
                                       name="ico"
                                       value="{{ old('ico', isset($user->ico) ? $user->ico : '') }}"
                                       {{ $required }}>

                                @error('ico')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="dic" class="col-md-4 col-form-label text-md-right">DIČ</label>

                            <div class="col-md-8">
                                <input id="dic"
                                       type="number"
                                       class="form-control @error('dic') is-invalid @enderror"
                                       name="dic"
                                       value="{{ old('dic', isset($user->dic) ? $user->dic : '') }}"
                                       {{ $required }}>

                                @error('dic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="icdph" class="col-md-4 col-form-label text-md-right">IČDPH</label>

                            <div class="col-md-8">
                                <input id="icdph"
                                       type="text"
                                       class="form-control @error('icdph') is-invalid @enderror"
                                       name="icdph"
                                       value="{{ old('icdph', isset($user->icdph) ? $user->icdph : '') }}">

                                @error('icdph')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="adresa" class="col-md-4 col-form-label text-md-right">Ulica</label>

                            <div class="col-md-8">
                                <input id="adresa"
                                       type="text"
                                       class="form-control @error('adresa') is-invalid @enderror"
                                       name="adresa"
                                       value="{{ old('adresa', isset($user->adresa) ? $user->adresa : '') }}"
                                       {{ $required }}>

                                @error('adresa')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="psc" class="col-md-4 col-form-label text-md-right">PSČ</label>

                            <div class="col-md-8">
                                <input id="psc"
                                       type="text"
                                       class="form-control @error('psc') is-invalid @enderror"
                                       name="psc"
                                       value="{{ old('psc', isset($user->psc) ? $user->psc : '') }}"
                                       {{ $required }}>

                                @error('psc')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="mesto" class="col-md-4 col-form-label text-md-right">Mesto</label>

                            <div class="col-md-8">
                                <input id="mesto"
                                       type="text"
                                       class="form-control @error('mesto') is-invalid @enderror"
                                       name="mesto"
                                       value="{{ old('mesto', isset($user->mesto) ? $user->mesto : '') }}"
                                       {{ $required }}>

                                @error('mesto')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="zodpovedny_veduci" class="col-md-4 col-form-label text-md-right">Zodpovedný pracovník</label>

                            <div class="col-md-8">
                                <input
                                    id="zodpovedny_veduci"
                                    type="text"
                                    class="form-control @error('zodpovedny_veduci') is-invalid @enderror"
                                    name="zodpovedny_veduci"
                                    value="{{ old('zodpovedny_veduci', isset($user->zodpovedny_veduci) ? $user->zodpovedny_veduci : '') }}"
                                    {{ $required }}>

                                @error('zodpovedny_veduci')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="z_telefon" class="col-md-4 col-form-label text-md-right">Telefón - z. pracovník</label>

                            <div class="col-md-8 input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">+421/</div>
                                </div>
                                <input
                                    id="z_telefon"
                                    type="number"
                                    class="form-control @error('z_telefon') is-invalid @enderror"
                                    name="z_telefon"
                                    value="{{ old('z_telefon', isset($user->z_telefon) ? $user->z_telefon : '') }}">

                                @error('z_telefon')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        @endif
                    </div>
                    <div class="d-flex row justify-items-between col-12 mt-2 mb-2">
                        <div class="col-md-6">
                            @switch(isset($role_id) ? $role_id :$user->role_id)
                                @case(2)
                                    <a href="{{ url('user-list') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                                @break
                                @case(3)
                                <a href="{{ url('driver-list') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                                @break
                                @case(4)
                                <a href="{{ url('customer-list') }}" class="btn btn-warning"><i class="fa fa-reply"> Naspäť</i></a>
                                @break
                            @endswitch
                        </div>
                        <div class="col-md-6 text-right">
                            <button type="submit" class="btn btn-primary">
                                @if(isset($user->id))
                                    <i class="fa fa-user-edit"> Upraviť klienta</i>
                                @else
                                    <i class="fa fa-user-plus"> Vytvoriť klienta</i>
                                @endif
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="{{ asset('public/js/bootstrap-select.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.parrent_id').selectpicker();
        })
    </script>
@endsection
