window.setSearchSession = function(currentPage=0){
    let setSessionUrl = window.location.origin + "/set-session"
    let currentUrl = window.location.href
    let searchValue = $('input[type=search]').val()

    $.post({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        url: setSessionUrl,
        data: {
            currentUrl: currentUrl,
            currentPage: currentPage,
            searchValue: searchValue
        },
        success: function (data, status, xhr) {
            console.log('set status: ' + status + ', data: ' + data)
        }
    })
}

window.getSearchSession = function(table) {
    let currentUrl = window.location.href
    let getSessionUrl = window.location.origin + "/get-session"

    $.get({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'GET',
        url: getSessionUrl,
        data: {
            currentUrl: currentUrl
        },
        success: function (data, status, xhr) {
            console.log('get status: ' + status + ', parseint, data-searchValue: ' + data['searchValue'] + ', data-currentPage: ' + data.currentPage)
            table.search(data.searchValue ? data.searchValue : '').draw()
            table.page(data.currentPage ? parseInt(data.currentPage)  : 0).draw(false)
        }
    })
}
