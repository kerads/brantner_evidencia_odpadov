DROP TEMPORARY TABLE IF EXISTS tmpcontract;

use evidencia_odpad;

CREATE TEMPORARY TABLE tmpcontract
SELECT
evidencia_odpad.users.id AS id,
evidencia_odpad.users.id AS user_id,
evidencia_odpad.users.company AS branch_name
FROM evidencia_odpad.users;

SELECT * FROM tmpcontract;


INSERT INTO eo_prod.contracts (id, user_id, branch_name)
SELECT id, user_id, branch_name
FROM tmpcontract;
