DROP TEMPORARY TABLE IF EXISTS tmp;

USE access;
CREATE TEMPORARY TABLE tmp
SELECT
    access.podnety_t.Id_podnet AS `id`,
    fares.c_offices.id AS `service_id`,
    COALESCE(fares.c_delivered_methods.id, 0) AS `delivered_method_id`,
    COALESCE(fares.c_suggestion_specifications.id, 0) AS `specification_id`,
    CASE access.podnety_t.Datum_prijatia
        WHEN NULL THEN null
        ELSE STR_TO_DATE(CONCAT(DATE_FORMAT(access.podnety_t.Datum_prijatia,'%Y-%m-%d'), '', DATE_FORMAT(access.podnety_t.Cas_prijatia,"%H:%i:%s")), '%Y-%m-%d %H:%i:%s')
        END AS `delivered_date`,
    COALESCE(access.podnety_t.Meno , '-') AS `name`,
    CASE access.podnety_t.Tel_cislo
		WHEN 'neuvedené' THEN null
		WHEN 'Severná 1-2, Martin' THEN null
		WHEN 'Severná 1-2' THEN null
		WHEN 'utajene cislo' THEN null
		WHEN 'č.t. 150' THEN 150
		ELSE SUBSTRING_INDEX(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(access.podnety_t.Tel_cislo, 'tel.', ''), '(0)', ''), '/', ''), ' ', ''), '+', '00'), ',', 1)
		END AS `tel`,
    CASE access.podnety_t.Email
        WHEN '@' THEN null
        ELSE access.podnety_t.Email
        END AS `email`,
    COALESCE(access.podnety_t.Obsah, '-') AS `description`,
    access.podnety_t.Zodpovedny AS `responsible_person`,
    access.podnety_t.Podnet_vybavuje AS `task_solver`,
    access.podnety_t.Stanovisko AS `stand`,
    access.podnety_t.Navrhnute_riesenie AS `task_solution`,
    CASE access.podnety_t.Datum_odstranenia_nezhody
		WHEN access.podnety_t.Datum_odstranenia_nezhody < '2010-08-04 00:00:00' THEN null
		ELSE access.podnety_t.Datum_odstranenia_nezhody
		END AS `solved_date`,
    CASE access.podnety_t.Splenenie_ulohy
        WHEN 'Splnená' THEN 1
        ELSE 0
        END AS `task_finished`,
    access.podnety_t.Vybavil AS `final_solver`,
    access.podnety_t.Datum_vybavenia AS `finished_date`,
    CASE access.podnety_t.Cas_kontaktovania
        WHEN NULL THEN null
        ELSE STR_TO_DATE(CONCAT(DATE_FORMAT(access.podnety_t.Datum_vybavenia,'%Y-%m-%d'), '', DATE_FORMAT(access.podnety_t.Cas_kontaktovania,"%H:%i:%s")), '%Y-%m-%d %H:%i:%s')
        END AS `contact_date`,
    access.podnety_t.Poznamka AS `note`,
    access.podnety_t.Reklamacia_podnet AS `camplaint`,
    CASE access.podnety_t.Opodstatnenost
        WHEN 'N' THEN 1
        WHEN 'Nezhoda' THEN 1
        WHEN 'P' THEN 2
        WHEN 'Podnet' THEN 2
        WHEN 'R' THEN 3
        WHEN 'Reklamácia' THEN 3
        WHEN 'S' THEN 4
        WHEN 'Sťažnosť' THEN 4
        ELSE 5
        END AS `suggestion_id`,
    CASE access.podnety_t.Opodstatnenost
        WHEN 'Opodstatnené' THEN 1
        WHEN 'Neopodstatnené' THEN 0
        ELSE null
        END AS `valid_request`,
    access.podnety_t.Dovod_staznosti AS `request_reason`
FROM access.podnety_t
         LEFT JOIN fares.c_delivered_methods on access.podnety_t.Sposob_prijatia = c_delivered_methods.name
         LEFT JOIN fares.c_offices on access.podnety_t.ICO_prev = c_offices.bid
         LEFT JOIN fares.c_suggestion_specifications on access.podnety_t.Specifikacia = c_suggestion_specifications.name;



select * from tmp;
select * from tmp WHERE solved_date IS NOT NULL ORDER BY solved_date;
select camplaint, count(*) from tmp group by camplaint;

INSERT INTO fares.suggestions (suggestion_id, service_id, delivered_method_id, specification_id, delivered_date, name, tel, email, description, responsible_person, task_solver, stand, task_solution, solved_date, task_finished, final_solver, finished_date, contact_date, note, camplaint, valid_request, request_reason)
SELECT suggestion_id, service_id, delivered_method_id, specification_id, delivered_date, name, tel, email, description, responsible_person, task_solver, stand, task_solution, solved_date, task_finished, final_solver, finished_date, contact_date, note, camplaint, valid_request, request_reason
FROM tmp;
