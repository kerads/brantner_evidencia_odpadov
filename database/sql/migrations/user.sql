# uprava pred TMP
use evidencia_odpad;

SET SQL_SAFE_UPDATES = 0;
UPDATE evidencia_odpad.users SET `telpracovnik` = null WHERE `telpracovnik` = '' ;
UPDATE evidencia_odpad.users SET `p_telefon` = null WHERE `p_telefon` = '' ;

UPDATE evidencia_odpad.users SET `telpracovnik` = SUBSTRING_INDEX(`telpracovnik`, ', ', 1);
UPDATE evidencia_odpad.users SET `p_telefon` = SUBSTRING_INDEX(`p_telefon`, ', ', 1);

UPDATE evidencia_odpad.users SET `telpracovnik` = REPLACE(`telpracovnik`, ' ', '');
UPDATE evidencia_odpad.users SET `p_telefon` = REPLACE(`p_telefon`, ' ', '');

UPDATE evidencia_odpad.users SET `telpracovnik` = REPLACE(`telpracovnik`, '/', '');
UPDATE evidencia_odpad.users SET `p_telefon` = REPLACE(`p_telefon`, '/', '');

UPDATE evidencia_odpad.users SET `telpracovnik` = REPLACE(`telpracovnik`, '	', '');
UPDATE evidencia_odpad.users SET `p_telefon` = REPLACE(`p_telefon`, '	', '');

SELECT SUBSTRING(`telpracovnik`, 1, 10) FROM evidencia_odpad.users;
UPDATE evidencia_odpad.users SET `telpracovnik` = SUBSTRING(`telpracovnik`, 1, 10);
UPDATE evidencia_odpad.users SET `p_telefon` = SUBSTRING(`p_telefon`, 1, 10);


# TMP and  insertt

DROP TEMPORARY TABLE IF EXISTS tmpuser;

# 'external_id'

USE evidencia_odpad;
CREATE TEMPORARY TABLE tmpuser
SELECT
evidencia_odpad.users.id AS id,
evidencia_odpad.users.company AS name,
evidencia_odpad.users.email AS email,
evidencia_odpad.users.ico AS ico,
evidencia_odpad.users.dic AS dic,
evidencia_odpad.users.icdph AS icdph,
evidencia_odpad.users.adresa AS adresa,
evidencia_odpad.users.psc AS psc,
evidencia_odpad.users.mesto AS mesto,
evidencia_odpad.users.pracovnik AS zodpovedny_veduci,
convert(STRING(evidencia_odpad.users.p_telefon), UNSIGNED INTEGER) AS z_telefon,
# evidencia_odpad.users.p_telefon AS z_telefon,
evidencia_odpad.users.telpracovnik AS telefon,
'4' AS role_id
FROM evidencia_odpad.users;

SELECT * FROM tmpuser;


INSERT INTO eo_prod.users ('id', 'name', 'email', 'ico', 'dic', 'icdph', 'adresa', 'psc', 'mesto', 'zodpovedny_veduci', 'z_telefon', 'telefon', 'role_id')
SELECT 'id', 'name', 'email', 'ico', 'dic', 'icdph', 'adresa', 'psc', 'mesto', 'zodpovedny_veduci', 'z_telefon', 'telefon', 'role_id'
FROM tmpuser;

INSERT INTO eo_prod.users ('id', 'name', 'email', 'ico', 'dic', 'icdph', 'adresa', 'psc', 'mesto', 'zodpovedny_veduci', 'z_telefon', 'telefon', 'role_id') SELECT id, name, email, ico, dic, icdph, adresa, psc, mesto, zodpovedny_veduci, z_telefon, telefon, role_id FROM tmpuser	Error Code: 1064. You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near ''id', 'name', 'email', 'ico', 'dic', 'icdph', 'adresa', 'psc', 'mesto', 'zodpove' at line 1	0.000 sec





