<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $warehouse = [
            ['user_id' => 1, 'toilet_paper' => 0, 'hygienic_handkerchiefs' => 0, 'kitchen_towels' => 0, 'is_summary' => 1, 'summary_date' => '2021-04-01', 'change_date' => '2021-04-01', 'created_at' => '2021-04-01 23:55:33', 'updated_at' => '2021-04-01 23:55:33']
        ];

        DB::table('storage')->insert($warehouse);
    }
}
