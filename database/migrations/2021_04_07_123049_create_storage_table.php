<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storage', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('toilet_paper')->nullable();
            $table->integer('hygienic_handkerchiefs')->nullable();
            $table->integer('kitchen_towels')->nullable();
            $table->boolean('is_summary')->default(0);
            $table->date('summary_date')->nullable();
            $table->date('change_date')->nullable();
            $table->timestamps();
        });

        Artisan::call('db:seed', [
            '--class' => WarehouseSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storage');
    }
}
